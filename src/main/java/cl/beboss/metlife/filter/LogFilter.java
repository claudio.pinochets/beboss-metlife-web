package cl.beboss.metlife.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.UsuarioSession;

@WebFilter(filterName = "LogFilter",urlPatterns = {"/admin/*", "/perfil/*", "/funnel/*", "/login/*", "/mixProd/*", "/tableroMando/*", "/cargaDatosAs/*", "/sueldo/*" })
public class LogFilter implements Filter {     
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {           
		HttpServletRequest request = (HttpServletRequest) req;  
		HttpServletResponse response = (HttpServletResponse) res;  

		response.addHeader("X-Frame-Options", "SAMEORIGIN");
		response.addHeader("Content-Security-Policy", "frame-ancestors 'self'"); 
		response.addHeader("X-XSS-Protection", "1; mode=block");
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setHeader("Expires", "0"); // Proxies.

		UsuarioSession usuario= (UsuarioSession)request.getSession().getAttribute("usuario");
		
		System.out.println("request.getRequestURI()=" + request.getRequestURI());
		
		if(usuario != null || request.getRequestURI().contains("login")){
		    if(usuario != null){
		    	if(usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAdmin)) {
		    		if(!request.getRequestURI().contains("admin")) {
		    			response.sendRedirect("/admin/usuarios");
		    		}
		    	}else {
		    		if(request.getRequestURI().contains("admin")) {
		    			response.sendRedirect("/perfil");
		    		}
		    	}
	    		chain.doFilter(request, response);
		    }else{
		    	chain.doFilter(request, response);
		    }
		}else{
			response.sendRedirect("/login");
		}
	}
	
    public void destroy()
    {}

	public void init(FilterConfig config) throws ServletException {                   
	}     
	
}


