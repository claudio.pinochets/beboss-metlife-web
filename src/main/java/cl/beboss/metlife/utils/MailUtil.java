package cl.beboss.metlife.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailUtil {
	public static void sendMail(String correoUsuario, String nombreUsuario, String claveNueva) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", 465);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        //props.put("mail.smtp.debug", "true");
        
        String usuario = ConfigUtils.getCuentaNoReply();
        String clave = ConfigUtils.getClaveCuentaNoReply();

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, clave);
            }
        });
      
      	session.setDebug(true);
       
        try{
        	 Message message = new MimeMessage(session);
        	 message.addHeader("Content-type", "text/HTML; charset=UTF-8");
        	 message.addHeader("format", "flowed");
        	 message.addHeader("Content-Transfer-Encoding", "8bit");
             message.setFrom(new InternetAddress(ConfigUtils.getCuentaNoReply()));
             message.setSentDate(new Date());
             message.setRecipients(
                     Message.RecipientType.TO,
                     InternetAddress.parse(correoUsuario)
             );
             message.setSubject("Tablero De Mando : Nueva clave generada por el sistema");

             MimeMultipart multipart = new MimeMultipart();
             BodyPart messageBodyPart = new MimeBodyPart();
             
             //Set key values
             Map<String, String> input = new HashMap<String, String>();
             input.put("usuario", nombreUsuario);
             input.put("claveNueva", claveNueva);
             input.put("imagen", ConfigUtils.getImagenCorreo());
 			 String htmlText = readEmailFromHtml(ConfigUtils.getPlantillaCorreo(),input);
 			 messageBodyPart.setContent(htmlText, "text/html");
 			 multipart.addBodyPart(messageBodyPart); 
  			 message.setContent(multipart);
             Transport.send(message);
           System.out.println("Sent message successfully....");
        }catch (MessagingException mex) {
           mex.printStackTrace();
        }
    }
	
	//Method to replace the values for keys
	protected static String readEmailFromHtml(String filePath, Map<String, String> input)
	{
		String msg = readContentFromFile(filePath);
		try {
			Set<Entry<String, String>> entries = input.entrySet();
			for(Map.Entry<String, String> entry : entries) {
				msg = msg.replace(entry.getKey().trim(), entry.getValue().trim());
			}
		} catch(Exception exception){
			exception.printStackTrace();
		}
		return msg;
	}
	
	private static String readContentFromFile(String fileName)
	{
		StringBuffer contents = new StringBuffer();
		try {
			BufferedReader reader =	new BufferedReader(new FileReader(fileName));
			try {
				String line = null; 
				while (( line = reader.readLine()) != null){
					contents.append(line);
					contents.append(System.getProperty("line.separator"));
				}
			}catch (Exception ex){
				ex.printStackTrace();
			}finally {
				reader.close();
			}
		}catch (IOException ex){
			ex.printStackTrace();
		}
		return contents.toString();
	}
}
