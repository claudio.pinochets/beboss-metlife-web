package cl.beboss.metlife.utils;

import java.text.NumberFormat;
import java.util.Locale;

public class MathUtils {
	public static String quitarPunto(String valor){
		String resultado = "";
		if (valor!=null && !valor.isEmpty()) {
			if (valor.indexOf(".") >= 0) {
				int dif = valor.length() - valor.indexOf(".");
				if (dif <= 2) {
					resultado = valor.replace(".0", "");
				} else {
					resultado = valor.replace(".", ",");
				}
			} else {
				resultado = valor;
			}
		} else {
			resultado = "";
		}
		return resultado;
	}
	
	public static String formatoNumerico(String valor) {
		String resultado = "";
		double d;
		NumberFormat formato = NumberFormat.getNumberInstance(new Locale("es","ES"));
		formato.setMaximumFractionDigits(1);

		if(valor!=null && !valor.isEmpty()) {
			valor= quitarPunto(valor);
			if (valor.equalsIgnoreCase("0")) {
				return "0";
			}else {
				if(valor.indexOf(",") >= 0 ) {
					String entero = valor.substring(0, valor.indexOf(","));
					String decimal = valor.substring(valor.indexOf(","), valor.length());
					d = new Double(entero);
					resultado = formato.format(d).concat(decimal);
				}else {
					d = new Double(valor);
					resultado = formato.format(d);
				}
				int cuenta = resultado.length() - resultado.indexOf(",");
				if(resultado.indexOf(",") >= 0 && cuenta <= 2){
					resultado = resultado + '0';
				}
			}
		}else {
			return "";
		}
		return resultado;
	}
	
	public static String formatoNumericoDecimal(String valor) {
		String resultado = "";
		double d;
		NumberFormat formato = NumberFormat.getNumberInstance(new Locale("es","ES"));
		formato.setMaximumFractionDigits(1);

		if(valor!=null && !valor.isEmpty()) {
			valor= quitarPunto(valor);
			if (valor.equalsIgnoreCase("0")) {
				return "0,00";
			}else {
				if(valor.indexOf(",") >= 0 ) {
					String entero = valor.substring(0, valor.indexOf(","));
					String decimal = valor.substring(valor.indexOf(","), valor.length());
					d = new Double(entero);
					resultado = formato.format(d).concat(decimal);
				}else {
					d = new Double(valor);
					resultado = formato.format(d);
				}
				int cuenta = resultado.length() - resultado.indexOf(",");
				if(resultado.indexOf(",") >= 0 && cuenta <= 2){
					resultado = resultado + '0';
				}
			}
		}else {
			return "";
		}
		return resultado;
	}

}
