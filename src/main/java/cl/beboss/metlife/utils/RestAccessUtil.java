package cl.beboss.metlife.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.beboss.metlife.constantes.Constantes;

public class RestAccessUtil {
	
	
	/**
	 * @param url
	 * @param object
	 * @return
	 * @throws Exception
	 */
	public static String post(String url, Object object) throws Exception {
		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);

		String json = gson().toJson(object);
		System.out.println("url=" + url);
		System.out.println("json=" + json);

		StringEntity param = new StringEntity(json, "UTF-8");
		post.addHeader("content-type", MediaType.APPLICATION_FORM_URLENCODED);
		post.setEntity(param);
		post.setHeader("Accept", "application/json");
		post.setHeader("Content-type", "application/json");

		return executeResponse(client, post);
	}

	/**
	 * @param client
	 * @param arg0
	 * @return
	 * @throws Exception
	 */
	private static String executeResponse(HttpClient client, HttpUriRequest arg0)
			throws Exception {
		HttpResponse response = client.execute(arg0);
		int responseStatus = response.getStatusLine().getStatusCode();
		
		System.out.println("responseStatus=" + responseStatus);

		if (Status.INTERNAL_SERVER_ERROR.getStatusCode() == responseStatus) {
			throw new Exception(
					"Error (500) interno en el servidor de servicios.");
		} else if (Status.NOT_FOUND.getStatusCode() == responseStatus) {
			throw new Exception("Error (404) servicio no encontrado.");
		} else {
			
			System.out.println("antes de BufferedReader");
			 
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));
	 
 			System.out.println("despues de BufferedReader");
			
			String line = "";
			StringBuilder builder = new StringBuilder();
			 
			while ((line = rd.readLine()) != null) {
				builder.append(line);
			}
			System.out.println("executeResponse=" + builder.toString());
			return builder.toString();
		}
	}

	/**
	 * @return
	 */
	public static Gson gson() {
		Gson gson = new GsonBuilder().setDateFormat(Constantes.DateFormat).create();
		return gson;
	}
}
