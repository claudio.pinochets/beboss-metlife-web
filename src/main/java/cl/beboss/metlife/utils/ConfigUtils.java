package cl.beboss.metlife.utils;

import cl.beboss.metlife.constantes.Constantes;

public class ConfigUtils {
	public static String getBusIP(){
		return System.getProperty(Constantes.METLIFE_ENDPOINT);
	}
	public static String getRutaArchivos(){
		return System.getProperty(Constantes.METLIFE_RUTA_ARCHIVOS);
	}
	public static String getCuentaNoReply(){
		return System.getProperty(Constantes.METLIFE_CUENTA_NOREPLY);
	}
	public static String getClaveCuentaNoReply(){
		return System.getProperty(Constantes.METLIFE_CLAVE_CUENTA_NOREPLY);
	}
	public static String getImagenCorreo(){
		return System.getProperty(Constantes.METLIFE_IMAGEN_BEBOSS);
	}
	public static String getPlantillaCorreo(){
		return System.getProperty(Constantes.METLIFE_PLANTILLA_HTML);
	}
}
