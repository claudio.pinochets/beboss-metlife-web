package cl.beboss.metlife.vo;

import cl.beboss.metlife.utils.MathUtils;

public class TablaPerfil {
	
	private Integer idPersonal;
	private String nombre;	
	private String bcpu;	
	private String rotacion;
	private String nap_real;	
	private String nap_meta;
	private String nap_ppto;
	private String nap_estado;
	private String polizas_real;	
	private String polizas_meta;
	private String polizas_ppto;
	private String polizas_estado;
	private String pb_real;	
	private String pb_meta;
	private String pb_ppto;
	private String pb_estado;
	private String pe_real;
	private String pe_meta;
	private String pe_ppto;
	private String pe_estado;
	private String icono;
	private String estilo;
	private String perfilActual;
	private String verPerfil;
	private String accion;
	private boolean habilitado;
	private String tramo;
	private String antiguedad;
	private Integer IdPlanificacion;
	
	public TablaPerfil(Integer idPersonal, String nombre, String bcpu,
						String rotacion, String nap_real, String nap_meta, String nap_ppto, String nap_estado, String polizas_real, String polizas_meta, String polizas_ppto, String polizas_estado, String pb_real, String pb_meta, String pb_ppto, String pb_estado,
						String pe_real, String pe_meta, String pe_ppto, String pe_estado, String icono, String estilo,String perfilActual, String verPerfil, String accion, boolean habilitado, String tramo, String antiguedad, Integer IdPlanificacion) {
		
		this.idPersonal=idPersonal;
		this.nombre=nombre;
		this.bcpu=bcpu;
		this.rotacion=rotacion;
		this.nap_real=nap_real;
		this.nap_meta=nap_meta;
		this.nap_ppto=nap_ppto;
		this.nap_estado=nap_estado;
		this.polizas_real=polizas_real;
		this.polizas_meta=polizas_meta;
		this.polizas_ppto=polizas_ppto;
		this.polizas_estado=polizas_estado;
		this.pb_real=pb_real;
		this.pb_meta=pb_meta;
		this.pb_ppto=pb_ppto;
		this.pb_estado=pb_estado;
		this.pe_real=pe_real;
		this.pe_meta=pe_meta;
		this.pe_ppto=pe_ppto;
		this.pe_estado=pe_estado;
		this.icono=icono;
		this.estilo=estilo;
		this.perfilActual=perfilActual;
		this.verPerfil=verPerfil;
		this.polizas_estado=polizas_estado;
		this.pb_estado=pb_estado;
		this.pe_estado=pe_estado;	
		this.accion=accion;
		this.habilitado=habilitado;
		this.tramo=tramo;
		this.antiguedad=antiguedad;
		this.IdPlanificacion=IdPlanificacion;
	}

	public Integer getIdPersonal() {
		return idPersonal;
	}

	public void setIdPersonal(Integer idPersonal) {
		this.idPersonal = idPersonal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getBcpu() {
		return MathUtils.formatoNumerico(bcpu);
	}

	public void setBcpu(String bcpu) {
		this.bcpu = bcpu;
	}

	public String getRotacion() {
		return MathUtils.formatoNumerico(rotacion);
	}
	
	public void setRotacion(String rotacion) {
		this.rotacion = rotacion;
	}

	public String getNap_real() {
		return MathUtils.formatoNumerico(nap_real);
	}

	public void setNap_real(String nap_real) {
		this.nap_real = nap_real;
	}

	public String getNap_meta() {
		return MathUtils.formatoNumerico(nap_meta);
	}

	public void setNap_meta(String nap_meta) {
		this.nap_meta = nap_meta;
	}

	public String getNap_ppto() {
		return MathUtils.formatoNumerico(nap_ppto);
	}

	public void setNap_ppto(String nap_ppto) {
		this.nap_ppto = nap_ppto;
	}
	
	public String getNap_estado() {
		return nap_estado;
	}

	public void setNap_estado(String nap_estado) {
		this.nap_estado = nap_estado;
	}

	public String getPolizas_real() {
		return MathUtils.formatoNumerico(polizas_real);
	}

	public void setPolizas_real(String polizas_real) {
		this.polizas_real = polizas_real;
	}

	public String getPolizas_meta() {
		return MathUtils.formatoNumerico(polizas_meta);
	}

	public void setPolizas_meta(String polizas_meta) {
		this.polizas_meta = polizas_meta;
	}

	public String getPolizas_estado() {
		return polizas_estado;
	}

	public void setPolizas_estado(String polizas_estado) {
		this.polizas_estado = polizas_estado;
	}

	public String getPb_real() {
		return MathUtils.formatoNumerico(pb_real);
	}

	public void setPb_real(String pb_real) {
		this.pb_real = pb_real;
	}

	public String getPb_meta() {
		return MathUtils.formatoNumerico(pb_meta);
	}

	public void setPb_meta(String pb_meta) {
		this.pb_meta = pb_meta;
	}

	public String getPb_estado() {
		return pb_estado;
	}

	public void setPb_estado(String pb_estado) {
		this.pb_estado = pb_estado;
	}

	public String getPe_real() {
		return MathUtils.formatoNumerico(pe_real);
	}

	public void setPe_real(String pe_real) {
		this.pe_real = pe_real;
	}

	public String getPe_meta() {
		return MathUtils.formatoNumerico(pe_meta);
	}

	public void setPe_meta(String pe_meta) {
		this.pe_meta = pe_meta;
	}

	public String getPe_estado() {
		return pe_estado;
	}

	public void setPe_estado(String pe_estado) {
		this.pe_estado = pe_estado;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getPerfilActual() {
		return perfilActual;
	}

	public void setPerfilActual(String perfilActual) {
		this.perfilActual = perfilActual;
	}

	public String getVerPerfil() {
		return verPerfil;
	}

	public void setVerPerfil(String verPerfil) {
		this.verPerfil = verPerfil;
	}

	public String getPolizas_ppto() {
		return MathUtils.formatoNumerico(polizas_ppto);
	}

	public void setPolizas_ppto(String polizas_ppto) {
		this.polizas_ppto = polizas_ppto;
	}

	public String getPb_ppto() {
		return MathUtils.formatoNumerico(pb_ppto);
	}

	public void setPb_ppto(String pb_ppto) {
		this.pb_ppto = pb_ppto;
	}

	public String getPe_ppto() {
		return MathUtils.formatoNumerico(pe_ppto);
	}

	public void setPe_ppto(String pe_ppto) {
		this.pe_ppto = pe_ppto;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getTramo() {
		return tramo;
	}

	public void setTramo(String tramo) {
		this.tramo = tramo;
	}

	public String getAntiguedad() {
		return MathUtils.formatoNumerico(antiguedad); 
	}

	public void setAntiguedad(String antiguedad) {
		this.antiguedad = antiguedad;
	}

	public Integer getIdPlanificacion() {
		return IdPlanificacion;
	}

	public void setIdPlanificacion(Integer idPlanificacion) {
		IdPlanificacion = idPlanificacion;
	}	
	
}
