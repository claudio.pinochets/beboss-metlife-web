package cl.beboss.metlife.constantes;

import java.text.SimpleDateFormat;

import cl.beboss.metlife.utils.ConfigUtils;

public final class Constantes {
	public static final String DateFormat = "yyyy-MM-dd'T'HH:mm:ss";
	public static final SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-mm-yyyy");
	public static String rutaArchivos = ConfigUtils.getRutaArchivos();
	public static String empresa = "1";
	public static String perfilAdmin = "Administrativo";
	public static String perfilCorp = "Corporativo";
	public static String perfilGA = "GA";
	public static String perfilGU = "GU";
	public static String perfilAsesor = "AS";
	public static String perfilAs = "Asesor";
	
	public static final String SOA_SERVER = "http://".concat(ConfigUtils.getBusIP());
	public static final String METLIFE_ENDPOINT = "METLIFE_ENDPOINT";
	public static final String METLIFE_RUTA_ARCHIVOS = "METLIFE_RUTA_ARCHIVOS";
	/*Configuraciones de correo*/
	public static final String METLIFE_CUENTA_NOREPLY = "METLIFE_CUENTA_NOREPLY";
	public static final String METLIFE_CLAVE_CUENTA_NOREPLY = "METLIFE_CLAVE_CUENTA_NOREPLY";
	public static final String METLIFE_IMAGEN_BEBOSS = "METLIFE_IMAGEN_BEBOSS";
	public static final String METLIFE_PLANTILLA_HTML = "METLIFE_PLANTILLA_HTML";
}
