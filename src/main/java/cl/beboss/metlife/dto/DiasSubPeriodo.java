package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("serial")
public class DiasSubPeriodo implements Serializable{
	private static final String[] dias={"Dom","Lun","Mar", "Mier","Jue","Vie","Sab"};
	private String Fecha;
	private String EsHabil;
	private Integer Real;
	private Integer Plan;
	
	public String getEstilo() {
		String estilo;
		if(this.Real >= this.Plan){
			estilo = "totalesOKClass";
    	}else{
    		estilo = "totalesNOKClass";
    	}
		return estilo;
	}
	
	public String getDia() {
		String dia = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar now = Calendar.getInstance();
		try {
			now.setTime(sdf.parse(this.Fecha));
			dia = dias[now.get(Calendar.DAY_OF_WEEK) - 1] + " " + this.Fecha.substring(0,5);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dia;
	}
	
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public String getEsHabil() {
		return EsHabil;
	}
	public void setEsHabil(String esHabil) {
		EsHabil = esHabil;
	}

	public Integer getReal() {
		return Real;
	}

	public void setReal(Integer real) {
		Real = real;
	}

	public Integer getPlan() {
		return Plan;
	}

	public void setPlan(Integer plan) {
		Plan = plan;
	}
}
