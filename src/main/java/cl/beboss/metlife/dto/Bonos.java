package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Bonos implements Serializable{

	private String Tramo;
	private String Bono;
	
	public String getTramo() {
		return Tramo;
	}
	public void setTramo(String tramo) {
		Tramo = tramo;
	}
	public String getBono() {
		return MathUtils.formatoNumerico(Bono);
	}
	public void setBono(String bono) {
		Bono = bono;
	}
}
