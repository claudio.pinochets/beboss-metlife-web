package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class SubPeriodos implements Serializable{

	private String IdSubPeriodo;
	private String NombreSubPeriodo;
	private String SubPeriodo;
	private String FechaInicio;
	private String FechaFin;
	
	public String getIdSubPeriodo() {
		return IdSubPeriodo;
	}
	public void setIdSubPeriodo(String idSubPeriodo) {
		IdSubPeriodo = idSubPeriodo;
	}
	public String getNombreSubPeriodo() {
		return NombreSubPeriodo;
	}
	public void setNombreSubPeriodo(String nombreSubPeriodo) {
		NombreSubPeriodo = nombreSubPeriodo;
	}
	public String getSubPeriodo() {
		return SubPeriodo;
	}
	public void setSubPeriodo(String subPeriodo) {
		SubPeriodo = subPeriodo;
	}
	public String getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return FechaFin;
	}
	public void setFechaFin(String fechaFin) {
		FechaFin = fechaFin;
	}
}
