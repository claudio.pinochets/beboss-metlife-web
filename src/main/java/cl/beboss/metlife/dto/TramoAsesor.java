package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TramoAsesor implements Serializable{
	private String Tramo;
	private String Avance;
	
	public String getTramo() {
		return Tramo;
	}
	public void setTramo(String tramo) {
		Tramo = tramo;
	}
	public String getAvance() {
		return Avance;
	}
	public void setAvance(String avance) {
		Avance = avance;
	}
}
