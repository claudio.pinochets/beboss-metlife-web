package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class FunnelDiario implements Serializable{
	
	private String NombreActividad;
	private Integer TotalReal;
	private Integer TotalPlan; 
	private List<DiasSubPeriodo> DiasSubPeriodo;
	
	public String getNombreActividad() {
		return NombreActividad;
	}
	public void setNombreActividad(String nombreActividad) {
		NombreActividad = nombreActividad;
	}
	public Integer getTotalReal() {
		return TotalReal;
	}
	public void setTotalReal(Integer totalReal) {
		TotalReal = totalReal;
	}
	public Integer getTotalPlan() {
		return TotalPlan;
	}
	public void setTotalPlan(Integer totalPlan) {
		TotalPlan = totalPlan;
	}
	public List<DiasSubPeriodo> getDiasSubPeriodo() {
		return DiasSubPeriodo;
	}
	public void setDiasSubPeriodo(List<DiasSubPeriodo> diasSubPeriodo) {
		DiasSubPeriodo = diasSubPeriodo;
	}
}
