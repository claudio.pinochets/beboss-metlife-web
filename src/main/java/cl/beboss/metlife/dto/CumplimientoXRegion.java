package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.enums.Regiones;

@SuppressWarnings("serial")
public class CumplimientoXRegion implements Serializable{
	private String IdRegion;
	private String Region;
	private String Estado;
	
	public String getCoordenada() {
		return Regiones.getRegionById(Integer.parseInt(this.IdRegion.replace(".0", ""))).getCoordenadas();
	}
	
	public String getCodigo() {
		return Regiones.getRegionById(Integer.parseInt(this.IdRegion.replace(".0", ""))).getNombre();
	}
	
	public String getIdRegion() {
		return IdRegion;
	}
	public void setIdRegion(String idRegion) {
		IdRegion = idRegion;
	}
	public String getRegion() {
		return Region;
	}
	public void setRegion(String region) {
		Region = region;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}	
}
