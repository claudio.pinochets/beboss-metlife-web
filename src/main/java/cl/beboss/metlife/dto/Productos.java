package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Productos implements Serializable{
	private String IdProducto;
	private String NombreProducto;
	private String Nombre;
	private String Meta;
	private String Real;
	private List<SubProductos> SubProductos;
	
	public String getIdProducto() {
		return IdProducto;
	}
	public void setIdProducto(String idProducto) {
		IdProducto = idProducto;
	}
	public String getNombreProducto() {
		return NombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}
	public List<SubProductos> getSubProductos() {
		return SubProductos;
	}
	public void setSubProductos(List<SubProductos> subProductos) {
		SubProductos = subProductos;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMeta() {
		return MathUtils.formatoNumerico(Meta);
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return MathUtils.formatoNumerico(Real);
	}
	public void setReal(String real) {
		Real = real;
	}
}
