package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class LoQueLlevo implements Serializable{
	private String PBasica;
	private String PExceso;
	private String NPolizas;
	private String NPolizasVida;
	
	public String getPBasica() {
		return MathUtils.formatoNumerico(PBasica);
	}
	public void setPBasica(String pBasica) {
		PBasica = pBasica;
	}
	public String getPExceso() {
		return MathUtils.formatoNumerico(PExceso);
	}
	public void setPExceso(String pExceso) {
		PExceso = pExceso;
	}
	public String getNPolizas() {
		return MathUtils.formatoNumerico(NPolizas);
	}
	public void setNPolizas(String nPolizas) {
		NPolizas = nPolizas;
	}
	public String getNPolizasVida() {
		return MathUtils.formatoNumerico(NPolizasVida);
	}
	public void setNPolizasVida(String nPolizasVida) {
		NPolizasVida = nPolizasVida;
	}
}
