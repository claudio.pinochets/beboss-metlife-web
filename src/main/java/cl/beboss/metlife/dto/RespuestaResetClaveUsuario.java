package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RespuestaResetClaveUsuario implements Serializable {
	private String Nombres;
	private String Apellidos;
	private String PassNuevo;
	
	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public String getPassNuevo() {
		return PassNuevo;
	}

	public void setPassNuevo(String passNuevo) {
		PassNuevo = passNuevo;
	}
}
