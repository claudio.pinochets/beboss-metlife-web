package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CumplimientoPeriodoComercial implements Serializable{
	
	private String Nombre;
	private String Real;
	private String Meta;
	private String Plan;
	 
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getReal() {
		return Real;
	}
	public void setReal(String real) {
		Real = real;
	}
	public String getMeta() {
		return Meta;
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getPlan() {
		return Plan;
	}
	public void setPlan(String plan) {
		Plan = plan;
	}
}
