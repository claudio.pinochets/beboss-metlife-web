package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetTableroMando implements Serializable{
	
	private String IdEmpresa;
	private String IdPersonal;
	private String IdPeriodoDesde;
	private String IdPeriodoHasta;
	private String Token;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdPeriodoDesde() {
		return IdPeriodoDesde;
	}
	public void setIdPeriodoDesde(String idPeriodoDesde) {
		IdPeriodoDesde = idPeriodoDesde;
	}
	public String getIdPeriodoHasta() {
		return IdPeriodoHasta;
	}
	public void setIdPeriodoHasta(String idPeriodoHasta) {
		IdPeriodoHasta = idPeriodoHasta;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
	
}
