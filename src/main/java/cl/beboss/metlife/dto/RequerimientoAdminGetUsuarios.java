package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoAdminGetUsuarios implements Serializable {

	private String IdEmpresa;
	private String Busqueda;
	private String CantRegisXPag;
	private String Token;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getBusqueda() {
		return Busqueda;
	}
	public void setBusqueda(String busqueda) {
		Busqueda = busqueda;
	}
	public String getCantRegisXPag() {
		return CantRegisXPag;
	}
	public void setCantRegisXPag(String cantRegisXPag) {
		CantRegisXPag = cantRegisXPag;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
}
