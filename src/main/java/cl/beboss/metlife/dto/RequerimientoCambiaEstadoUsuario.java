package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoCambiaEstadoUsuario implements Serializable{
	
	private String IdConsultor;
	private String IdEmpresa;
	private String IdSuperior;
	private String IdPersonal;
	private String Motivo;
	private String EstadoOld;
	private String EstadoNew;
	
	public String getIdConsultor() {
		return IdConsultor;
	}
	public void setIdConsultor(String idConsultor) {
		IdConsultor = idConsultor;
	}
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdSuperior() {
		return IdSuperior;
	}
	public void setIdSuperior(String idSuperior) {
		IdSuperior = idSuperior;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getMotivo() {
		return Motivo;
	}
	public void setMotivo(String motivo) {
		Motivo = motivo;
	}
	public String getEstadoOld() {
		return EstadoOld;
	}
	public void setEstadoOld(String estadoOld) {
		EstadoOld = estadoOld;
	}
	public String getEstadoNew() {
		return EstadoNew;
	}
	public void setEstadoNew(String estadoNew) {
		EstadoNew = estadoNew;
	}
}
