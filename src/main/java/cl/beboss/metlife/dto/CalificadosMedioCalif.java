package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class CalificadosMedioCalif implements Serializable{
	private Integer Cero;
	private Integer A;
	private Integer B;
	private Integer C;
	
	public Integer getCero() {
		return Cero;
	}
	public void setCero(Integer cero) {
		Cero = cero;
	}
	public Integer getA() {
		return A;
	}
	public void setA(Integer a) {
		A = a;
	}
	public Integer getB() {
		return B;
	}
	public void setB(Integer b) {
		B = b;
	}
	public Integer getC() {
		return C;
	}
	public void setC(Integer c) {
		C = c;
	}
	
}
