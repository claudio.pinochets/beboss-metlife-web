package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class SubProductos implements Serializable{

	private String IdSubProducto;
	private String SubProducto;
	private String Polizas;
	private String PBasica;
	private String PExceso;
	private String NombreSubProducto;
	private String CantidadPolizas;
	private String PrimaBasica;
	private String PrimaExceso;
	private String Nombre;
	private String Meta;
	private String Real;
	
	public String getIdSubProducto() {
		return IdSubProducto;
	}
	public void setIdSubProducto(String idSubProducto) {
		IdSubProducto = idSubProducto;
	}
	public String getSubProducto() {
		return SubProducto;
	}
	public void setSubProducto(String subProducto) {
		SubProducto = subProducto;
	}
	public String getPolizas() {
		return MathUtils.formatoNumerico(Polizas);
	}
	public void setPolizas(String polizas) {
		Polizas = polizas;
	}
	public String getPBasica() {
		return MathUtils.formatoNumericoDecimal(PBasica);
	}
	public void setPBasica(String pBasica) {
		PBasica = pBasica;
	}
	public String getPExceso() {
		return MathUtils.formatoNumericoDecimal(PExceso);
	}
	public void setPExceso(String pExceso) {
		PExceso = pExceso;
	}
	public String getNombreSubProducto() {
		return NombreSubProducto;
	}
	public void setNombreSubProducto(String nombreSubProducto) {
		NombreSubProducto = nombreSubProducto;
	}
	public String getCantidadPolizas() {
		return MathUtils.formatoNumerico(CantidadPolizas);
	}
	public void setCantidadPolizas(String cantidadPolizas) {
		CantidadPolizas = cantidadPolizas;
	}
	public String getPrimaBasica() {
		return MathUtils.formatoNumericoDecimal(PrimaBasica);
	}
	public void setPrimaBasica(String primaBasica) {
		PrimaBasica = primaBasica;
	}
	public String getPrimaExceso() {
		return MathUtils.formatoNumericoDecimal(PrimaExceso);
	}
	public void setPrimaExceso(String primaExceso) {
		PrimaExceso = primaExceso;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMeta() {
		return MathUtils.formatoNumerico(Meta);
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return MathUtils.formatoNumerico(Real);
	}
	public void setReal(String real) {
		Real = real;
	}
}
