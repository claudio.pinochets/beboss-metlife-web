package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Totales implements Serializable {

	private String Polizas;
	private String PBasica;
	private String PExceso;
	
	public String getPolizas() {
		return MathUtils.formatoNumerico(Polizas);
	}
	public void setPolizas(String polizas) {
		Polizas = polizas;
	}
	public String getPBasica() {
		return MathUtils.formatoNumerico(PBasica);
	}
	public void setPBasica(String pBasica) {
		PBasica = pBasica;
	}
	public String getPExceso() {
		return MathUtils.formatoNumerico(PExceso);
	}
	public void setPExceso(String pExceso) {
		PExceso = pExceso;
	}
}
