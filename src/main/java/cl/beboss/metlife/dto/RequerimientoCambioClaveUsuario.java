package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoCambioClaveUsuario implements Serializable{
	private String IdEmpresa;
	private String IdPersonal;
	private String User;
	private String PassActual;
	private String PassNuevo;
	private String Token;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getUser() {
		return User;
	}
	public void setUser(String user) {
		User = user;
	}
	public String getPassActual() {
		return PassActual;
	}
	public void setPassActual(String passActual) {
		PassActual = passActual;
	}
	public String getPassNuevo() {
		return PassNuevo;
	}
	public void setPassNuevo(String passNuevo) {
		PassNuevo = passNuevo;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
}
