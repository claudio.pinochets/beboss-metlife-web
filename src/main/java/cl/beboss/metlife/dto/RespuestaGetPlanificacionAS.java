package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RespuestaGetPlanificacionAS implements Serializable {

	private PlanAsesor PlanAsesor;

	public PlanAsesor getPlanAsesor() {
		return PlanAsesor;
	}

	public void setPlanAsesor(PlanAsesor planAsesor) {
		PlanAsesor = planAsesor;
	}
}
