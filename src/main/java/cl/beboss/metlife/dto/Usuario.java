package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Usuario implements Serializable {
	
	private String IdPersonal;
	private String NombresPersonal;
	private String ApellidosPersonal;
	private String IdRol;
	private String Rol;
	private String IdSupervisor;
	private String Supervisor;
	private String IdSucursal;
	private String Sucursal;
	private String IdEstadoPersonal;
	private String EstadoPersonal;
	private String IdPlanificacion;
	private String IdEstadoPlan;
	private String EstadoPlan;
	
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getNombresPersonal() {
		return NombresPersonal;
	}
	public void setNombresPersonal(String nombresPersonal) {
		NombresPersonal = nombresPersonal;
	}
	public String getApellidosPersonal() {
		return ApellidosPersonal;
	}
	public void setApellidosPersonal(String apellidosPersonal) {
		ApellidosPersonal = apellidosPersonal;
	}
	public String getIdRol() {
		return IdRol;
	}
	public void setIdRol(String idRol) {
		IdRol = idRol;
	}
	public String getRol() {
		return Rol;
	}
	public void setRol(String rol) {
		Rol = rol;
	}
	public String getIdSupervisor() {
		return IdSupervisor;
	}
	public void setIdSupervisor(String idSupervisor) {
		IdSupervisor = idSupervisor;
	}
	public String getSupervisor() {
		return Supervisor;
	}
	public void setSupervisor(String supervisor) {
		Supervisor = supervisor;
	}
	public String getIdSucursal() {
		return IdSucursal;
	}
	public void setIdSucursal(String idSucursal) {
		IdSucursal = idSucursal;
	}
	public String getSucursal() {
		return Sucursal;
	}
	public void setSucursal(String sucursal) {
		Sucursal = sucursal;
	}
	public String getIdEstadoPersonal() {
		return IdEstadoPersonal;
	}
	public void setIdEstadoPersonal(String idEstadoPersonal) {
		IdEstadoPersonal = idEstadoPersonal;
	}
	public String getEstadoPersonal() {
		return EstadoPersonal;
	}
	public void setEstadoPersonal(String estadoPersonal) {
		EstadoPersonal = estadoPersonal;
	}
	public String getIdPlanificacion() {
		return IdPlanificacion;
	}
	public void setIdPlanificacion(String idPlanificacion) {
		IdPlanificacion = idPlanificacion;
	}
	public String getIdEstadoPlan() {
		return IdEstadoPlan;
	}
	public void setIdEstadoPlan(String idEstadoPlan) {
		IdEstadoPlan = idEstadoPlan;
	}
	public String getEstadoPlan() {
		return EstadoPlan;
	}
	public void setEstadoPlan(String estadoPlan) {
		EstadoPlan = estadoPlan;
	}
}
