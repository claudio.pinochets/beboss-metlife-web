package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class PrimaEsporadica implements Serializable{
	private String Nombre;
	private String Meta;
	private String Real;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMeta() {
		return MathUtils.formatoNumerico(Meta);
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return MathUtils.formatoNumerico(Real);
	}
	public void setReal(String real) {
		Real = real;
	}
}
