package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class InfoSubAlterno implements Serializable{

	private Integer IdPersonal;
	private String Nombres;
	private String Apellidos;
	private Integer IdPerfil;
	private String Perfil;
	private String URI;
	private List<Cumplimiento> Cumplimiento;
	private String BCPU;
	private String RotacionPersonal;
	private String icono;
	private String estilo;
	private VerMeta VerMeta;
	private String Estado;
	private String Tramo;
	private String Antiguedad;
	
	public String getNombreCompleto() {
		return this.Nombres + " " + this.Apellidos;
	}
	
	public Integer getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(Integer idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public Integer getIdPerfil() {
		return IdPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		IdPerfil = idPerfil;
	}
	public String getPerfil() {
		return Perfil;
	}
	public void setPerfil(String perfil) {
		Perfil = perfil;
	}
	public String getURI() {
		return URI;
	}
	public void setURI(String uRI) {
		URI = uRI;
	}
	public List<Cumplimiento> getCumplimiento() {
		return Cumplimiento;
	}
	public void setCumplimiento(List<Cumplimiento> cumplimiento) {
		Cumplimiento = cumplimiento;
	}
	public String getBCPU() {
		return BCPU;
	}
	public void setBCPU(String bCPU) {
		BCPU = bCPU;
	}
	public String getRotacionPersonal() {
		return RotacionPersonal;
	}
	public void setRotacionPersonal(String rotacionPersonal) {
		RotacionPersonal = rotacionPersonal;
	}
	public String getIcono() {
		return icono;
	}
	public void setIcono(String icono) {
		this.icono = icono;
	}
	public String getEstilo() {
		return estilo;
	}
	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public VerMeta getVerMeta() {
		return VerMeta;
	}

	public void setVerMeta(VerMeta verMeta) {
		VerMeta = verMeta;
	}

	public String getEstado() {
		return Estado;
	}

	public void setEstado(String estado) {
		Estado = estado;
	}

	public String getTramo() {
		return Tramo;
	}

	public void setTramo(String tramo) {
		Tramo = tramo;
	}

	public String getAntiguedad() {
		return Antiguedad;
	}

	public void setAntiguedad(String antiguedad) {
		Antiguedad = antiguedad;
	}  
}
