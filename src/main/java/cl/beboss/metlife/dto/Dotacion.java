package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Dotacion implements Serializable{
	private String Nombre;
	private Integer Valor;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public Integer getValor() {
		return Valor;
	}
	public void setValor(Integer valor) {
		Valor = valor;
	}
}
