package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetSueldoMesActualAses implements Serializable{

	private String IdEmpresa;
	private String IdPersonal;
	private String IdPeriodo;
	private String Token;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
}
