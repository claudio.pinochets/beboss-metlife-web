package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class PlanAsesor  implements Serializable{

	private String IdPlan;
    private List<Productos> Productos;
    private Totales Totales;
    private String ClientesTotales;
    private String PrimaEsporadica;
    private String IdAprobador;
    private String Aprobador;
    private String FechaAprobacion;
    private String EstadoPlan;
    
	public String getIdPlan() {
		return IdPlan;
	}
	public void setIdPlan(String idPlan) {
		IdPlan = idPlan;
	}
	public List<Productos> getProductos() {
		return Productos;
	}
	public void setProductos(List<Productos> productos) {
		Productos = productos;
	}
	public Totales getTotales() {
		return Totales;
	}
	public void setTotales(Totales totales) {
		Totales = totales;
	}
	public String getClientesTotales() {
		return MathUtils.formatoNumerico(ClientesTotales);
	}
	public void setClientesTotales(String clientesTotales) {
		ClientesTotales = clientesTotales;
	}
	public String getPrimaEsporadica() {
		return MathUtils.formatoNumerico(PrimaEsporadica);
	}
	public void setPrimaEsporadica(String primaEsporadica) {
		PrimaEsporadica = primaEsporadica;
	}
	public String getIdAprobador() {
		return IdAprobador;
	}
	public void setIdAprobador(String idAprobador) {
		IdAprobador = idAprobador;
	}
	public String getAprobador() {
		return Aprobador;
	}
	public void setAprobador(String aprobador) {
		Aprobador = aprobador;
	}
	public String getFechaAprobacion() {
		return FechaAprobacion;
	}
	public void setFechaAprobacion(String fechaAprobacion) {
		FechaAprobacion = fechaAprobacion;
	}
	public String getEstadoPlan() {
		return EstadoPlan;
	}
	public void setEstadoPlan(String estadoPlan) {
		EstadoPlan = estadoPlan;
	}
}
