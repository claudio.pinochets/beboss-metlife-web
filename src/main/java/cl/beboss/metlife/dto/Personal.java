package cl.beboss.metlife.dto;

import java.io.Serializable;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Personal implements Serializable{
	private String  IdRango;
	private String  RangoAntiguedad;
	private String  Dotacion;
	private String  Rotacion;
	
	public String getIdRango() {
		return IdRango;
	}
	public void setIdRango(String idRango) {
		IdRango = idRango;
	}
	public String getRangoAntiguedad() {
		return RangoAntiguedad;
	}
	public void setRangoAntiguedad(String rangoAntiguedad) {
		RangoAntiguedad = rangoAntiguedad;
	}
	public String getDotacion() {
		return MathUtils.formatoNumerico(Dotacion);
	}
	public void setDotacion(String dotacion) {
		Dotacion = dotacion;
	}
	public String getRotacion() {
		return MathUtils.formatoNumerico(Rotacion);
	}
	public void setRotacion(String rotacion) {
		Rotacion = rotacion;
	}
}
