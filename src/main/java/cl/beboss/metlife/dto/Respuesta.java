package cl.beboss.metlife.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.google.gson.Gson;

@XmlRootElement(name="Respuesta")
@XmlType(propOrder={"codError", "msgError", "data"})
public class Respuesta<T> implements Serializable{
	private static final long serialVersionUID = 1L;
	
    private Integer codError;
    private String msgError;
	private T data;
	
	public String getDataJson() {
		String data;
		data = new Gson().toJson(this.data);
		return data;
	}

	/**
	 * Constructor.
	 */
	public Respuesta() {
	}

	/**
	 * Constructor.
	 * 
	 * @param success Si ha sido exitoso.
	 */
	public Respuesta(final int codError, final String msgError) {
		this.setCodError(codError);
		this.setMsgError(msgError);
	}

	/**
	 * Constructor.
	 * 
	 * @param success Si ha sido exitoso.
	 * @param data Data asociada a la respuesta.
	 */
	public Respuesta(final int codError, final String msgError, final T data) {
		this.setCodError(codError);
		this.setMsgError(msgError);
		this.setData(data);
	}

	public Integer getCodError() {
		return codError;
	}

	public void setCodError(Integer codError) {
		this.codError = codError;
	}

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
