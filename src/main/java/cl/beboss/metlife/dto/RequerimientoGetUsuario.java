package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetUsuario implements Serializable{
	
	private String User;
	private String Pass;
	private String IdEmpresa;
	
	public String getUser() {
		return User;
	}
	public void setUser(String user) {
		User = user;
	}
	public String getPass() {
		return Pass;
	}
	public void setPass(String pass) {
		Pass = pass;
	}
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	
	
}
