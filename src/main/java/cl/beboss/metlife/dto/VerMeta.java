package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class VerMeta implements Serializable {

	private String VerMeta;
	private String Nombre;
	private String IdPlanificacion;
	
	public String getVerMeta() {
		return VerMeta;
	}
	public void setVerMeta(String verMeta) {
		VerMeta = verMeta;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getIdPlanificacion() {
		return IdPlanificacion;
	}
	public void setIdPlanificacion(String idPlanificacion) {
		IdPlanificacion = idPlanificacion;
	}
}
