package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class RespuestaGetListaPeriodos implements Serializable{
	List<ListaPeriodos> ListaPeriodos;

	public List<ListaPeriodos> getListaPeriodos() {
		return ListaPeriodos;
	}

	public void setListaPeriodos(List<ListaPeriodos> listaPeriodos) {
		ListaPeriodos = listaPeriodos;
	}
}
