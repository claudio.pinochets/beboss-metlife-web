package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetGUXGAPerfilCorp implements Serializable{
	
	private String IdEmpresa;
	private String IdPersonal;
	private String IdGA;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdGA() {
		return IdGA;
	}
	public void setIdGA(String idGA) {
		IdGA = idGA;
	}
}
