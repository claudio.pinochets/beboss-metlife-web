package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class InfoProdRealPerAses implements Serializable{
	
	private String IdEmpresa;
	private String IdPersonal;
	private String IdAsesor;
	private String IdPeriodo;
	private Polizas Polizas;
	private PrimaBasica PrimaBasica;
	private PrimaExceso PrimaExceso;
	private PrimaEsporadica PrimaEsporadica;
	private TotalClientes TotalClientes;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdAsesor() {
		return IdAsesor;
	}
	public void setIdAsesor(String idAsesor) {
		IdAsesor = idAsesor;
	}
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public Polizas getPolizas() {
		return Polizas;
	}
	public void setPolizas(Polizas polizas) {
		Polizas = polizas;
	}
	public PrimaBasica getPrimaBasica() {
		return PrimaBasica;
	}
	public void setPrimaBasica(PrimaBasica primaBasica) {
		PrimaBasica = primaBasica;
	}
	public PrimaExceso getPrimaExceso() {
		return PrimaExceso;
	}
	public void setPrimaExceso(PrimaExceso primaExceso) {
		PrimaExceso = primaExceso;
	}
	public PrimaEsporadica getPrimaEsporadica() {
		return PrimaEsporadica;
	}
	public void setPrimaEsporadica(PrimaEsporadica primaEsporadica) {
		PrimaEsporadica = primaEsporadica;
	}
	public TotalClientes getTotalClientes() {
		return TotalClientes;
	}
	public void setTotalClientes(TotalClientes totalClientes) {
		TotalClientes = totalClientes;
	}
}
