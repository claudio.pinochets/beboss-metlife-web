package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetAsesoresXGUPerfilCorp implements Serializable{
	
	private String IdEmpresa;
	private String IdPersonal;
	private String IdGU;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdGU() {
		return IdGU;
	}
	public void setIdGU(String idGU) {
		IdGU = idGU;
	}
}
