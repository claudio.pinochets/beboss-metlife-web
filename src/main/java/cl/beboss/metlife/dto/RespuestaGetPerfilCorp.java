package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class RespuestaGetPerfilCorp implements Serializable{

	private List<CumplimientoXRegion> CumplimientoXRegion;
	private List<CumplimientoPeriodoComercial> CumplimientoPeriodoComercial;
	private String PorcentajeDotacion;
	private Autodisciplina Autodisciplina;
	private CalificadosMedioCalif CalificadosMedioCalif;
	private List<InfoGA> InfoGA;
	private List<Personal> Personal;
	
	public List<CumplimientoXRegion> getCumplimientoXRegion() {
		return CumplimientoXRegion;
	}
	public void setCumplimientoXRegion(List<CumplimientoXRegion> cumplimientoXRegion) {
		CumplimientoXRegion = cumplimientoXRegion;
	}
	public List<CumplimientoPeriodoComercial> getCumplimientoPeriodoComercial() {
		return CumplimientoPeriodoComercial;
	}
	public void setCumplimientoPeriodoComercial(List<CumplimientoPeriodoComercial> cumplimientoPeriodoComercial) {
		CumplimientoPeriodoComercial = cumplimientoPeriodoComercial;
	}
	public String getPorcentajeDotacion() {
		return MathUtils.formatoNumerico(PorcentajeDotacion);
	}
	public void setPorcentajeDotacion(String porcentajeDotacion) {
		PorcentajeDotacion = porcentajeDotacion;
	}
	public Autodisciplina getAutodisciplina() {
		return Autodisciplina;
	}
	public void setAutodisciplina(Autodisciplina autodisciplina) {
		Autodisciplina = autodisciplina;
	}
	public CalificadosMedioCalif getCalificadosMedioCalif() {
		return CalificadosMedioCalif;
	}
	public void setCalificadosMedioCalif(CalificadosMedioCalif calificadosMedioCalif) {
		CalificadosMedioCalif = calificadosMedioCalif;
	}
	public List<InfoGA> getInfoGA() {
		return InfoGA;
	}
	public void setInfoGA(List<InfoGA> infoGA) {
		InfoGA = infoGA;
	}
	public List<Personal> getPersonal() {
		return Personal;
	}
	public void setPersonal(List<Personal> personal) {
		Personal = personal;
	}
}
