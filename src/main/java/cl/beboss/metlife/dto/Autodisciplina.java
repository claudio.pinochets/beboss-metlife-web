package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Autodisciplina implements Serializable{
	
	private List<FunnelHoy> FunnelHoy;
	private List<Planificacion> Planificacion;
	private String Cargados;
	private String HabilHoy;
	private String TotalHabil;
	
	public List<FunnelHoy> getFunnelHoy() {
		return FunnelHoy;
	}
	public void setFunnelHoy(List<FunnelHoy> funnelHoy) {
		FunnelHoy = funnelHoy;
	}
	public List<Planificacion> getPlanificacion() {
		return Planificacion;
	}
	public void setPlanificacion(List<Planificacion> planificacion) {
		Planificacion = planificacion;
	}
	public String getCargados() {
		return MathUtils.formatoNumerico(Cargados);
	}
	public void setCargados(String cargados) {
		Cargados = cargados;
	}
	public String getHabilHoy() {
		return HabilHoy;
	}
	public void setHabilHoy(String habilHoy) {
		HabilHoy = habilHoy;
	}
	public String getTotalHabil() {
		return MathUtils.formatoNumerico(TotalHabil);
	}
	public void setTotalHabil(String totalHabil) {
		TotalHabil = totalHabil;
	}
	
	public int getDiasFaltantes() {
		return Integer.parseInt(MathUtils.quitarPunto(this.TotalHabil)) - Integer.parseInt(MathUtils.quitarPunto(this.HabilHoy));
	}
}
