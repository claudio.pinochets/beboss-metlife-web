package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class RespuestaGetPerfilGA implements Serializable{

	private List<CumplimientoPeriodoComercial> CumplimientoPeriodoComercial;
	private String PorcentajeDotacion;
	private Autodisciplina Autodisciplina;
	private CalificadosMedioCalif CalificadosMedioCalif;
	private List<Personal> Personal;
	private List<Dotacion> Dotacion;
	
	public List<CumplimientoPeriodoComercial> getCumplimientoPeriodoComercial() {
		return CumplimientoPeriodoComercial;
	}
	public void setCumplimientoPeriodoComercial(List<CumplimientoPeriodoComercial> cumplimientoPeriodoComercial) {
		CumplimientoPeriodoComercial = cumplimientoPeriodoComercial;
	}
	public String getPorcentajeDotacion() {
		return MathUtils.formatoNumerico(PorcentajeDotacion);
	}
	public void setPorcentajeDotacion(String porcentajeDotacion) {
		PorcentajeDotacion = porcentajeDotacion;
	}
	public Autodisciplina getAutodisciplina() {
		return Autodisciplina;
	}
	public void setAutodisciplina(Autodisciplina autodisciplina) {
		Autodisciplina = autodisciplina;
	}
	public CalificadosMedioCalif getCalificadosMedioCalif() {
		return CalificadosMedioCalif;
	}
	public void setCalificadosMedioCalif(CalificadosMedioCalif calificadosMedioCalif) {
		CalificadosMedioCalif = calificadosMedioCalif;
	}
	public List<Personal> getPersonal() {
		return Personal;
	}
	public void setPersonal(List<Personal> personal) {
		Personal = personal;
	}
	public List<Dotacion> getDotacion() {
		return Dotacion;
	}
	public void setDotacion(List<Dotacion> dotacion) {
		Dotacion = dotacion;
	}
}
