package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class FunnelActividades implements Serializable{
	
	private String IdFunnelActividad;
	private String FunnelActividad;
	private Integer Real; 
	private Integer Plan;
	private Integer Hoy;
	
	public FunnelActividades() {
		this.setHoy(0);
	}
	
	public String getIdFunnelActividad() {
		return IdFunnelActividad;
	}
	public void setIdFunnelActividad(String idFunnelActividad) {
		IdFunnelActividad = idFunnelActividad;
	}
	public String getFunnelActividad() {
		return FunnelActividad;
	}
	public void setFunnelActividad(String funnelActividad) {
		FunnelActividad = funnelActividad;
	}
	public Integer getReal() {
		return Real;
	}
	public void setReal(Integer real) {
		Real = real;
	}
	public Integer getPlan() {
		return Plan;
	}
	public void setPlan(Integer plan) {
		Plan = plan;
	}
	public Integer getHoy() {
		return Hoy;
	}
	public void setHoy(Integer hoy) {
		Hoy = hoy;
	}
}
