package cl.beboss.metlife.dto;

import java.util.List;

public class RespuestaGetGUXGAPerfilCorp {
	private List<InfoSubAlterno> InfoSubAlterno;

	public List<InfoSubAlterno> getInfoSubAlterno() {
		return InfoSubAlterno;
	}

	public void setInfoSubAlterno(List<InfoSubAlterno> infoSubAlterno) {
		InfoSubAlterno = infoSubAlterno;
	}
}
