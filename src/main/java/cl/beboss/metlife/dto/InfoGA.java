package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class InfoGA implements Serializable{

	private Integer IdPersonal;
	private String Nombres;
	private String  Apellidos;
	private Integer IdPerfil;
	private String Perfil;
	private List<Cumplimiento> Cumplimiento;
	private String RotacionPersonal;
	private String NAP;
	
	public String getNombreCompleto() {
		return this.Nombres + " " + this.Apellidos;
	}
	
	public Integer getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(Integer idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public Integer getIdPerfil() {
		return IdPerfil;
	}
	public void setIdPerfil(Integer idPerfil) {
		IdPerfil = idPerfil;
	}
	public String getPerfil() {
		return Perfil;
	}
	public void setPerfil(String perfil) {
		Perfil = perfil;
	}
	public List<Cumplimiento> getCumplimiento() {
		return Cumplimiento;
	}
	public void setCumplimiento(List<Cumplimiento> cumplimiento) {
		Cumplimiento = cumplimiento;
	}

	public String getRotacionPersonal() {
		return RotacionPersonal;
	}

	public void setRotacionPersonal(String rotacionPersonal) {
		RotacionPersonal = rotacionPersonal;
	}

	public String getNAP() {
		return NAP;
	}

	public void setNAP(String nAP) {
		NAP = nAP;
	}
	
}
