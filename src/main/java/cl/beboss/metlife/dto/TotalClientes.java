package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class TotalClientes implements Serializable{
	private String Nombre;
	private String Meta;
	private String Real;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMeta() {
		return Meta;
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return Real;
	}
	public void setReal(String real) {
		Real = real;
	}
}
