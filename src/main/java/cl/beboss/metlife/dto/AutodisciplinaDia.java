package cl.beboss.metlife.dto;

public class AutodisciplinaDia {

	private String dia;
	private String estado;
	
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
}
