package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ListaPeriodos implements Serializable {
	private String IdPeriodo;
	private String Periodo;
	private String FechaInicio;
	private String FechaFin;
	
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public String getPeriodo() {
		return Periodo;
	}
	public void setPeriodo(String periodo) {
		Periodo = periodo;
	}
	public String getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return FechaFin;
	}
	public void setFechaFin(String fechaFin) {
		FechaFin = fechaFin;
	}
}
