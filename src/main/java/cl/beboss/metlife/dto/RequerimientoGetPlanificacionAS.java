package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetPlanificacionAS implements Serializable{
	private Integer IdPlan;

	public Integer getIdPlan() {
		return IdPlan;
	}

	public void setIdPlan(Integer idPlan) {
		IdPlan = idPlan;
	}
}
