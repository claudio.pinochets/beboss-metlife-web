package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class BCPU implements Serializable {

	private String Nombre;
	private String Valor;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getValor() {
		return Valor;
	}
	public void setValor(String valor) {
		Valor = valor;
	}
}
