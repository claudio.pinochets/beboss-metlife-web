package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Cumplimiento implements Serializable{
	
	private String Nombre;
	private String Meta;
	private String Real;
	private String PPTO;
	private String Estado;
	private String Cumple;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getMeta() {
		return Meta;
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return Real;
	}
	public void setReal(String real) {
		Real = real;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getCumple() {
		return Cumple;
	}
	public void setCumple(String cumple) {
		Cumple = cumple;
	}
	public String getPPTO() {
		return PPTO;
	}
	public void setPPTO(String pPTO) {
		PPTO = pPTO;
	}
}
