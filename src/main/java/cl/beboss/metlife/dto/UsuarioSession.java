package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class UsuarioSession implements Serializable{
		
	private String IdUsuarioSys;
	private String IdPersonal;
    private String Identificador;
    private String Nombres;
    private String Apellidos;
    private String Fono;
    private String Mail;
    private String IdEmpresa;
    private String Empresa;
    private String IdPerfil;
    private String Perfil;
    private String Estado;
    private String CodError;
    private String MsgError;
    private String token;
    
    public String getNombreCompletoUsuario() {
    	return this.getNombres() + " " + this.getApellidos();
    }
    
	public String getIdUsuarioSys() {
		return IdUsuarioSys;
	}
	public void setIdUsuarioSys(String idUsuarioSys) {
		IdUsuarioSys = idUsuarioSys;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdentificador() {
		return Identificador;
	}
	public void setIdentificador(String identificador) {
		Identificador = identificador;
	}
	public String getNombres() {
		return Nombres;
	}
	public void setNombres(String nombres) {
		Nombres = nombres;
	}
	public String getApellidos() {
		return Apellidos;
	}
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	public String getFono() {
		return Fono;
	}
	public void setFono(String fono) {
		Fono = fono;
	}
	public String getMail() {
		return Mail;
	}
	public void setMail(String mail) {
		Mail = mail;
	}
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getEmpresa() {
		return Empresa;
	}
	public void setEmpresa(String empresa) {
		Empresa = empresa;
	}
	public String getIdPerfil() {
		return IdPerfil;
	}
	public void setIdPerfil(String idPerfil) {
		IdPerfil = idPerfil;
	}
	public String getPerfil() {
		return Perfil;
	}
	public void setPerfil(String perfil) {
		Perfil = perfil;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public String getCodError() {
		return CodError;
	}
	public void setCodError(String codError) {
		CodError = codError;
	}
	public String getMsgError() {
		return MsgError;
	}
	public void setMsgError(String msgError) {
		MsgError = msgError;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
