package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class AutodisciplinaPorDia implements Serializable{

	private List<AutodisciplinaDia> autodisciplinaDia;

	public List<AutodisciplinaDia> getAutodisciplinaDia() {
		return autodisciplinaDia;
	}

	public void setAutodisciplinaDia(List<AutodisciplinaDia> autodisciplinaDia) {
		this.autodisciplinaDia = autodisciplinaDia;
	}
}
