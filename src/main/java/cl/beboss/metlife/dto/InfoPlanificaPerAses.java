package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class InfoPlanificaPerAses implements Serializable{

	private String IdEmpresa;
	private String IdPersonal;
	private String IdAsesor;
	private String IdPeriodo;
	private String IdEstadoPlan;
	private String EstadoPlan;
	private List<Productos> Productos;
	private String TotalClientes;
	private String PrimaEsporadica;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdAsesor() {
		return IdAsesor;
	}
	public void setIdAsesor(String idAsesor) {
		IdAsesor = idAsesor;
	}
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public String getIdEstadoPlan() {
		return IdEstadoPlan;
	}
	public void setIdEstadoPlan(String idEstadoPlan) {
		IdEstadoPlan = idEstadoPlan;
	}
	public String getEstadoPlan() {
		return EstadoPlan;
	}
	public void setEstadoPlan(String estadoPlan) {
		EstadoPlan = estadoPlan;
	}
	public List<Productos> getProductos() {
		return Productos;
	}
	public void setProductos(List<Productos> productos) {
		Productos = productos;
	}
	public String getTotalClientes() {
		return MathUtils.formatoNumerico(TotalClientes);
	}
	public void setTotalClientes(String totalClientes) {
		TotalClientes = totalClientes;
	}
	public String getPrimaEsporadica() {
		return MathUtils.formatoNumericoDecimal(PrimaEsporadica);
	}
	public void setPrimaEsporadica(String primaEsporadica) {
		PrimaEsporadica = primaEsporadica;
	}
}
