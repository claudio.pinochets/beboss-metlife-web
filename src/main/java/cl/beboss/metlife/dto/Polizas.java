package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

import cl.beboss.metlife.utils.MathUtils;

@SuppressWarnings("serial")
public class Polizas implements Serializable{
	private String Nombre;
	private String Meta;
	private String Real;
	List<Productos> Productos;
	
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getMeta() {
		return MathUtils.formatoNumerico(Meta);
	}
	public void setMeta(String meta) {
		Meta = meta;
	}
	public String getReal() {
		return MathUtils.formatoNumerico(Real);
	}
	public void setReal(String real) {
		Real = real;
	}
	public List<Productos> getProductos() {
		return Productos;
	}
	public void setProductos(List<Productos> productos) {
		Productos = productos;
	}
}
