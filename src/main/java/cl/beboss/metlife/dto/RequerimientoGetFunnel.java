package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoGetFunnel implements Serializable{

	private String IdEmpresa;
	private String IdPersonal;
	private String IdSubPeriodo;
	private String Token;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	
	public String getIdSubPeriodo() {
		return IdSubPeriodo;
	}
	public void setIdSubPeriodo(String idSubPeriodo) {
		IdSubPeriodo = idSubPeriodo;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
}
