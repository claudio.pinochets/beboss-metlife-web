package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class FunnelActXSubPeriodo implements Serializable{

	private String IdSubPeriodo;
	private List<FunnelDiario> FunnelActividades;
	private List<RatioActividadComercial> RatioActividadComercial;
	
	public String getIdSubPeriodo() {
		return IdSubPeriodo;
	}
	public void setIdSubPeriodo(String idSubPeriodo) {
		IdSubPeriodo = idSubPeriodo;
	}
	public List<FunnelDiario> getFunnelActividades() {
		return FunnelActividades;
	}
	public void setFunnelActividades(List<FunnelDiario> funnelActividades) {
		FunnelActividades = funnelActividades;
	}
	public List<RatioActividadComercial> getRatioActividadComercial() {
		return RatioActividadComercial;
	}
	public void setRatioActividadComercial(List<RatioActividadComercial> ratioActividadComercial) {
		RatioActividadComercial = ratioActividadComercial;
	}
}
