package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class PeriodoVigente implements Serializable{
	
	private String IdPeriodo;
	private String FechaInicio;
	private String FechaFin;
	private List<SubPeriodos> SubPeriodos;
	
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public String getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return FechaFin;
	}
	public void setFechaFin(String fechaFin) {
		FechaFin = fechaFin;
	}
	public List<SubPeriodos> getSubPeriodos() {
		return SubPeriodos;
	}
	public void setSubPeriodos(List<SubPeriodos> subPeriodos) {
		SubPeriodos = subPeriodos;
	}
}
