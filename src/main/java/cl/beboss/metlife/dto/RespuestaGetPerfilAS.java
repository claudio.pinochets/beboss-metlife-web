package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class RespuestaGetPerfilAS implements Serializable{

	private String IdEmpresa;
	private String IdPersonal;
	private String  IdPeriodo;
	private List<Bonos> Bonos;
	private TramoAsesor TramoAsesor;
	private LoQueLlevo LoQueLlevo;
	private LoQueLlevo LoQueMeFalta;
	private Autodisciplina Autodisciplina;
	private List<CumplimientoPeriodoComercial> Cumplimiento;
	private AutodisciplinaPorDia AutodisciplinaPorDia;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public List<Bonos> getBonos() {
		return Bonos;
	}
	public void setBonos(List<Bonos> bonos) {
		Bonos = bonos;
	}
	public TramoAsesor getTramoAsesor() {
		return TramoAsesor;
	}
	public void setTramoAsesor(TramoAsesor tramoAsesor) {
		TramoAsesor = tramoAsesor;
	}
	public LoQueLlevo getLoQueLlevo() {
		return LoQueLlevo;
	}
	public void setLoQueLlevo(LoQueLlevo loQueLlevo) {
		LoQueLlevo = loQueLlevo;
	}
	public LoQueLlevo getLoQueMeFalta() {
		return LoQueMeFalta;
	}
	public void setLoQueMeFalta(LoQueLlevo loQueMeFalta) {
		LoQueMeFalta = loQueMeFalta;
	}
	public Autodisciplina getAutodisciplina() {
		return Autodisciplina;
	}
	public void setAutodisciplina(Autodisciplina autodisciplina) {
		Autodisciplina = autodisciplina;
	}
	public List<CumplimientoPeriodoComercial> getCumplimiento() {
		return Cumplimiento;
	}
	public void setCumplimiento(List<CumplimientoPeriodoComercial> cumplimiento) {
		Cumplimiento = cumplimiento;
	}
	public AutodisciplinaPorDia getAutodisciplinaPorDia() {
		return AutodisciplinaPorDia;
	}
	public void setAutodisciplinaPorDia(AutodisciplinaPorDia autodisciplinaPorDia) {
		AutodisciplinaPorDia = autodisciplinaPorDia;
	}
}
