package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class CargaPlanificaPerAses implements Serializable{

	private String IdEmpresa;
	private String IdPersonal;
	private String IdAsesor;
	private String IdPeriodo;
	private String IdEstadoPlan;
	private String FormaIngreso;
	private String TotalClientes;
	private String PrimaEsporadica;
	private String Token;
	private List<SubProductos> SubProductos;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdAsesor() {
		return IdAsesor;
	}
	public void setIdAsesor(String idAsesor) {
		IdAsesor = idAsesor;
	}
	public String getIdPeriodo() {
		return IdPeriodo;
	}
	public void setIdPeriodo(String idPeriodo) {
		IdPeriodo = idPeriodo;
	}
	public List<SubProductos> getSubProductos() {
		return SubProductos;
	}
	public void setSubProductos(List<SubProductos> subProductos) {
		SubProductos = subProductos;
	}
	public String getIdEstadoPlan() {
		return IdEstadoPlan;
	}
	public void setIdEstadoPlan(String idEstadoPlan) {
		IdEstadoPlan = idEstadoPlan;
	}
	public String getFormaIngreso() {
		return FormaIngreso;
	}
	public void setFormaIngreso(String formaIngreso) {
		FormaIngreso = formaIngreso;
	}
	public String getTotalClientes() {
		return TotalClientes;
	}
	public void setTotalClientes(String totalClientes) {
		TotalClientes = totalClientes;
	}
	public String getPrimaEsporadica() {
		return PrimaEsporadica;
	}
	public void setPrimaEsporadica(String primaEsporadica) {
		PrimaEsporadica = primaEsporadica;
	}
	public String getToken() {
		return Token;
	}
	public void setToken(String token) {
		Token = token;
	}
}
