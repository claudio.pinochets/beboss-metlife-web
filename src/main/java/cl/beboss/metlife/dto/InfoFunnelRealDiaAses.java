package cl.beboss.metlife.dto;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class InfoFunnelRealDiaAses implements Serializable{
	
	private String IdEmpresa;
	private String IdPersonal;
	private String IdAsesor;
	private String Fecha;
	private List<FunnelActividades> FunnelActividades;
	
	public String getIdEmpresa() {
		return IdEmpresa;
	}
	public void setIdEmpresa(String idEmpresa) {
		IdEmpresa = idEmpresa;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getIdAsesor() {
		return IdAsesor;
	}
	public void setIdAsesor(String idAsesor) {
		IdAsesor = idAsesor;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public List<FunnelActividades> getFunnelActividades() {
		return FunnelActividades;
	}
	public void setFunnelActividades(List<FunnelActividades> funnelActividades) {
		FunnelActividades = funnelActividades;
	}
}
