package cl.beboss.metlife.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class RequerimientoApruebaRechazaPlan implements Serializable {
	private String IdPlan;
	private String IdPersonal;
	private String Estado;
	
	public String getIdPlan() {
		return IdPlan;
	}
	public void setIdPlan(String idPlan) {
		IdPlan = idPlan;
	}
	public String getIdPersonal() {
		return IdPersonal;
	}
	public void setIdPersonal(String idPersonal) {
		IdPersonal = idPersonal;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
}
