package cl.beboss.metlife.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.model.chart.DonutChartModel;
import org.primefaces.model.chart.PieChartModel;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartOptions;

import com.google.gson.Gson;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.AutodisciplinaPorDia;
import cl.beboss.metlife.dto.BCPU;
import cl.beboss.metlife.dto.Cumplimiento;
import cl.beboss.metlife.dto.CumplimientoPeriodoComercial;
import cl.beboss.metlife.dto.CumplimientoXRegion;
import cl.beboss.metlife.dto.Dotacion;
import cl.beboss.metlife.dto.FunnelHoy;
import cl.beboss.metlife.dto.InfoGA;
import cl.beboss.metlife.dto.InfoSubAlterno;
import cl.beboss.metlife.dto.PeriodoVigente;
import cl.beboss.metlife.dto.PlanAsesor;
import cl.beboss.metlife.dto.Planificacion;
import cl.beboss.metlife.dto.RespuestaGetGUXGAPerfilCorp;
import cl.beboss.metlife.dto.RespuestaGetPerfilAS;
import cl.beboss.metlife.dto.RespuestaGetPerfilCorp;
import cl.beboss.metlife.dto.RespuestaGetPerfilGA;
import cl.beboss.metlife.dto.RespuestaGetPerfilGU;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.enums.Regiones;
import cl.beboss.metlife.services.CargaDatosAsService;
import cl.beboss.metlife.services.PerfilService;
import cl.beboss.metlife.utils.MathUtils;
import cl.beboss.metlife.vo.MapaPerfil;
import cl.beboss.metlife.vo.TablaPerfil;

@ManagedBean(name="perfilBean")
@SessionScoped
public class PerfilBean extends BaseViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger( PerfilBean.class.getName() );
	
	private FacesContext facesContext = FacesContext.getCurrentInstance();
	private PerfilService perfilService = CDI.current().select(PerfilService.class).get();
	private CargaDatosAsService cargaDatosAsService = CDI.current().select(CargaDatosAsService.class).get();
	
	private List<UsuarioSession> listUsuario;
	private RespuestaGetPerfilCorp respuestaGetPerfilCorp;
	private List<InfoGA> listInfoGA;
	private RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp;
	private RespuestaGetPerfilGA respuestaGetPerfilGA;
	private RespuestaGetPerfilGU respuestaGetPerfilGU;
	private RespuestaGetPerfilAS respuestaGetPerfilAS;
	
	private String imageUrl;  
	 
	private PieChartModel planPieModel;
	private PieChartModel funPieModel;
	private PieChartModel calPieModel;
	
	private DonutChartModel donutPlanPieModel;
	private DonutChartModel donutFunPieModel;
	 
	private BarChartModel barCharPolizas;
	 
	private CumplimientoPeriodoComercial cumplimientoPoliza;
	private CumplimientoPeriodoComercial cumplimientoPB;
	private CumplimientoPeriodoComercial cumplimientoPE;
	 
	private List<MapaPerfil> listCumplimiento;
	private List<MapaPerfil> listBajoCumplimiento;
	private List<MapaPerfil> listSinSucursal;
	
	private String jsonCumplimiento;
	private String jsonBajoCumplimiento;
	private String jsonSinSucursal;
	
	private List<TablaPerfil> listTablaPerfil;
	private List<TablaPerfil> listTablaPerfilGU;
	private List<TablaPerfil> listTablaPerfilAsesor;
	
	private TablaPerfil tablaPerfilSelect;
	private TablaPerfil tablaPerfilChildSelect;
	private String paginaPerfil;
	private Dotacion dotacionActivos;
	private Dotacion dotacionTotales;
	private Integer idPersonalSelect;
	private String estadoSelect;
	private String estadoNuevoSelect;
	private String supervisorSelect;
	
	private String bcpuMeta;
	private String bcpuReal;
	private String bcpuEstilo;
	private PlanAsesor planAsesor;
	private String claveActual;
	private String claveNueva;
	private String claveNuevaR;
	private int total;
	private String respuestaSueldoMesActualAses;
	private String respuestaSueldoArrastreAses;
	private PeriodoVigente periodoVigente;
	private boolean verDetalleTableroSueldo = false;
	private String autodisciplinaPorDiaJson;
	
	@PostConstruct
    public void initPerfilBean() {
		log.info("initPerfilBean");
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		this.usuario = (UsuarioSession) session.getAttribute("usuario");
		this.usuarioDrillDown = (UsuarioSession) session.getAttribute("usuarioDrillDown");
		
		if (this.usuarioDrillDown != null) {
			cargaInicialPerfilDrillDown();
		} else {
			cargaInicialPerfil();
		}
    }
	
	public void cambioClave() {
		if(!this.claveNueva.equalsIgnoreCase(this.claveNuevaR)) {
			addMessage("Clave nueva no coincide con repetición");
		}else {
			try {
				perfilService.cambioClaveUsuario(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.usuario.getMail(), this.claveActual, this.claveNueva, this.usuario.getToken());
				addMessage("Clave modificada con éxito");
			} catch (Exception e) {
				addMessage("No fue posible modificar la clave, datos ingresador no validos");
				log.info(e,e);
			}
		}
	}
	
	public void cargaInicialPerfil() {
		if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilCorp)) {
			this.paginaPerfil = "corporativo.xhtml";
			
			try {
				this.respuestaGetPerfilCorp = perfilService.getListaPerfilCorp(this.usuario, "");
			} catch (Exception e) {
				log.info(e,e);
			}
			createPlanPieModel();
			createFunPieModel();
			createCalPieModel();
			createMixedModel();
			completaCumplimiento();
			completaMapa();
			cargaListadoUsuarios();
		} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
			this.paginaPerfil = "ga.xhtml";
			try {
				this.respuestaGetPerfilGA = perfilService.getPerfilGA(this.usuario);
			} catch (Exception e) {
				log.info(e,e);
			}
			createCalPieModelGA();
			createPlanPieModelGA();
			createFunPieModelGA();
			completaCumplimientoGA();
			cargaListadoUsuariosGA();
		} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
			this.paginaPerfil = "gu.xhtml";
			try {
				this.respuestaGetPerfilGU = perfilService.getPerfilGU(this.usuario);
			} catch (Exception e) {
				log.info(e,e);
			}
			createCalPieModelGU();
			createPlanPieModelGU();
			createFunPieModelGU();
			completaCumplimientoGU();
			cargaListadoUsuariosGU();
		} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
				|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
			this.paginaPerfil = "asesor.xhtml";
			try {
				this.respuestaGetPerfilAS = perfilService.getPerfilAS(this.usuario);
			} catch (Exception e) {
				log.info(e,e);
			}
			completaTramo();
			completaCumplimientoAS();
			completaSueldo();
			completaAutoDisciplinaAS();
		}
	}
		
	public void verDetalleTableroSueldo() {
		try {
			if (this.verDetalleTableroSueldo) {
				this.verDetalleTableroSueldo = false;
			}else {
				this.verDetalleTableroSueldo = true;
			}
		
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void completaAutoDisciplinaAS() {
		try {
			AutodisciplinaPorDia autodisciplinaPorDia = this.respuestaGetPerfilAS.getAutodisciplinaPorDia();
			this.autodisciplinaPorDiaJson = new Gson().toJson(autodisciplinaPorDia);
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void completaSueldo() {
		try {
			if (this.usuarioDrillDown != null) {
				this.periodoVigente = cargaDatosAsService.getPeriodoVigente(this.usuarioDrillDown.getIdEmpresa(), this.usuarioDrillDown.getToken());
				this.respuestaSueldoMesActualAses = perfilService.getSueldoMesActualAses(this.usuarioDrillDown.getIdEmpresa(), this.usuarioDrillDown.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuarioDrillDown.getToken());
				this.respuestaSueldoArrastreAses = perfilService.getSueldoArrastreAses(this.usuarioDrillDown.getIdEmpresa(), this.usuarioDrillDown.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuarioDrillDown.getToken());
			} else {
				this.periodoVigente = cargaDatosAsService.getPeriodoVigente(this.usuario.getIdEmpresa(), this.usuario.getToken());
				this.respuestaSueldoMesActualAses = perfilService.getSueldoMesActualAses(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuario.getToken());
				this.respuestaSueldoArrastreAses = perfilService.getSueldoArrastreAses(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuario.getToken());
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public int restaTramo(int porcentaje) {
		int resultado = total - porcentaje;
		if (resultado >= 0) {
			resultado = 100;
		}
		if (resultado < 0) {
			resultado = 0;
		}
		return resultado;
	}
	
	public void completaTramo() {
		String tramo = this.respuestaGetPerfilAS.getTramoAsesor().getTramo();
		try {
			if(tramo.equalsIgnoreCase("0")) {
				total = Integer.parseInt(MathUtils.quitarPunto(this.respuestaGetPerfilAS.getTramoAsesor().getAvance()));
			}else {	
				for (int i=0;i<=this.respuestaGetPerfilAS.getBonos().size()-1;i++) {
					if(this.respuestaGetPerfilAS.getBonos().get(i).getTramo().equalsIgnoreCase(tramo)) {
						total = total + 100 + Integer.parseInt(MathUtils.quitarPunto(this.respuestaGetPerfilAS.getTramoAsesor().getAvance()));
						break;
					}else {
						total = total + 100;
					}
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void cargaInicialPerfilDrillDown() {
		if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
			this.paginaPerfil = "ga.xhtml";
			try {
				this.respuestaGetPerfilGA = perfilService.getPerfilGA(this.usuarioDrillDown);
			} catch (Exception e) {
				log.info(e,e);
			}
			createCalPieModelGA();
			createPlanPieModelGA();
			createFunPieModelGA();
			completaCumplimientoGA();
			cargaListadoUsuariosGA();
		} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
			this.paginaPerfil = "gu.xhtml";
			try {
				this.respuestaGetPerfilGU = perfilService.getPerfilGU(this.usuarioDrillDown);
			} catch (Exception e) {
				log.info(e,e);
			}
			createCalPieModelGU();
			createPlanPieModelGU();
			createFunPieModelGU();
			completaCumplimientoGU();
			cargaListadoUsuariosGU();
		} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor)) {
			this.paginaPerfil = "asesor.xhtml";
			try {
				this.respuestaGetPerfilAS = perfilService.getPerfilAS(this.usuarioDrillDown);
			} catch (Exception e) {
				log.info(e,e);
			}
			completaTramo();
			completaCumplimientoAS();
			completaSueldo();
		}
	}
	
	public void drillDownPerfil(TablaPerfil tablaPerfil) {
		try {
			usuarioDrillDown = new UsuarioSession();
			usuarioDrillDown.setIdPersonal(tablaPerfil.getIdPersonal().toString());
			usuarioDrillDown.setIdEmpresa(this.usuario.getIdEmpresa());
			usuarioDrillDown.setNombres(tablaPerfil.getNombre());
			usuarioDrillDown.setPerfil(tablaPerfil.getPerfilActual());
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
			session.setAttribute("usuarioDrillDown", usuarioDrillDown);
			cargaInicialPerfilDrillDown();
		} catch (Exception e) {
			log.error(e,e);
		}
	}
	
	public void expandChildActionGA(TablaPerfil tablaPerfil) {
		log.info("expandChildAction");
		
		if (this.tablaPerfilChildSelect !=null 
					&& this.tablaPerfilChildSelect.getIdPersonal() == tablaPerfil.getIdPersonal()) {
			this.tablaPerfilChildSelect = null;
		}else {
			this.tablaPerfilChildSelect = tablaPerfil;
			
			Cumplimiento cumplimientoPoliza = new Cumplimiento();
	        Cumplimiento cumplimientoPb = new Cumplimiento();
	        Cumplimiento cumplimientoPe = new Cumplimiento();
	        
	        String perfilActual = "AS";
	        String icono = "icons-Triangle-ArrowDown";
	    	String estilo = "hiddenRow";
	    	String accion = null;
	    	boolean habilitado = true;
	    	Integer idPlanificacion = null;
	    	
	    	if(this.listTablaPerfilAsesor!=null && listTablaPerfilAsesor.size()>0) {
	    		this.listTablaPerfilAsesor.clear();
			}else {
				this.listTablaPerfilAsesor = new LinkedList<TablaPerfil>();
			}
			
			try {
	    		this.respuestaGetGUXGAPerfilCorp = perfilService.getListaAsesoresXGUPerfilGA(usuario, this.tablaPerfilChildSelect.getIdPersonal());
				for (InfoSubAlterno infoSubAlterno:this.respuestaGetGUXGAPerfilCorp.getInfoSubAlterno()) {
					
					for(Cumplimiento com:infoSubAlterno.getCumplimiento()) {
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
		        			cumplimientoPoliza.setMeta(com.getMeta()); 
		        			cumplimientoPoliza.setReal(com.getReal());
		        			cumplimientoPoliza.setEstado(com.getEstado());
		        			cumplimientoPoliza.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PB")) {
		        			cumplimientoPb.setMeta(com.getMeta()); 
		        			cumplimientoPb.setReal(com.getReal());
		        			cumplimientoPb.setEstado(com.getEstado());
		        			cumplimientoPb.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PE")) {
		        			cumplimientoPe.setMeta(com.getMeta()); 
		        			cumplimientoPe.setReal(com.getReal());
		        			cumplimientoPe.setEstado(com.getEstado());
		        			cumplimientoPe.setPPTO(com.getPPTO());
		        		}
		        	}
					
					if (infoSubAlterno.getVerMeta() != null) {
						if (infoSubAlterno.getVerMeta().getVerMeta() != null) {
							if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("O")) {
								accion = "";
							}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("S")) {
								accion = "Sin meta";
							}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("E")) {
								accion = "Ver meta";
								idPlanificacion = Integer.parseInt(MathUtils.quitarPunto(infoSubAlterno.getVerMeta().getIdPlanificacion()));
							}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("A")) {
								accion = "Meta aprovada";
								
							}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("R")) {
								accion = "Meta rechazada";
							}
						}
					}
					
					if (infoSubAlterno.getEstado() != null) {
						if (infoSubAlterno.getEstado().equalsIgnoreCase("A")) {
							habilitado = true;
						}else {
							habilitado = false;
						}
					}
								
					TablaPerfil tp = new TablaPerfil(infoSubAlterno.getIdPersonal(), 
							infoSubAlterno.getNombreCompleto(), 
							infoSubAlterno.getBCPU(), 
							infoSubAlterno.getRotacionPersonal(), 
							null, 
							null, 
							null,
							null,
							cumplimientoPoliza.getReal(), 
							cumplimientoPoliza.getMeta(), 
							cumplimientoPoliza.getPPTO(),
							cumplimientoPoliza.getEstado(),
		        			cumplimientoPb.getReal(), 
		        			cumplimientoPb.getMeta(), 
		        			cumplimientoPb.getPPTO(),
		        			cumplimientoPb.getEstado(),
		        			cumplimientoPe.getReal(), 
		        			cumplimientoPe.getMeta(), 
		        			cumplimientoPe.getPPTO(),
		        			cumplimientoPe.getEstado(),
		        			icono, estilo, perfilActual, 
		        			null, accion, habilitado,
		        			infoSubAlterno.getTramo(), 
		        			infoSubAlterno.getAntiguedad(),
		        			idPlanificacion);
					
					listTablaPerfilAsesor.add(tp);
	        	}
			} catch (Exception e) {
				log.error(e,e);
			}
		}
	}
	
	public void verMeta(Integer idPlan) {
		log.info("verMeta:" + idPlan);
		
		if (idPlan != null) {
			try {
				this.planAsesor = perfilService.getPlanificacionAsesor(usuario, idPlan);
			} catch (Exception e) {
				addMessage("Ocurrió un error al buscar planificación");
				log.error(e,e);
			}
		}
	}
	
	public void apruebaPlan() {
		log.info("apruebaPlan");
		boolean respuesta = false;
		 
		if (this.planAsesor != null) {
			try {
				 respuesta = perfilService.apruebaRechazaPlan(usuario, this.planAsesor, "A");
			} catch (Exception e) {
				addMessage("Ocurrió un error al aprobar planificación");
				log.error(e,e);
			}
			if (respuesta) {
				addMessage("Planificación aprobada con éxito");
			}else {
				addMessage("Ocurrió un error al aprobar planificación");
			}
		}
	}
	
	public void rechazaPlan() {
		log.info("rechazaPlan");
		boolean respuesta = false;
		
		if (this.planAsesor != null) {
			try {
				 respuesta = perfilService.apruebaRechazaPlan(usuario, this.planAsesor, "R");
			} catch (Exception e) {
				addMessage("Ocurrió un error al aprobar planificación");
				log.error(e,e);
			}
			if (respuesta) {
				addMessage("Planificación rechazada con éxito");
			}else {
				addMessage("Ocurrió un error al rechazar planificación");
			}
		}
	}
	
	public void habilitaDeshabilitaGU() {
		log.info("habilitaDeshabilita");
		log.info("idPersonalSelect" + this.idPersonalSelect);
		log.info("estadoSelect" + this.estadoSelect);
		log.info("estadoNuevoSelect" + this.estadoNuevoSelect);
		
		if (this.idPersonalSelect != null) {
			try {
				boolean resultado = perfilService.cambiaEstadoUsuario(usuario, usuario.getIdPersonal(), String.valueOf(this.idPersonalSelect), this.estadoSelect, this.estadoNuevoSelect);
				if (resultado) {
					addMessage("Estado actualizado con éxito");
				}else {
					addMessage("No fue posible modificar el estado del usuario");
				}
			} catch (Exception e) {
				addMessage("Ocurrió un error an intentar modificar el estado del usuario");
				log.error(e,e);
			}
		}
	}
	
	public void habilitaDeshabilitaAS() {
		log.info("habilitaDeshabilita");
		log.info("idPersonalSelect" + this.idPersonalSelect);
		log.info("estadoSelect" + this.estadoSelect);
		log.info("estadoNuevoSelect" + this.estadoNuevoSelect);
		log.info("supervisorSelect" + this.supervisorSelect);
		
		if (this.idPersonalSelect != null) {
			try {
				boolean resultado = perfilService.cambiaEstadoUsuario(usuario, supervisorSelect, String.valueOf(this.idPersonalSelect), this.estadoSelect, this.estadoNuevoSelect);
				if (resultado) {
					addMessage("Estado actualizado con éxito");
				}else {
					addMessage("Ocurrió un error an intentar modificar el estado del usuario");
				}
			} catch (Exception e) {
				addMessage("Ocurrió un error an intentar modificar el estado del usuario");
				log.error(e,e);
			}
		}
	}
	
	public void expandChildAction(TablaPerfil tablaPerfil) {
		log.info("expandChildAction");
		
		if (this.tablaPerfilChildSelect !=null 
					&& this.tablaPerfilChildSelect.getIdPersonal() == tablaPerfil.getIdPersonal()) {
			this.tablaPerfilChildSelect = null;
		}else {
			this.tablaPerfilChildSelect = tablaPerfil;
			
			Cumplimiento cumplimientoPoliza = new Cumplimiento();
	        Cumplimiento cumplimientoPb = new Cumplimiento();
	        Cumplimiento cumplimientoPe = new Cumplimiento();
	        
	        String perfilActual = "AS";
	        String icono = "icons-Triangle-ArrowDown";
	    	String estilo = "hiddenRow";
	    	
	    	if(this.listTablaPerfilAsesor!=null && listTablaPerfilAsesor.size()>0) {
	    		this.listTablaPerfilAsesor.clear();
			}else {
				this.listTablaPerfilAsesor = new LinkedList<TablaPerfil>();
			}
			
			try {
	    		this.respuestaGetGUXGAPerfilCorp = perfilService.getListaAsesoresXGUPerfilCorp(usuario, this.tablaPerfilChildSelect.getIdPersonal());
				for (InfoSubAlterno infoSubAlterno:this.respuestaGetGUXGAPerfilCorp.getInfoSubAlterno()) {
					
					for(Cumplimiento com:infoSubAlterno.getCumplimiento()) {
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
		        			cumplimientoPoliza.setMeta(com.getMeta()); 
		        			cumplimientoPoliza.setReal(com.getReal());
		        			cumplimientoPoliza.setEstado(com.getEstado());
		        			cumplimientoPoliza.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PB")) {
		        			cumplimientoPb.setMeta(com.getMeta()); 
		        			cumplimientoPb.setReal(com.getReal());
		        			cumplimientoPb.setEstado(com.getEstado());
		        			cumplimientoPb.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PE")) {
		        			cumplimientoPe.setMeta(com.getMeta()); 
		        			cumplimientoPe.setReal(com.getReal());
		        			cumplimientoPe.setEstado(com.getEstado());
		        			cumplimientoPe.setPPTO(com.getPPTO());
		        		}
		        	}
					
					TablaPerfil tp = new TablaPerfil(infoSubAlterno.getIdPersonal(), 
							infoSubAlterno.getNombreCompleto(), 
							infoSubAlterno.getBCPU(), 
							infoSubAlterno.getRotacionPersonal(), 
							null, 
							null, 
							null,
							null,
							cumplimientoPoliza.getReal(), 
							cumplimientoPoliza.getMeta(), 
							cumplimientoPoliza.getPPTO(),
							cumplimientoPoliza.getEstado(),
		        			cumplimientoPb.getReal(), 
		        			cumplimientoPb.getMeta(), 
		        			cumplimientoPb.getPPTO(),
		        			cumplimientoPb.getEstado(),
		        			cumplimientoPe.getReal(), 
		        			cumplimientoPe.getMeta(), 
		        			cumplimientoPe.getPPTO(),
		        			cumplimientoPe.getEstado(),
		        			icono, estilo, perfilActual, 
		        			null, null, true, null, null, null);
					
					listTablaPerfilAsesor.add(tp);
	        	}
			} catch (Exception e) {
				log.error(e,e);
			}
		}
	}
	
	public void expandAction(TablaPerfil tablaPerfil) {
		log.info("expandAction");
		
		this.tablaPerfilChildSelect = null; 
		if (this.tablaPerfilSelect !=null 
					&& this.tablaPerfilSelect.getIdPersonal() == tablaPerfil.getIdPersonal()) {
			this.tablaPerfilSelect = null;
		}else {
			this.tablaPerfilSelect = tablaPerfil;
			
			Cumplimiento cumplimientoPoliza = new Cumplimiento();
	        Cumplimiento cumplimientoPb = new Cumplimiento();
	        Cumplimiento cumplimientoPe = new Cumplimiento();
	        
	        String verPerfil = "VER Asesores";
	        String perfilActual = "GU";
	        String icono = "icons-Triangle-ArrowDown";
	    	String estilo = "hiddenRow";
	    	
	    	if(this.listTablaPerfilGU!=null && listTablaPerfilGU.size()>0) {
	    		this.listTablaPerfilGU.clear();
			}else {
				this.listTablaPerfilGU = new LinkedList<TablaPerfil>();
			}
			
			try {
	    		this.respuestaGetGUXGAPerfilCorp = perfilService.getListaGUXGAPerfilCorp(usuario, this.tablaPerfilSelect.getIdPersonal());
				for (InfoSubAlterno infoSubAlterno:this.respuestaGetGUXGAPerfilCorp.getInfoSubAlterno()) {
					
					for(Cumplimiento com:infoSubAlterno.getCumplimiento()) {
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
		        			cumplimientoPoliza.setMeta(com.getMeta()); 
		        			cumplimientoPoliza.setReal(com.getReal());
		        			cumplimientoPoliza.setEstado(com.getCumple());
		        			cumplimientoPoliza.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima basica")) {
		        			cumplimientoPb.setMeta(com.getMeta()); 
		        			cumplimientoPb.setReal(com.getReal());
		        			cumplimientoPb.setEstado(com.getCumple());
		        			cumplimientoPb.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima exceso")) {
		        			cumplimientoPe.setMeta(com.getMeta()); 
		        			cumplimientoPe.setReal(com.getReal());
		        			cumplimientoPe.setEstado(com.getCumple());
		        			cumplimientoPe.setPPTO(com.getPPTO());
		        		}
		        	}
					
					TablaPerfil tp = new TablaPerfil(infoSubAlterno.getIdPersonal(), 
							infoSubAlterno.getNombreCompleto(), 
							infoSubAlterno.getBCPU(), 
							infoSubAlterno.getRotacionPersonal(), 
							null,
							null,
							null,
							null,
							cumplimientoPoliza.getReal(), 
							cumplimientoPoliza.getMeta(), 
							cumplimientoPoliza.getPPTO(),
							cumplimientoPoliza.getEstado(),
		        			cumplimientoPb.getReal(), 
		        			cumplimientoPb.getMeta(), 
		        			cumplimientoPb.getPPTO(),
		        			cumplimientoPb.getEstado(),
		        			cumplimientoPe.getReal(), 
		        			cumplimientoPe.getMeta(), 
		        			cumplimientoPe.getPPTO(),
		        			cumplimientoPe.getEstado(),
		        			icono, estilo, perfilActual, verPerfil, 
		        			null, true, null, null, null);
					
					listTablaPerfilGU.add(tp);
	        	}
			} catch (Exception e) {
				log.error(e,e);
			}
		}
	}
	
	public boolean isExpanded(TablaPerfil tablaPerfil) {
		if(this.tablaPerfilSelect != null) {
			if (tablaPerfil.getIdPersonal() == this.tablaPerfilSelect.getIdPersonal()) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isExpandedChild(TablaPerfil tablaPerfil) {
		if(this.tablaPerfilChildSelect != null) {
			if (tablaPerfil.getIdPersonal() == this.tablaPerfilChildSelect.getIdPersonal()) {
				return true;
			}
		}
		return false;
	}
	
	public void cargaListadoUsuariosGA() {
		Cumplimiento cumplimientoPoliza = new Cumplimiento();
	    Cumplimiento cumplimientoPb = new Cumplimiento();
	    Cumplimiento cumplimientoPe = new Cumplimiento();
	    String icono = "icons-Triangle-ArrowRight";
	    String estilo = "accordion-toggle";
	    String perfilActual = "GU";
        String verPerfil = "VER Asesores";
        boolean habilitado = true;
        
        if(this.listTablaPerfilGU!=null && listTablaPerfilGU.size()>0) {
    		this.listTablaPerfilGU.clear();
		}else {
			this.listTablaPerfilGU = new LinkedList<TablaPerfil>();
		}
		
		try {
			if (this.usuarioDrillDown != null) {
				this.respuestaGetGUXGAPerfilCorp = perfilService.getListaGUXGAPerfilGA(this.usuarioDrillDown, Integer.parseInt(MathUtils.quitarPunto(this.usuarioDrillDown.getIdPersonal())));
			}else {
				this.respuestaGetGUXGAPerfilCorp = perfilService.getListaGUXGAPerfilGA(this.usuario, Integer.parseInt(MathUtils.quitarPunto(this.usuario.getIdPersonal())));
			}
			
			for (InfoSubAlterno infoSubAlterno:this.respuestaGetGUXGAPerfilCorp.getInfoSubAlterno()) {
				
				for(Cumplimiento com:infoSubAlterno.getCumplimiento()) {
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
	        			cumplimientoPoliza.setMeta(com.getMeta()); 
	        			cumplimientoPoliza.setReal(com.getReal());
	        			cumplimientoPoliza.setEstado(com.getCumple());
	        			cumplimientoPoliza.setPPTO(com.getPPTO());
	        		}
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima basica")) {
	        			cumplimientoPb.setMeta(com.getMeta()); 
	        			cumplimientoPb.setReal(com.getReal());
	        			cumplimientoPb.setEstado(com.getCumple());
	        			cumplimientoPb.setPPTO(com.getPPTO());
	        		}
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima exceso")) {
	        			cumplimientoPe.setMeta(com.getMeta()); 
	        			cumplimientoPe.setReal(com.getReal());
	        			cumplimientoPe.setEstado(com.getCumple());
	        			cumplimientoPe.setPPTO(com.getPPTO());
	        		}
	        	}
				
				if (infoSubAlterno.getEstado() != null) {
					if (infoSubAlterno.getEstado().equalsIgnoreCase("A")) {
						habilitado = true;
					}else {
						habilitado = false;
					}
				}
				
				TablaPerfil tp = new TablaPerfil(infoSubAlterno.getIdPersonal(), 
						infoSubAlterno.getNombreCompleto(), 
						infoSubAlterno.getBCPU(), 
						infoSubAlterno.getRotacionPersonal(), 
						null,
						null,
						null,
						null,
						cumplimientoPoliza.getReal(), 
						cumplimientoPoliza.getMeta(), 
						cumplimientoPoliza.getPPTO(),
						cumplimientoPoliza.getEstado(),
	        			cumplimientoPb.getReal(), 
	        			cumplimientoPb.getMeta(), 
	        			cumplimientoPb.getPPTO(),
	        			cumplimientoPb.getEstado(),
	        			cumplimientoPe.getReal(), 
	        			cumplimientoPe.getMeta(), 
	        			cumplimientoPe.getPPTO(),
	        			cumplimientoPe.getEstado(),
	        			icono, estilo, perfilActual, 
	        			verPerfil, null, habilitado,
	        			infoSubAlterno.getTramo(), 
	        			infoSubAlterno.getAntiguedad(), 
	        			null);
				
				listTablaPerfilGU.add(tp);
        	}
		} catch (Exception e) {
			log.error(e,e);
		}
	}
	
	public void cargaListadoUsuariosGU() {
		log.info("cargaListadoUsuariosGU");
		Cumplimiento cumplimientoPoliza = new Cumplimiento();
        Cumplimiento cumplimientoPb = new Cumplimiento();
        Cumplimiento cumplimientoPe = new Cumplimiento();
        
        this.listTablaPerfilAsesor = new LinkedList<TablaPerfil>();
        
        String perfilActual = "AS";
        String icono = "icons-Triangle-ArrowDown";
    	String estilo = "hiddenRow";
    	String accion = null;
    	boolean habilitado = true;
		Integer idPlanificacion;
		
		try {
			if (this.usuarioDrillDown != null) {
				this.respuestaGetGUXGAPerfilCorp = perfilService.getListaAsesoresPerfilGU(usuarioDrillDown);
			} else {
				this.respuestaGetGUXGAPerfilCorp = perfilService.getListaAsesoresPerfilGU(usuario);
			}
			for (InfoSubAlterno infoSubAlterno:this.respuestaGetGUXGAPerfilCorp.getInfoSubAlterno()) {
				idPlanificacion = null;
				for(Cumplimiento com:infoSubAlterno.getCumplimiento()) {
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
	        			cumplimientoPoliza.setMeta(com.getMeta()); 
	        			cumplimientoPoliza.setReal(com.getReal());
	        			cumplimientoPoliza.setEstado(com.getEstado());
	        			cumplimientoPoliza.setPPTO(com.getPPTO());
	        		}
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PB")) {
	        			cumplimientoPb.setMeta(com.getMeta()); 
	        			cumplimientoPb.setReal(com.getReal());
	        			cumplimientoPb.setEstado(com.getEstado());
	        			cumplimientoPb.setPPTO(com.getPPTO());
	        		}
	        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("PE")) {
	        			cumplimientoPe.setMeta(com.getMeta()); 
	        			cumplimientoPe.setReal(com.getReal());
	        			cumplimientoPe.setEstado(com.getEstado());
	        			cumplimientoPe.setPPTO(com.getPPTO());
	        		}
	        	}
				
				if (infoSubAlterno.getVerMeta() != null) {
					if (infoSubAlterno.getVerMeta().getVerMeta() != null) {
						if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("O")) {
							accion = "";
						}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("S")) {
							accion = "Sin meta";	
						}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("E")) {
							accion = "Ver meta";
							idPlanificacion = Integer.parseInt(MathUtils.quitarPunto(infoSubAlterno.getVerMeta().getIdPlanificacion()));				
						}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("A")) {
							accion = "Meta aprovada";
							
						}else if (infoSubAlterno.getVerMeta().getVerMeta().equalsIgnoreCase("R")) {
							accion = "Meta rechazada";
						}
					}
				}
				
				if (infoSubAlterno.getEstado() != null) {
					if (infoSubAlterno.getEstado().equalsIgnoreCase("A")) {
						habilitado = true;
					}else {
						habilitado = false;
					}
				}
							
				TablaPerfil tp = new TablaPerfil(infoSubAlterno.getIdPersonal(), 
						infoSubAlterno.getNombreCompleto(), 
						infoSubAlterno.getBCPU(), 
						infoSubAlterno.getRotacionPersonal(), 
						null, 
						null, 
						null,
						null,
						cumplimientoPoliza.getReal(), 
						cumplimientoPoliza.getMeta(), 
						cumplimientoPoliza.getPPTO(),
						cumplimientoPoliza.getEstado(),
	        			cumplimientoPb.getReal(), 
	        			cumplimientoPb.getMeta(), 
	        			cumplimientoPb.getPPTO(),
	        			cumplimientoPb.getEstado(),
	        			cumplimientoPe.getReal(), 
	        			cumplimientoPe.getMeta(), 
	        			cumplimientoPe.getPPTO(),
	        			cumplimientoPe.getEstado(),
	        			icono, estilo, perfilActual, 
	        			null, accion, habilitado,
	        			infoSubAlterno.getTramo(), 
	        			infoSubAlterno.getAntiguedad(),
	        			idPlanificacion);
				
				this.listTablaPerfilAsesor.add(tp);
        	}
		} catch (Exception e) {
			log.error(e,e);
		}
	}
	
	
	public void cargaListadoUsuarios() {
		listTablaPerfil = new LinkedList<TablaPerfil>();
		Cumplimiento cumplimientoPoliza = new Cumplimiento();
	    Cumplimiento cumplimientoPb = new Cumplimiento();
	    Cumplimiento cumplimientoPe = new Cumplimiento();
	    Cumplimiento cumplimientoNap = new Cumplimiento();
	    String icono = "icons-Triangle-ArrowRight";
	    String estilo = "accordion-toggle";
	    String perfilActual = "GA";
        String verPerfil = "VER GU";
        
		try {
			if(this.respuestaGetPerfilCorp != null 
						&& this.respuestaGetPerfilCorp.getInfoGA() != null) {
		        for(InfoGA infoGA:this.respuestaGetPerfilCorp.getInfoGA()) {
		           	for(Cumplimiento com:infoGA.getCumplimiento()) {
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("polizas")) {
		        			cumplimientoPoliza.setMeta(com.getMeta()); 
		        			cumplimientoPoliza.setReal(com.getReal());
		        			cumplimientoPoliza.setEstado(com.getEstado());
		        			cumplimientoPoliza.setCumple(com.getCumple());
		        			cumplimientoPoliza.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima basica")) {
		        			cumplimientoPb.setMeta(com.getMeta()); 
		        			cumplimientoPb.setReal(com.getReal());
		        			cumplimientoPb.setEstado(com.getEstado());
		        			cumplimientoPb.setCumple(com.getCumple());
		        			cumplimientoPb.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("prima exceso")) {
		        			cumplimientoPe.setMeta(com.getMeta()); 
		        			cumplimientoPe.setReal(com.getReal());
		        			cumplimientoPe.setEstado(com.getEstado());
		        			cumplimientoPe.setCumple(com.getCumple());
		        			cumplimientoPe.setPPTO(com.getPPTO());
		        		}
		        		if(com.getNombre().trim().toLowerCase().equalsIgnoreCase("NAP")) {
		        			cumplimientoNap.setMeta(com.getMeta()); 
		        			cumplimientoNap.setReal(com.getReal());
		        			cumplimientoNap.setEstado(com.getEstado());
		        			cumplimientoNap.setPPTO(com.getPPTO());
		        		}
		        	}
		           						
					TablaPerfil tablaPerfil = new TablaPerfil(infoGA.getIdPersonal(), 
													infoGA.getNombreCompleto(), 
													null, 
													infoGA.getRotacionPersonal(), 
													cumplimientoNap.getReal(),
													cumplimientoNap.getMeta(),
													cumplimientoNap.getPPTO(),
													cumplimientoNap.getEstado(),
													cumplimientoPoliza.getReal(), 
													cumplimientoPoliza.getMeta(), 
													cumplimientoPoliza.getPPTO(),
													cumplimientoPoliza.getEstado(),
								        			cumplimientoPb.getReal(), 
								        			cumplimientoPb.getMeta(), 
								        			cumplimientoPb.getPPTO(),
								        			cumplimientoPb.getEstado(),
								        			cumplimientoPe.getReal(), 
								        			cumplimientoPe.getMeta(), 
								        			cumplimientoPe.getPPTO(),
								        			cumplimientoPe.getEstado(),
								        			icono, estilo, perfilActual, verPerfil, 
								        			null, true, null, null, null);
		
					listTablaPerfil.add(tablaPerfil);
		        }
			}
    	} catch (Exception e) {
    		log.error(e,e);
		}
		
	}
	
	public void completaMapa() {
		listCumplimiento = new LinkedList<MapaPerfil>();
		listBajoCumplimiento = new LinkedList<MapaPerfil>();
		listSinSucursal = new LinkedList<MapaPerfil>();
		
		try {
			for(CumplimientoXRegion c: this.respuestaGetPerfilCorp.getCumplimientoXRegion()) {
				MapaPerfil m = new MapaPerfil();
				m.setCodigo(c.getCodigo());
				m.setName(c.getRegion());
				m.setPath(c.getCoordenada());
				
				if(c.getEstado().equalsIgnoreCase("S")) {
					listCumplimiento.add(m);
				}else if(c.getEstado().equalsIgnoreCase("N")) {
					listBajoCumplimiento.add(m);
				}else {
					listSinSucursal.add(m);
				}
			}
			this.jsonBajoCumplimiento = new Gson().toJson(listBajoCumplimiento);
			this.jsonCumplimiento = new Gson().toJson(listCumplimiento);
			this.jsonSinSucursal = new Gson().toJson(listSinSucursal);	
		} catch (Exception e) {
    		log.error(e,e);
		}	
	}
	
	public void completaCumplimiento() {
		cumplimientoPoliza = new CumplimientoPeriodoComercial();
		cumplimientoPB = new CumplimientoPeriodoComercial();
		cumplimientoPE = new CumplimientoPeriodoComercial();
		
		try {
			for(CumplimientoPeriodoComercial c:this.respuestaGetPerfilCorp.getCumplimientoPeriodoComercial()) {
				if (c.getNombre().trim().equalsIgnoreCase("Polizas")) {
					cumplimientoPoliza.setMeta(c.getMeta());
					cumplimientoPoliza.setNombre(c.getNombre());
					cumplimientoPoliza.setPlan(c.getPlan());
					cumplimientoPoliza.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Basica")) {
					cumplimientoPB.setMeta(c.getMeta());
					cumplimientoPB.setNombre(c.getNombre());
					cumplimientoPB.setPlan(c.getPlan());
					cumplimientoPB.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Exceso")) {
					cumplimientoPE.setMeta(c.getMeta());
					cumplimientoPE.setNombre(c.getNombre());
					cumplimientoPE.setPlan(c.getPlan());
					cumplimientoPE.setReal(c.getReal());
				}
			}
		} catch (Exception e) {
    		log.error(e,e);
		}	
	}
	
	public void completaCumplimientoGA() {
		cumplimientoPoliza = new CumplimientoPeriodoComercial();
		cumplimientoPB = new CumplimientoPeriodoComercial();
		cumplimientoPE = new CumplimientoPeriodoComercial();
		dotacionActivos = new Dotacion();
		dotacionTotales = new Dotacion();
		
		try {
			for(CumplimientoPeriodoComercial c:this.respuestaGetPerfilGA.getCumplimientoPeriodoComercial()) {
				if (c.getNombre().trim().equalsIgnoreCase("Polizas")) {
					cumplimientoPoliza.setMeta(c.getMeta());
					cumplimientoPoliza.setNombre(c.getNombre());
					cumplimientoPoliza.setPlan(c.getPlan());
					cumplimientoPoliza.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Basica")) {
					cumplimientoPB.setMeta(c.getMeta());
					cumplimientoPB.setNombre(c.getNombre());
					cumplimientoPB.setPlan(c.getPlan());
					cumplimientoPB.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Exceso")) {
					cumplimientoPE.setMeta(c.getMeta());
					cumplimientoPE.setNombre(c.getNombre());
					cumplimientoPE.setPlan(c.getPlan());
					cumplimientoPE.setReal(c.getReal());
				}
			}
			
			for(Dotacion d:this.respuestaGetPerfilGA.getDotacion()) {
				if (d.getNombre().trim().equalsIgnoreCase("Activos")) {
					dotacionActivos.setNombre(d.getNombre());
					dotacionActivos.setValor(d.getValor());
				}
				if (d.getNombre().trim().equalsIgnoreCase("Totales")) {
					dotacionTotales.setNombre(d.getNombre());
					dotacionTotales.setValor(d.getValor());
				}
			}
		} catch (Exception e) {
    		log.error(e,e);
		}
	}
	
	public void completaCumplimientoAS() {
		cumplimientoPoliza = new CumplimientoPeriodoComercial();
		cumplimientoPB = new CumplimientoPeriodoComercial();
		cumplimientoPE = new CumplimientoPeriodoComercial();
		
		try {
			for(CumplimientoPeriodoComercial c:this.respuestaGetPerfilAS.getCumplimiento()) {
				if (c.getNombre().trim().equalsIgnoreCase("Polizas")) {
					cumplimientoPoliza.setMeta(c.getMeta());
					cumplimientoPoliza.setNombre(c.getNombre());
					cumplimientoPoliza.setPlan("0");
					cumplimientoPoliza.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("PB")) {
					cumplimientoPB.setMeta(c.getMeta());
					cumplimientoPB.setNombre(c.getNombre());
					cumplimientoPB.setPlan("0");
					cumplimientoPB.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("PE")) {
					cumplimientoPE.setMeta(c.getMeta());
					cumplimientoPE.setNombre(c.getNombre());
					cumplimientoPE.setPlan("0");
					cumplimientoPE.setReal(c.getReal());
				}
			}
		} catch (Exception e) {
    		log.error(e,e);
		}
	}
	
	public void completaCumplimientoGU() {
		cumplimientoPoliza = new CumplimientoPeriodoComercial();
		cumplimientoPB = new CumplimientoPeriodoComercial();
		cumplimientoPE = new CumplimientoPeriodoComercial();
		dotacionActivos = new Dotacion();
		dotacionTotales = new Dotacion();
		
		try {
			for(CumplimientoPeriodoComercial c:this.respuestaGetPerfilGU.getCumplimientoPeriodoComercial()) {
				if (c.getNombre().trim().equalsIgnoreCase("Polizas")) {
					cumplimientoPoliza.setMeta(c.getMeta());
					cumplimientoPoliza.setNombre(c.getNombre());
					cumplimientoPoliza.setPlan(c.getPlan());
					cumplimientoPoliza.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Basica")) {
					cumplimientoPB.setMeta(c.getMeta());
					cumplimientoPB.setNombre(c.getNombre());
					cumplimientoPB.setPlan(c.getPlan());
					cumplimientoPB.setReal(c.getReal());
				}
				if (c.getNombre().trim().equalsIgnoreCase("Prima Exceso")) {
					cumplimientoPE.setMeta(c.getMeta());
					cumplimientoPE.setNombre(c.getNombre());
					cumplimientoPE.setPlan(c.getPlan());
					cumplimientoPE.setReal(c.getReal());
				}
			}
			
			for(Dotacion d:this.respuestaGetPerfilGU.getDotacion()) {
				if (d.getNombre().trim().equalsIgnoreCase("Activos")) {
					dotacionActivos.setNombre(d.getNombre());
					dotacionActivos.setValor(d.getValor());
				}
				if (d.getNombre().trim().equalsIgnoreCase("Totales")) {
					dotacionTotales.setNombre(d.getNombre());
					dotacionTotales.setValor(d.getValor());
				}
			}
			
			this.bcpuEstilo = "";
			for(BCPU d:this.respuestaGetPerfilGU.getBCPU()) {
				if (d.getNombre().trim().equalsIgnoreCase("Real")) {
					this.bcpuReal = d.getValor();
				}
				if (d.getNombre().trim().equalsIgnoreCase("Meta")) {
					this.bcpuMeta = d.getValor();
				}
			}
			if(this.bcpuReal != null && this.bcpuMeta != null) {
				if(Double.parseDouble(this.bcpuReal) > 0) {
					if (Double.parseDouble(this.bcpuReal) 
							>= Double.parseDouble(this.bcpuMeta)) {
						this.bcpuEstilo = "text-success";
					} else {
						this.bcpuEstilo = "text-danger";
					}
				}
				this.bcpuReal = MathUtils.formatoNumerico(this.bcpuReal);
				this.bcpuMeta = MathUtils.formatoNumerico(this.bcpuMeta);
			}
		} catch (Exception e) {
    		log.error(e,e);
		}
	}
	
	public void filtraPorRegion() {
		log.info("filtraPorRegion");
		String region = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmPerfil:regionFiltro");
		String idRegion = "";
		if (region != null && !region.isEmpty()) {
			idRegion = String.valueOf(Regiones.getRegionByName(region).getId());
		}
		
		log.info("region=" + region);
		log.info("idRegion=" + idRegion);
		try {
			this.respuestaGetPerfilCorp = perfilService.getListaPerfilCorp(this.usuario, idRegion);
			
			createPlanPieModel();
			createFunPieModel();
			createCalPieModel();
			createMixedModel();
			completaCumplimiento();
			completaMapa();
			cargaListadoUsuarios();
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	private void createPlanPieModelGA() {
		donutPlanPieModel = new DonutChartModel();
		donutPlanPieModel.setExtender("chartExtender");
		
		List<Map<String,Number>> list = new LinkedList<Map<String,Number>>();
		Map<String, Number> values = new HashMap<String,Number>();
        for(Planificacion p: this.respuestaGetPerfilGA.getAutodisciplina().getPlanificacion()) {
        	values.put(p.getNombre().replace(" ", "&nbsp;") + "&nbsp;:&nbsp;" + p.getValor(), p.getValor());
        }
        
        list.add(values);
            
        donutPlanPieModel.setData(list);
        donutPlanPieModel.setSeriesColors("00ACA0, 007ABC, A4CE4E, a7a8aa");
        donutPlanPieModel.setLegendPosition("e");
        donutPlanPieModel.setFill(true);
        donutPlanPieModel.setShowDataLabels(true);
        donutPlanPieModel.setShadow(false);
        donutPlanPieModel.setSliceMargin(2);
	}
	
	private void createPlanPieModelGU() {
		donutPlanPieModel = new DonutChartModel();
		donutPlanPieModel.setExtender("chartExtender");
		
		List<Map<String,Number>> list = new LinkedList<Map<String,Number>>();
        Map<String, Number> values = new HashMap<String,Number>();
        for(Planificacion p: this.respuestaGetPerfilGU.getAutodisciplina().getPlanificacion()) {
        	values.put(p.getNombre().replace(" ", "&nbsp;") + "&nbsp;:&nbsp;" + p.getValor(), p.getValor());
        }
        
        list.add(values);
            
        donutPlanPieModel.setData(list);
        donutPlanPieModel.setSeriesColors("A4CE4E, 00ACA0, a7a8aa, 007ABC");
        donutPlanPieModel.setLegendPosition("e");
        donutPlanPieModel.setFill(true);
        donutPlanPieModel.setShowDataLabels(true);
        donutPlanPieModel.setShadow(false);
        donutPlanPieModel.setSliceMargin(2);
	}
	
	private void createFunPieModelGA() {
		donutFunPieModel = new DonutChartModel();
		donutFunPieModel.setExtender("chartExtender");
		
		List<Map<String,Number>> list = new LinkedList<Map<String,Number>>();
        Map<String, Number> values = new HashMap<String,Number>();
        for(FunnelHoy p: this.respuestaGetPerfilGA.getAutodisciplina().getFunnelHoy()) {
        	values.put(p.getNombre().replace(" ", "&nbsp;") + "&nbsp;:&nbsp;" + p.getValor(), p.getValor());
        }
        list.add(values);
            
        donutFunPieModel.setData(list);
        donutFunPieModel.setSeriesColors("A7A8AA, 00ACA0");
        donutFunPieModel.setLegendPosition("e");
        donutFunPieModel.setFill(true);
        donutFunPieModel.setShowDataLabels(true);
        donutFunPieModel.setShadow(false);
        donutFunPieModel.setSliceMargin(2);
	}
	
	private void createFunPieModelGU() {
		donutFunPieModel = new DonutChartModel();
		donutFunPieModel.setExtender("chartExtender");
		
		List<Map<String,Number>> list = new LinkedList<Map<String,Number>>();
        Map<String, Number> values = new HashMap<String,Number>();
        for(FunnelHoy p: this.respuestaGetPerfilGU.getAutodisciplina().getFunnelHoy()) {
        	values.put(p.getNombre().replace(" ", "&nbsp;") + "&nbsp;:&nbsp;" + p.getValor(), p.getValor());
        }
        list.add(values);
            
        donutFunPieModel.setData(list);
        donutFunPieModel.setSeriesColors("A7A8AA, 00ACA0");
        donutFunPieModel.setLegendPosition("e");
        donutFunPieModel.setFill(true);
        donutFunPieModel.setShowDataLabels(true);
        donutFunPieModel.setShadow(false);
        donutFunPieModel.setSliceMargin(2);
	}
	
	private void createPlanPieModel() {
		planPieModel = new PieChartModel();
        ChartData data = new ChartData();
         
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values = new ArrayList<>();
        
        for(Planificacion p: this.respuestaGetPerfilCorp.getAutodisciplina().getPlanificacion()) {
        	values.add(p.getValor());
        }
        dataSet.setData(values);
         
        List<String> bgColors = new ArrayList<>();
        bgColors.add("rgb(217, 217, 214)");
        bgColors.add("rgb(0, 144, 218)");
        bgColors.add("rgb(164, 206, 78)");
        bgColors.add("rgb(219, 10, 91)");
        
        dataSet.setBackgroundColor(bgColors);
         
        data.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        for(Planificacion p: this.respuestaGetPerfilCorp.getAutodisciplina().getPlanificacion()) {
        	labels.add(p.getNombre());
        }
        data.setLabels(labels);
        
        PieChartOptions options = new PieChartOptions();
        Legend legend = new Legend();
        legend.setDisplay(true);
        legend.setPosition("right");
        options.setLegend(legend);
	}
	
	private void createFunPieModel() {
		funPieModel = new PieChartModel();
        ChartData data = new ChartData();
         
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values = new ArrayList<>();
        
        for(FunnelHoy p: this.respuestaGetPerfilCorp.getAutodisciplina().getFunnelHoy()) {
        	values.add(p.getValor());
        }
        dataSet.setData(values);
         
        List<String> bgColors = new ArrayList<>();
        bgColors.add("rgb(164, 206, 78)");
        bgColors.add("rgb(0, 122, 188)");
        dataSet.setBackgroundColor(bgColors);
         
        data.addChartDataSet(dataSet);
        List<String> labels = new ArrayList<>();
        
        for(FunnelHoy p: this.respuestaGetPerfilCorp.getAutodisciplina().getFunnelHoy()) {
        	labels.add(p.getNombre());
        }
	}
	
	private void createCalPieModelGA() {
		calPieModel = new PieChartModel();
		calPieModel.setExtender("chartExtender");
		
        Map<String, Number> values = new HashMap<String,Number>();
        
        values.put("0&nbsp;:&nbsp;" + this.respuestaGetPerfilGA.getCalificadosMedioCalif().getCero(), this.respuestaGetPerfilGA.getCalificadosMedioCalif().getCero());
        values.put("A&nbsp;:&nbsp;" + this.respuestaGetPerfilGA.getCalificadosMedioCalif().getA(), this.respuestaGetPerfilGA.getCalificadosMedioCalif().getA());
        values.put("B&nbsp;:&nbsp;" + this.respuestaGetPerfilGA.getCalificadosMedioCalif().getB(), this.respuestaGetPerfilGA.getCalificadosMedioCalif().getB());
        values.put("C+&nbsp;:&nbsp;" + this.respuestaGetPerfilGA.getCalificadosMedioCalif().getC(), this.respuestaGetPerfilGA.getCalificadosMedioCalif().getC());
        
        calPieModel.setData(values);
        calPieModel.setSeriesColors("FFC600, 0090DA, A4CE4E, D9D9D6");
        calPieModel.setLegendPosition("e");
        calPieModel.setFill(true);
        calPieModel.setShowDataLabels(true);
        calPieModel.setShadow(false);
        calPieModel.setSliceMargin(2);
 	}
	
	private void createCalPieModelGU() {
		calPieModel = new PieChartModel();
		calPieModel.setExtender("chartExtender");
		
        Map<String, Number> values = new HashMap<String,Number>();
        
        values.put("0&nbsp;:&nbsp;" + this.respuestaGetPerfilGU.getCalificadosMedioCalif().getCero(), this.respuestaGetPerfilGU.getCalificadosMedioCalif().getCero());
        values.put("A&nbsp;:&nbsp;" + this.respuestaGetPerfilGU.getCalificadosMedioCalif().getA(), this.respuestaGetPerfilGU.getCalificadosMedioCalif().getA());
        values.put("B&nbsp;:&nbsp;" + this.respuestaGetPerfilGU.getCalificadosMedioCalif().getB(), this.respuestaGetPerfilGU.getCalificadosMedioCalif().getB());
        values.put("C+&nbsp;:&nbsp;" + this.respuestaGetPerfilGU.getCalificadosMedioCalif().getC(), this.respuestaGetPerfilGU.getCalificadosMedioCalif().getC());
        
        calPieModel.setData(values);
        calPieModel.setSeriesColors("FFC600, 0090DA, A4CE4E, D9D9D6");
        calPieModel.setLegendPosition("e");
        calPieModel.setFill(true);
        calPieModel.setShowDataLabels(true);
        calPieModel.setShadow(false);
        calPieModel.setSliceMargin(2);
 	}
	
	private void createCalPieModel() {
		calPieModel = new PieChartModel();
		calPieModel.setExtender("chartExtender");
		
        Map<String, Number> values = new HashMap<String,Number>();
        
        values.put("0&nbsp;:&nbsp;" + this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getCero(), this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getCero());
        values.put("A&nbsp;:&nbsp;" + this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getA(), this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getA());
        values.put("B&nbsp;:&nbsp;" + this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getB(), this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getB());
        values.put("C+&nbsp;:&nbsp;" + this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getC(), this.respuestaGetPerfilCorp.getCalificadosMedioCalif().getC());
        
        calPieModel.setData(values);
        calPieModel.setSeriesColors("FFC600, 0090DA, A4CE4E, D9D9D6");
        calPieModel.setLegendPosition("e");
        calPieModel.setFill(true);
        calPieModel.setShowDataLabels(true);
        calPieModel.setShadow(false);
        calPieModel.setSliceMargin(2);
 	}
	
	 public void createMixedModel() {
		 barCharPolizas = new BarChartModel();
        ChartData data = new ChartData();
        
        
        LineChartDataSet dataSet1 = new LineChartDataSet();
        List<Number> values1 = new ArrayList<>();
        values1.add(50);
        dataSet1.setData(values1);
        dataSet1.setLabel("Presupuesto");
        dataSet1.setFill(false);
        dataSet1.setBorderColor("rgb(54, 162, 235)");
         
        BarChartDataSet dataSet2 = new BarChartDataSet();
        List<Number> values2 = new ArrayList<>();
        values2.add(20);
        dataSet2.setData(values2);
        dataSet2.setLabel("Meta");
        dataSet2.setBorderColor("rgb(255, 99, 132)");
        dataSet2.setBackgroundColor("rgba(255, 99, 132, 0.2)");
         
        LineChartDataSet dataSet3 = new LineChartDataSet();
        List<Number> values3 = new ArrayList<>();
        values3.add(10);
        dataSet3.setData(values3);
        dataSet3.setLabel("Real");
        dataSet3.setFill(false);
        dataSet3.setBorderColor("rgb(54, 162, 235)");
         
        data.addChartDataSet(dataSet1);
        data.addChartDataSet(dataSet2);
        data.addChartDataSet(dataSet3);

        barCharPolizas.setData(data);
         
        //Options
        BarChartOptions options = new BarChartOptions();
        CartesianScales cScales = new CartesianScales();
        CartesianLinearAxes linearAxes = new CartesianLinearAxes();
        CartesianLinearTicks ticks = new CartesianLinearTicks();
        ticks.setBeginAtZero(true);
        linearAxes.setTicks(ticks);
         
        cScales.addYAxesData(linearAxes);
        options.setScales(cScales);
        barCharPolizas.setOptions(options);
    }

	 public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	public List<UsuarioSession> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(List<UsuarioSession> listUsuario) {
		this.listUsuario = listUsuario;
	}

	public RespuestaGetPerfilCorp getRespuestaGetPerfilCorp() {
		return respuestaGetPerfilCorp;
	}

	public void setRespuestaGetPerfilCorp(RespuestaGetPerfilCorp respuestaGetPerfilCorp) {
		this.respuestaGetPerfilCorp = respuestaGetPerfilCorp;
	}

	public List<InfoGA> getListInfoGA() {
		return listInfoGA;
	}

	public void setListInfoGA(List<InfoGA> listInfoGA) {
		this.listInfoGA = listInfoGA;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public PerfilService getPerfilService() {
		return perfilService;
	}

	public PieChartModel getPlanPieModel() {
		return planPieModel;
	}

	public void setPlanPieModel(PieChartModel planPieModel) {
		this.planPieModel = planPieModel;
	}

	public PieChartModel getFunPieModel() {
		return funPieModel;
	}

	public void setFunPieModel(PieChartModel funPieModel) {
		this.funPieModel = funPieModel;
	}

	public BarChartModel getBarCharPolizas() {
		return barCharPolizas;
	}

	public void setBarCharPolizas(BarChartModel barCharPolizas) {
		this.barCharPolizas = barCharPolizas;
	}

	public CumplimientoPeriodoComercial getCumplimientoPoliza() {
		return cumplimientoPoliza;
	}

	public void setCumplimientoPoliza(CumplimientoPeriodoComercial cumplimientoPoliza) {
		this.cumplimientoPoliza = cumplimientoPoliza;
	}

	public CumplimientoPeriodoComercial getCumplimientoPB() {
		return cumplimientoPB;
	}

	public void setCumplimientoPB(CumplimientoPeriodoComercial cumplimientoPB) {
		this.cumplimientoPB = cumplimientoPB;
	}

	public CumplimientoPeriodoComercial getCumplimientoPE() {
		return cumplimientoPE;
	}

	public void setCumplimientoPE(CumplimientoPeriodoComercial cumplimientoPE) {
		this.cumplimientoPE = cumplimientoPE;
	}

	public PieChartModel getCalPieModel() {
		return calPieModel;
	}

	public void setCalPieModel(PieChartModel calPieModel) {
		this.calPieModel = calPieModel;
	}

	public List<MapaPerfil> getListCumplimiento() {
		return listCumplimiento;
	}

	public void setListCumplimiento(List<MapaPerfil> listCumplimiento) {
		this.listCumplimiento = listCumplimiento;
	}

	public List<MapaPerfil> getListBajoCumplimiento() {
		return listBajoCumplimiento;
	}

	public void setListBajoCumplimiento(List<MapaPerfil> listBajoCumplimiento) {
		this.listBajoCumplimiento = listBajoCumplimiento;
	}

	public List<MapaPerfil> getListSinSucursal() {
		return listSinSucursal;
	}

	public void setListSinSucursal(List<MapaPerfil> listSinSucursal) {
		this.listSinSucursal = listSinSucursal;
	}

	public String getJsonCumplimiento() {
		return jsonCumplimiento;
	}

	public void setJsonCumplimiento(String jsonCumplimiento) {
		this.jsonCumplimiento = jsonCumplimiento;
	}

	public String getJsonBajoCumplimiento() {
		return jsonBajoCumplimiento;
	}

	public void setJsonBajoCumplimiento(String jsonBajoCumplimiento) {
		this.jsonBajoCumplimiento = jsonBajoCumplimiento;
	}

	public String getJsonSinSucursal() {
		return jsonSinSucursal;
	}

	public void setJsonSinSucursal(String jsonSinSucursal) {
		this.jsonSinSucursal = jsonSinSucursal;
	}

	public List<TablaPerfil> getListTablaPerfil() {
		return listTablaPerfil;
	}

	public void setListTablaPerfil(List<TablaPerfil> listTablaPerfil) {
		this.listTablaPerfil = listTablaPerfil;
	}

	public List<TablaPerfil> getListTablaPerfilGU() {
		return listTablaPerfilGU;
	}

	public void setListTablaPerfilGU(List<TablaPerfil> listTablaPerfilGU) {
		this.listTablaPerfilGU = listTablaPerfilGU;
	}

	public TablaPerfil getTablaPerfilSelect() {
		return tablaPerfilSelect;
	}

	public void setTablaPerfilSelect(TablaPerfil tablaPerfilSelect) {
		this.tablaPerfilSelect = tablaPerfilSelect;
	}

	public TablaPerfil getTablaPerfilChildSelect() {
		return tablaPerfilChildSelect;
	}

	public void setTablaPerfilChildSelect(TablaPerfil tablaPerfilChildSelect) {
		this.tablaPerfilChildSelect = tablaPerfilChildSelect;
	}

	public List<TablaPerfil> getListTablaPerfilAsesor() {
		return listTablaPerfilAsesor;
	}

	public void setListTablaPerfilAsesor(List<TablaPerfil> listTablaPerfilAsesor) {
		this.listTablaPerfilAsesor = listTablaPerfilAsesor;
	}

	public String getPaginaPerfil() {
		return paginaPerfil;
	}

	public void setPaginaPerfil(String paginaPerfil) {
		this.paginaPerfil = paginaPerfil;
	}

	public RespuestaGetPerfilGA getRespuestaGetPerfilGA() {
		return respuestaGetPerfilGA;
	}

	public void setRespuestaGetPerfilGA(RespuestaGetPerfilGA respuestaGetPerfilGA) {
		this.respuestaGetPerfilGA = respuestaGetPerfilGA;
	}

	public Dotacion getDotacionActivos() {
		return dotacionActivos;
	}

	public void setDotacionActivos(Dotacion dotacionActivos) {
		this.dotacionActivos = dotacionActivos;
	}

	public Dotacion getDotacionTotales() {
		return dotacionTotales;
	}

	public void setDotacionTotales(Dotacion dotacionTotales) {
		this.dotacionTotales = dotacionTotales;
	}

	public Integer getIdPersonalSelect() {
		return idPersonalSelect;
	}

	public void setIdPersonalSelect(Integer idPersonalSelect) {
		this.idPersonalSelect = idPersonalSelect;
	}

	public String getEstadoSelect() {
		return estadoSelect;
	}

	public void setEstadoSelect(String estadoSelect) {
		this.estadoSelect = estadoSelect;
	}

	public String getEstadoNuevoSelect() {
		return estadoNuevoSelect;
	}

	public void setEstadoNuevoSelect(String estadoNuevoSelect) {
		this.estadoNuevoSelect = estadoNuevoSelect;
	}

	public String getSupervisorSelect() {
		return supervisorSelect;
	}

	public void setSupervisorSelect(String supervisorSelect) {
		this.supervisorSelect = supervisorSelect;
	}

	public RespuestaGetPerfilGU getRespuestaGetPerfilGU() {
		return respuestaGetPerfilGU;
	}

	public void setRespuestaGetPerfilGU(RespuestaGetPerfilGU respuestaGetPerfilGU) {
		this.respuestaGetPerfilGU = respuestaGetPerfilGU;
	}

	public String getBcpuMeta() {
		return bcpuMeta;
	}

	public void setBcpuMeta(String bcpuMeta) {
		this.bcpuMeta = bcpuMeta;
	}

	public String getBcpuReal() {
		return bcpuReal;
	}

	public void setBcpuReal(String bcpuReal) {
		this.bcpuReal = bcpuReal;
	}

	public String getBcpuEstilo() {
		return bcpuEstilo;
	}

	public void setBcpuEstilo(String bcpuEstilo) {
		this.bcpuEstilo = bcpuEstilo;
	}

	public PlanAsesor getPlanAsesor() {
		return planAsesor;
	}

	public void setPlanAsesor(PlanAsesor planAsesor) {
		this.planAsesor = planAsesor;
	}

	public DonutChartModel getDonutPlanPieModel() {
		return donutPlanPieModel;
	}

	public void setDonutPlanPieModel(DonutChartModel donutPlanPieModel) {
		this.donutPlanPieModel = donutPlanPieModel;
	}

	public DonutChartModel getDonutFunPieModel() {
		return donutFunPieModel;
	}

	public void setDonutFunPieModel(DonutChartModel donutFunPieModel) {
		this.donutFunPieModel = donutFunPieModel;
	}

	public String getClaveActual() {
		return claveActual;
	}

	public void setClaveActual(String claveActual) {
		this.claveActual = claveActual;
	}

	public String getClaveNueva() {
		return claveNueva;
	}

	public void setClaveNueva(String claveNueva) {
		this.claveNueva = claveNueva;
	}

	public String getClaveNuevaR() {
		return claveNuevaR;
	}

	public void setClaveNuevaR(String claveNuevaR) {
		this.claveNuevaR = claveNuevaR;
	}

	public RespuestaGetPerfilAS getRespuestaGetPerfilAS() {
		return respuestaGetPerfilAS;
	}

	public void setRespuestaGetPerfilAS(RespuestaGetPerfilAS respuestaGetPerfilAS) {
		this.respuestaGetPerfilAS = respuestaGetPerfilAS;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getRespuestaSueldoMesActualAses() {
		return respuestaSueldoMesActualAses;
	}

	public void setRespuestaSueldoMesActualAses(String respuestaSueldoMesActualAses) {
		this.respuestaSueldoMesActualAses = respuestaSueldoMesActualAses;
	}
	
	public String getRespuestaSueldoArrastreAses() {
		return respuestaSueldoArrastreAses;
	}

	public void setRespuestaSueldoArrastreAses(String respuestaSueldoArrastreAses) {
		this.respuestaSueldoArrastreAses = respuestaSueldoArrastreAses;
	}

	public boolean isVerDetalleTableroSueldo() {
		return verDetalleTableroSueldo;
	}

	public void setVerDetalleTableroSueldo(boolean verDetalleTableroSueldo) {
		this.verDetalleTableroSueldo = verDetalleTableroSueldo;
	}

	public String getAutodisciplinaPorDiaJson() {
		return autodisciplinaPorDiaJson;
	}

	public void setAutodisciplinaPorDiaJson(String autodisciplinaPorDiaJson) {
		this.autodisciplinaPorDiaJson = autodisciplinaPorDiaJson;
	}
}
