package cl.beboss.metlife.mbean;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import cl.beboss.metlife.dto.Usuario;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.services.AdminService;
import cl.beboss.metlife.utils.RepeatPaginator;

@ManagedBean(name="adminBean")
@SessionScoped
public class AdminBean extends BaseViewBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger( AdminBean.class.getName() );
	private FacesContext facesContext = FacesContext.getCurrentInstance();
	private UsuarioSession usuarioSession;
	private List<Usuario> listUsuarios;
	private String busqueda = "";
	private String cantRegisXPag = "0";
	private RepeatPaginator paginator;
	
	private AdminService adminService 
	= CDI.current().select(AdminService.class).get();
	
	@PostConstruct
    public void initAdminBean() {
		log.info("initAdminBean");
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		this.usuarioSession = (UsuarioSession) session.getAttribute("usuario");
		cargaUsuarios();
    }
	
	public void cargaUsuarios() {
		try {
			if (this.listUsuarios != null && this.listUsuarios.size() > 0) {
				this.listUsuarios.clear();
			} else {
				this.listUsuarios = new LinkedList<Usuario>();
			}
			this.listUsuarios = adminService.getListaUsuarios(this.usuarioSession, this.busqueda, this.cantRegisXPag, "1");
			
			if (this.listUsuarios == null || this.listUsuarios.size() == 0) {
				addMessage("Busqueda de usuarios sin resultados");
				this.paginator = null;
			}else {
				this.paginator = new RepeatPaginator(this.listUsuarios);
			}
		}catch (Exception e) {
    		log.error(e,e);
			addMessage("A ocurrido un error al cargar lista de usuarios");
		}
	}
	
	public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	public List<Usuario> getListUsuarios() {
		return listUsuarios;
	}

	public void setListUsuarios(List<Usuario> listUsuarios) {
		this.listUsuarios = listUsuarios;
	}

	public UsuarioSession getUsuarioSession() {
		return usuarioSession;
	}

	public void setUsuarioSession(UsuarioSession usuarioSession) {
		this.usuarioSession = usuarioSession;
	}

	public RepeatPaginator getPaginator() {
		return paginator;
	}

	public void setPaginator(RepeatPaginator paginator) {
		this.paginator = paginator;
	}

	public String getBusqueda() {
		return busqueda;
	}

	public void setBusqueda(String busqueda) {
		this.busqueda = busqueda;
	}
	
	
}
