package cl.beboss.metlife.mbean;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.UsuarioSession;

public class BaseViewBean implements Serializable{

	private static final long serialVersionUID = 1L;
	public UsuarioSession usuario;
	public UsuarioSession usuarioDrillDown;

	public boolean getEsAsesor() {
		if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
			|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
			return true;
		}
		return false;
	}
	
	public boolean getEsAsesorODrillDownAsesor() {
		if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
				|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)
				|| (usuarioDrillDown != null && usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor))) {
			return true;
		}
		return false;
	}
	
	public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	public UsuarioSession getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioSession usuario) {
		this.usuario = usuario;
	}

	public UsuarioSession getUsuarioDrillDown() {
		return usuarioDrillDown;
	}

	public void setUsuarioDrillDown(UsuarioSession usuarioDrillDown) {
		this.usuarioDrillDown = usuarioDrillDown;
	}
}
