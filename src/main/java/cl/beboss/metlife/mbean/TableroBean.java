package cl.beboss.metlife.mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.json.JSONObject;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.ListaPeriodos;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.services.ListaPeriodosService;
import cl.beboss.metlife.services.TableroService;

@ManagedBean(name="tableroBean")
@SessionScoped
public class TableroBean extends BaseViewBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger( TableroBean.class.getName() );
	private FacesContext facesContext = FacesContext.getCurrentInstance();
	
	private TableroService tableroService 
	= CDI.current().select(TableroService.class).get();
	
	private ListaPeriodosService listaPeriodosService 
	= CDI.current().select(ListaPeriodosService.class).get();
	
	private String respuesta;
	private String anoDesde;
	private String anoHasta;
	private String mesDesde;
	private String mesHasta;
	
	private List<String> listaAnosDesde;
	private List<String> listaAnosHasta;
	private List<String> listaMesesDesde;
	private List<String> listaMesesHasta;
	private List<ListaPeriodos> listaPeriodos;
	
	private Map<String, List<String>> anoMesDisponibles = new HashMap<String, List<String>>();
	
	@PostConstruct
    public void initTableroBean() {
		log.info("initTableroBean");
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		this.usuario = (UsuarioSession) session.getAttribute("usuario");
		this.usuarioDrillDown = (UsuarioSession) session.getAttribute("usuarioDrillDown");
		cargaPeriodos();
		cargaTablero();
    }
	
	private void cargaPeriodos() {
		log.info("cargaPeriodos");
		try {
			String recorreAno = null;
			String recorreMes = null;
			
			this.listaAnosDesde = new LinkedList<String>();
			if (this.usuarioDrillDown != null) {		
				this.listaPeriodos = listaPeriodosService.getListaPeriodos(usuarioDrillDown.getIdEmpresa(), this.usuarioDrillDown.getToken()).getListaPeriodos();
			}else {
				this.listaPeriodos = listaPeriodosService.getListaPeriodos(usuario.getIdEmpresa(), this.usuario.getToken()).getListaPeriodos();
			}
			
			if(this.listaPeriodos != null && this.listaPeriodos.size() > 0) {
				for (ListaPeriodos periodos:this.listaPeriodos) {
					recorreAno = periodos.getPeriodo().substring(0, 4); 
					if(!this.listaAnosDesde.contains(recorreAno)) {
						this.listaAnosDesde.add(recorreAno);
					}
					
					if (!anoMesDisponibles.containsKey(recorreAno)) {
						this.anoMesDisponibles.put(recorreAno, new ArrayList<String>());
			        }
					recorreMes = periodos.getPeriodo().substring(4); 
					this.anoMesDisponibles.get(recorreAno).add(recorreMes);
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	private void cargaTablero() {
		try {
			String recorreAno = null;
			String recorreMes = null;
			
			if (this.usuarioDrillDown != null) {
				if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
					this.respuesta = tableroService.getTableroMandoGA(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), "", "", this.usuarioDrillDown.getToken());
				} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
					this.respuesta = tableroService.getTableroMandoGU(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), "", "", this.usuarioDrillDown.getToken());
				} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor)) {
					this.respuesta = tableroService.getTableroMandoAS(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), "", "", this.usuarioDrillDown.getToken());
				}
			}else {
				if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilCorp)) {
					this.respuesta = tableroService.getTableroMandoCorp(usuario.getIdEmpresa(), usuario.getIdPersonal(), "", "", this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
					this.respuesta = tableroService.getTableroMandoGA(usuario.getIdEmpresa(), usuario.getIdPersonal(), "", "", this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
					this.respuesta = tableroService.getTableroMandoGU(usuario.getIdEmpresa(), usuario.getIdPersonal(), "", "", this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
						|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
					this.respuesta = tableroService.getTableroMandoAS(usuario.getIdEmpresa(), usuario.getIdPersonal(), "", "", this.usuario.getToken());
				}
			}
			
			JSONObject obj = new JSONObject(this.respuesta);
	        int idPeriodoDesde = obj.getJSONObject("data").getInt("IdPeriodoDesde");
	        int idPeriodoHasta = obj.getJSONObject("data").getInt("IdPeriodoHasta");
			
			if(this.listaPeriodos != null && this.listaPeriodos.size() > 0) {
				for (ListaPeriodos periodos:this.listaPeriodos) {
					if(Integer.parseInt(periodos.getIdPeriodo().replace(".0", "")) == idPeriodoDesde) {
						recorreAno = periodos.getPeriodo().substring(0, 4);
						recorreMes = periodos.getPeriodo().substring(4); 
						this.anoDesde = recorreAno;
						completaMesesDesde();
						this.mesDesde = recorreMes;
					}
				}
				for (ListaPeriodos periodos:this.listaPeriodos) {
					if(Integer.parseInt(periodos.getIdPeriodo().replace(".0", "")) == idPeriodoHasta) {
						recorreAno = periodos.getPeriodo().substring(0, 4);
						recorreMes = periodos.getPeriodo().substring(4);
						this.anoHasta = recorreAno;
						completaMesesHasta();
						this.mesHasta = recorreMes;
					}
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void completaMesesDesde() {
		log.info("completaMesesDesde");
		
		if (this.listaMesesDesde != null && this.listaMesesDesde.size() > 0) {
			this.listaMesesDesde.clear();
		}else {
			this.listaMesesDesde = new LinkedList<String>();
		}
		if (this.listaAnosHasta != null && this.listaAnosHasta.size() > 0) {
			this.listaAnosHasta.clear();
		}else {
			this.listaAnosHasta = new LinkedList<String>();
		}
		if (this.listaMesesHasta != null && this.listaMesesHasta.size() > 0) {
			this.listaMesesHasta.clear();
		}else {
			this.listaMesesHasta = new LinkedList<String>();
		}
		
		this.mesDesde = null;
		this.anoHasta = null;
		this.mesHasta = null;
		
		if(this.anoDesde != null && this.anoMesDisponibles.get(this.anoDesde).size() > 0) {
			this.listaMesesDesde.addAll(this.anoMesDisponibles.get(this.anoDesde));
			for (Map.Entry<String, List<String>> entry : anoMesDisponibles.entrySet()) {	
				if (Integer.parseInt(entry.getKey()) >= Integer.parseInt(this.anoDesde)) {
					this.listaAnosHasta.add(entry.getKey());
				}
			}
		}
	}
	
	public void completaMesesHasta() {
		log.info("completaMesesDesde");
		
		if (this.listaMesesHasta != null && this.listaMesesHasta.size()>0) {
			this.listaMesesHasta.clear();
		}else {
			this.listaMesesHasta = new LinkedList<String>();
		}

		if (this.anoHasta != null) {
			if(this.anoMesDisponibles.get(this.anoHasta).size() > 0) {
				if(Integer.parseInt(this.anoDesde) == Integer.parseInt(this.anoHasta)) {
					this.mesHasta = null;
					for(String mes:this.anoMesDisponibles.get(this.anoHasta)) {
						if (Integer.parseInt(mes) >= Integer.parseInt(this.mesDesde)) {
							this.listaMesesHasta.add(mes);
						}
					}
				}else {
					this.listaMesesHasta.addAll(this.anoMesDisponibles.get(this.anoHasta));
				}
			}
		}
	}
	
	public void filtraHistorico() {
		log.info("filtraHistorico");
		try {
			
			String periodoDesde = this.anoDesde.concat(mesDesde);
			String periodoHasta = this.anoHasta.concat(mesHasta);	
			
			ListaPeriodos perDesde = null;
			ListaPeriodos perHasta = null;
			
			if(periodoDesde != null && periodoHasta != null) {
				for (ListaPeriodos periodo:this.listaPeriodos) {
					if (Integer.parseInt(periodo.getPeriodo()) == Integer.parseInt(periodoDesde)){
						perDesde = periodo;
					}
					if (Integer.parseInt(periodo.getPeriodo()) == Integer.parseInt(periodoHasta)){
						perHasta = periodo;
					}
				}
				
				if(Integer.parseInt(perDesde.getPeriodo()) > Integer.parseInt(perHasta.getPeriodo())) {
					this.addMessage("DESDE debe ser menor que HASTA");
				}else {
					if (this.usuarioDrillDown != null) {
						if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
							this.respuesta = tableroService.getTableroMandoGA(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuarioDrillDown.getToken());
						} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
							this.respuesta = tableroService.getTableroMandoGU(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuarioDrillDown.getToken());
						} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor)) {
							this.respuesta = tableroService.getTableroMandoAS(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuarioDrillDown.getToken());
						}
					} else {
						if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilCorp)) {
							this.respuesta = tableroService.getTableroMandoCorp(usuario.getIdEmpresa(), usuario.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuario.getToken());
						} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
							this.respuesta = tableroService.getTableroMandoGA(usuario.getIdEmpresa(), usuario.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuario.getToken());
						} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
							this.respuesta = tableroService.getTableroMandoGU(usuario.getIdEmpresa(), usuario.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuario.getToken());
						} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
								|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
							this.respuesta = tableroService.getTableroMandoAS(usuario.getIdEmpresa(), usuario.getIdPersonal(), perDesde.getIdPeriodo(), perHasta.getIdPeriodo(), this.usuario.getToken());
						}
					}
					log.info("respuesta=" + respuesta);
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public String getAnoDesde() {
		return anoDesde;
	}

	public void setAnoDesde(String anoDesde) {
		this.anoDesde = anoDesde;
	}

	public String getAnoHasta() {
		return anoHasta;
	}

	public void setAnoHasta(String anoHasta) {
		this.anoHasta = anoHasta;
	}

	public ListaPeriodosService getListaPeriodosService() {
		return listaPeriodosService;
	}

	public void setListaPeriodosService(ListaPeriodosService listaPeriodosService) {
		this.listaPeriodosService = listaPeriodosService;
	}

	public String getMesDesde() {
		return mesDesde;
	}

	public void setMesDesde(String mesDesde) {
		this.mesDesde = mesDesde;
	}

	public String getMesHasta() {
		return mesHasta;
	}

	public void setMesHasta(String mesHasta) {
		this.mesHasta = mesHasta;
	}

	public List<String> getListaAnosDesde() {
		return listaAnosDesde;
	}

	public void setListaAnosDesde(List<String> listaAnosDesde) {
		this.listaAnosDesde = listaAnosDesde;
	}

	public List<String> getListaAnosHasta() {
		return listaAnosHasta;
	}

	public void setListaAnosHasta(List<String> listaAnosHasta) {
		this.listaAnosHasta = listaAnosHasta;
	}

	public List<String> getListaMesesDesde() {
		return listaMesesDesde;
	}

	public void setListaMesesDesde(List<String> listaMesesDesde) {
		this.listaMesesDesde = listaMesesDesde;
	}

	public List<String> getListaMesesHasta() {
		return listaMesesHasta;
	}

	public void setListaMesesHasta(List<String> listaMesesHasta) {
		this.listaMesesHasta = listaMesesHasta;
	}
}
