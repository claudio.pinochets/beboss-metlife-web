package cl.beboss.metlife.mbean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.services.ValidaUsuarioService;

@ManagedBean(name="loginBean")
@ViewScoped
public class LoginBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = Logger.getLogger( LoginBean.class.getName() );
	
	private String cuenta;
	private String clave;
	private String redirect = "/perfil";
	private String redirect_admin = "/admin/usuarios";
	private boolean chkAcepta;
	private boolean recordar;
	private String baseUrl;
	private String emailRecuperaClave;
	
	private ValidaUsuarioService validaUsuarioService 
		= CDI.current().select(ValidaUsuarioService.class).get();
		
	@PostConstruct
    public void initLoginBean() {
         checkCookie();
    }
	
	public void loginAction(ActionEvent event) {	
		if (this.chkAcepta) {
		try {
			UsuarioSession usuario = validaUsuarioService.getUsuario(Constantes.empresa, cuenta, clave);		
			if(usuario != null) {
					FacesContext facesContext = FacesContext.getCurrentInstance();
					HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
					session.setAttribute("usuario", usuario);
					recordarCuenta();
					if(usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAdmin)) {
						facesContext.getExternalContext().redirect(this.redirect_admin);
					}else {
						facesContext.getExternalContext().redirect(this.redirect);
					}
			}else {
				addMessage("Nombre de usuario o contraseña no valida");
			}
    	}catch (Exception e) {
    		log.error(e,e);
			addMessage("ERROR: " + e.getMessage());
		}
		}else {
			addMessage("Para ingresar debe aceptar términos y condiciones");
		}
	}
	
	private void recordarCuenta() {
		String remember1;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		Cookie btuser = new Cookie("btuser", this.cuenta);
		Cookie btpasswd = new Cookie("btpasswd",this.clave);
		if(this.recordar == false){
			remember1 = "false";
			btuser.setMaxAge(0);
			btpasswd.setMaxAge(0);
		}else{
			remember1 = "true";
			btuser.setMaxAge(3600);
			btpasswd.setMaxAge(3600);
		}
		Cookie btremember = new Cookie("btremember",remember1);
		((HttpServletResponse)facesContext.getExternalContext().getResponse()).addCookie(btuser);
		((HttpServletResponse)facesContext.getExternalContext().getResponse()).addCookie(btpasswd);
		((HttpServletResponse)facesContext.getExternalContext().getResponse()).addCookie(btremember);
	}
	
	public void checkCookie(){
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String cookieName = null;
		String remember1;
		Cookie cookie[] = ((HttpServletRequest)facesContext.getExternalContext().getRequest()).getCookies();
		if(cookie != null && cookie.length > 0){
			for(int i = 0; i<cookie.length; i++){
				cookieName = cookie[i].getName();
				if(cookieName.equalsIgnoreCase("btuser")){
					this.cuenta = cookie[i].getValue();
				}else if(cookieName.equalsIgnoreCase("btpasswd")){
					this.clave = cookie[i].getValue();
				}else if(cookieName.equalsIgnoreCase("btremember")){
					remember1 = cookie[i].getValue();
					if(remember1.equalsIgnoreCase("false")){
						this.recordar = false;
						this.chkAcepta = false;
					}
					else if(remember1.equalsIgnoreCase("true")){
						this.recordar = true;
						this.chkAcepta = true; 
					}
				}
			}
		}
	}
	
	public void validate(FacesContext fc, UIComponent component, Object value) throws ValidatorException {
        if (!this.chkAcepta) {
            throw new ValidatorException(new FacesMessage(""));
        }
    }
	
	public void logoutAction(ActionEvent event) {
		log.info("logoutAction");
    	try {
	    	FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
			session.invalidate();
			facesContext.getExternalContext().redirect("/login");
		} catch (IOException e) {
			log.error(e.getMessage());
		}
    }
	
	public void volverPerfil(ActionEvent event) {
		log.info("volverPerfil");
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
			session.setAttribute("usuarioDrillDown", null);
			cleanBean();
			facesContext.getExternalContext().redirect(this.redirect);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
	
	public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage("loginform:messages", message);
    }
	
	public void cleanBean() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.getExternalContext().getSessionMap().remove("tableroBean");
		facesContext.getExternalContext().getSessionMap().remove("perfilBean");
		facesContext.getExternalContext().getSessionMap().remove("mixProdBean");
		facesContext.getExternalContext().getSessionMap().remove("funnelBean");
		facesContext.getExternalContext().getSessionMap().remove("adminBean");
		facesContext.getExternalContext().getSessionMap().remove("cargaDatosBean");
	}
	
	public void cleanBeanAndRedirect(String redirect) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		try {
			cleanBean();
			facesContext.getExternalContext().redirect(redirect);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}
	
	public void regenerarClave() {
		try {
			validaUsuarioService.resetClaveUsuario("1", this.emailRecuperaClave, "123");
			addMessage("La nueva clave de acceso fue enviada con éxito");
		} catch (Exception e) {
			addMessage("No fue posible regenerar clave, e-mail no existe");
		}
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public boolean isChkAcepta() {
		return chkAcepta;
	}

	public void setChkAcepta(boolean chkAcepta) {
		this.chkAcepta = chkAcepta;
	}

	public boolean isRecordar() {
		return recordar;
	}

	public void setRecordar(boolean recordar) {
		this.recordar = recordar;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getEmailRecuperaClave() {
		return emailRecuperaClave;
	}

	public void setEmailRecuperaClave(String emailRecuperaClave) {
		this.emailRecuperaClave = emailRecuperaClave;
	}	
	
}
