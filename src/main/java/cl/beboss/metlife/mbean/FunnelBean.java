package cl.beboss.metlife.mbean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.primefaces.model.chart.DonutChartModel;

import com.google.gson.Gson;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.FunnelActXSubPeriodo;
import cl.beboss.metlife.dto.RatioActividadComercial;
import cl.beboss.metlife.dto.SubPeriodos;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.services.FunnelService;
import cl.beboss.metlife.utils.MathUtils;

@ManagedBean(name="funnelBean")
@SessionScoped
public class FunnelBean extends BaseViewBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger( FunnelBean.class.getName() );
	private FacesContext facesContext = FacesContext.getCurrentInstance();
	
	private FunnelService funnelService 
	= CDI.current().select(FunnelService.class).get();
	
	private String respuesta;
	private DonutChartModel donutRatios;
	private int emitidas;
	private int sometidas;
	private String idSubPeriodoSelect;
	private FunnelActXSubPeriodo funnelActXSubPeriodo;
	private List<SubPeriodos> listSubPeriodos; 
	
	@PostConstruct
    public void initTableroBean() {
		log.info("initTableroBean");
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		this.usuario = (UsuarioSession) session.getAttribute("usuario");
		this.usuarioDrillDown = (UsuarioSession) session.getAttribute("usuarioDrillDown");
		cargaFunnel();
		cargaRatioProd();
    }
	
	public void limpiaFiltroSemana() {
		this.idSubPeriodoSelect = null;
		cargaFunnel();
	}
	
	public void cargaFunnelDiario() {
		if (this.idSubPeriodoSelect == null) {
			cargaFunnel();
		} else {
			try {
				if (this.usuarioDrillDown != null) {
					if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioGA(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.idSubPeriodoSelect, this.usuarioDrillDown.getToken());
					} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioGU(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.idSubPeriodoSelect, this.usuarioDrillDown.getToken());
					} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioAS(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.idSubPeriodoSelect, this.usuarioDrillDown.getToken());
					}
				}else {
					if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilCorp)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiario(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.idSubPeriodoSelect, this.usuario.getToken());
					} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioGA(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.idSubPeriodoSelect, this.usuario.getToken());
					} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioGU(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.idSubPeriodoSelect, this.usuario.getToken());
					} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
							|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
						this.funnelActXSubPeriodo = funnelService.getFunnelDiarioAS(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.idSubPeriodoSelect, this.usuario.getToken());
					}
				}
				this.respuesta = new Gson().toJson(this.funnelActXSubPeriodo);
				
				String nombre;
				for (RatioActividadComercial r:this.funnelActXSubPeriodo.getRatioActividadComercial()) {
					nombre = r.getNombre();
					if(nombre.equalsIgnoreCase("Sometidas")) {
						sometidas = Integer.parseInt(MathUtils.quitarPunto(r.getValor())); 
					}
					if(nombre.equalsIgnoreCase("Emitidas")) {
						emitidas = Integer.parseInt(MathUtils.quitarPunto(r.getValor())); 
					}
				}
			} catch (Exception e) {
				log.info(e,e);
			}
		}
	}
	
	private void cargaFunnel() {
		try {
			
			if (this.usuarioDrillDown != null) {
				if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
					this.respuesta = funnelService.getFunnelGA(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.usuarioDrillDown.getToken());
				} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
					this.respuesta = funnelService.getFunnelGU(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.usuarioDrillDown.getToken());
				} else if (this.usuarioDrillDown.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor)) {
					this.respuesta = funnelService.getFunnelAS(usuarioDrillDown.getIdEmpresa(), usuarioDrillDown.getIdPersonal(), this.usuarioDrillDown.getToken());
				}
			}else {
				if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilCorp)) {
					this.respuesta = funnelService.getFunnel(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGA)) {
					this.respuesta = funnelService.getFunnelGA(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilGU)) {
					this.respuesta = funnelService.getFunnelGU(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.usuario.getToken());
				} else if (this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAsesor) 
						|| this.usuario.getPerfil().equalsIgnoreCase(Constantes.perfilAs)) {
					this.respuesta = funnelService.getFunnelAS(usuario.getIdEmpresa(), usuario.getIdPersonal(), this.usuario.getToken());
				}
			}
			
			String nombre;
			Integer idPeriodo;
			if (this.respuesta != null) {
				JSONObject obj = new JSONObject(this.respuesta);
				JSONArray arrayRatios = obj.getJSONObject("data").getJSONArray("RatioActividadComercial");
				
				JSONArray arrayPeriodos = null;
				JSONArray locs = obj.getJSONObject("data").getJSONArray("FunnelActividades");
				try {
					JSONObject json_o = locs.getJSONObject(0);
					arrayPeriodos = json_o.getJSONArray("SubPeriodos");
				} catch (Exception e) {
					log.info("opcion 1 no sirve");
				}
				
				for(int i = 0; i < arrayRatios.length(); i++){
					JSONObject json_obj = arrayRatios.getJSONObject(i);
					nombre = json_obj.getString("Nombre");
					
					if(nombre.equalsIgnoreCase("Sometidas")) {
						sometidas = json_obj.getInt("Valor");
					}
					if(nombre.equalsIgnoreCase("Emitidas")) {
						emitidas = json_obj.getInt("Valor");
					}
		        }
				listSubPeriodos = new LinkedList<SubPeriodos>();
				for(int i = 0; i < arrayPeriodos.length(); i++){
					JSONObject json_obj = arrayPeriodos.getJSONObject(i);
					nombre = json_obj.getString("NombreSubPeriodo");
					idPeriodo = json_obj.getInt("IdSubPeriodo");
					
					SubPeriodos sub = new SubPeriodos();
					sub.setNombreSubPeriodo(nombre);
					sub.setIdSubPeriodo(idPeriodo.toString());
					listSubPeriodos.add(sub);
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	private void cargaRatioProd() {
		donutRatios = initDonutModel();
	}
	
	 private DonutChartModel initDonutModel() {
        DonutChartModel model = new DonutChartModel();
        
        Map<String, Number> circle1 = new LinkedHashMap<String, Number>();
        
        String valorE = MathUtils.formatoNumerico(String.valueOf(emitidas));
        String valorS = MathUtils.formatoNumerico(String.valueOf(sometidas));
       
        circle1.put("Emitidas&nbsp;:&nbsp;" + valorE, emitidas);
        circle1.put("Sometidas&nbsp;:&nbsp;" + valorS, sometidas);
        
        model.setExtender("chartExtender");
        model.setSeriesColors("FFC600, 36A2EB");
        model.setLegendPosition("e");
        model.setFill(true);
        model.setShowDataLabels(true);
        model.setShadow(false);
        model.addCircle(circle1);
        model.setSliceMargin(2);
        return model;
    }
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public DonutChartModel getDonutRatios() {
		return donutRatios;
	}

	public void setDonutRatios(DonutChartModel donutRatios) {
		this.donutRatios = donutRatios;
	}

	public int getEmitidas() {
		return emitidas;
	}

	public void setEmitidas(int emitidas) {
		this.emitidas = emitidas;
	}

	public int getSometidas() {
		return sometidas;
	}

	public void setSometidas(int sometidas) {
		this.sometidas = sometidas;
	}

	public List<SubPeriodos> getListSubPeriodos() {
		return listSubPeriodos;
	}

	public void setListSubPeriodos(List<SubPeriodos> listSubPeriodos) {
		this.listSubPeriodos = listSubPeriodos;
	}

	public String getIdSubPeriodoSelect() {
		return idSubPeriodoSelect;
	}

	public void setIdSubPeriodoSelect(String idSubPeriodoSelect) {
		this.idSubPeriodoSelect = idSubPeriodoSelect;
	}

	public FunnelActXSubPeriodo getFunnelActXSubPeriodo() {
		return funnelActXSubPeriodo;
	}

	public void setFunnelActXSubPeriodo(FunnelActXSubPeriodo funnelActXSubPeriodo) {
		this.funnelActXSubPeriodo = funnelActXSubPeriodo;
	}
	
}
