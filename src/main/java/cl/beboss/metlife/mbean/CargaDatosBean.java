package cl.beboss.metlife.mbean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.spi.CDI;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import cl.beboss.metlife.dto.CargaFunnelRealDiaAses;
import cl.beboss.metlife.dto.CargaPlanificaPerAses;
import cl.beboss.metlife.dto.FunnelActividades;
import cl.beboss.metlife.dto.InfoFunnelRealDiaAses;
import cl.beboss.metlife.dto.InfoPlanificaPerAses;
import cl.beboss.metlife.dto.InfoProdRealPerAses;
import cl.beboss.metlife.dto.PeriodoVigente;
import cl.beboss.metlife.dto.Productos;
import cl.beboss.metlife.dto.RespuestaGetPerfilAS;
import cl.beboss.metlife.dto.SubProductos;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.services.CargaDatosAsService;
import cl.beboss.metlife.services.PerfilService;

@ManagedBean(name="cargaDatosBean")
@SessionScoped
public class CargaDatosBean extends BaseViewBean implements Serializable{

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger( CargaDatosBean.class.getName() );
	
	private CargaDatosAsService cargaDatosAsService = CDI.current().select(CargaDatosAsService.class).get();
	private PerfilService perfilService = CDI.current().select(PerfilService.class).get();
	
	private FacesContext facesContext = FacesContext.getCurrentInstance();
	private RespuestaGetPerfilAS respuestaGetPerfilAS;
	private String tramo;
	private String bono;
	private List<FunnelActividades> listFunnelActividades;
	private PeriodoVigente periodoVigente;
	private InfoPlanificaPerAses infoPlanificaPerAses;
	private InfoProdRealPerAses infoProdRealPerAses;
	
	@PostConstruct
    public void initCargaDatosBean() {
		log.info("initCargaDatosBean");
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		this.usuario = (UsuarioSession) session.getAttribute("usuario");
		cargaInicialPerfil();
    }
	
	public void cargaInicialPerfil() {
		consultaTramo();
		cargaDatos();
	}
	
	public void cargaDatos() {
		try {
			InfoFunnelRealDiaAses infoFunnelRealDiaAses = cargaDatosAsService.getInfoFunnelRealDiaAses(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.usuario.getIdPersonal(), this.usuario.getToken());
			this.listFunnelActividades = infoFunnelRealDiaAses.getFunnelActividades();
			this.periodoVigente = cargaDatosAsService.getPeriodoVigente(this.usuario.getIdEmpresa(), this.usuario.getToken());
			this.infoPlanificaPerAses = cargaDatosAsService.getInfoPlanificaPerAses(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.usuario.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuario.getToken());
			this.infoProdRealPerAses = cargaDatosAsService.getInfoProdRealPerAses(this.usuario.getIdEmpresa(), this.usuario.getIdPersonal(), this.usuario.getIdPersonal(), this.periodoVigente.getIdPeriodo(), this.usuario.getToken());				
		} catch (Exception e) {
			log.info(e,e);
		}
	}
	
	public void guardar() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			CargaFunnelRealDiaAses cargaFunnelRealDiaAses = new CargaFunnelRealDiaAses();
			cargaFunnelRealDiaAses.setIdAsesor(this.usuario.getIdPersonal());
			cargaFunnelRealDiaAses.setIdEmpresa(this.usuario.getIdEmpresa());
			cargaFunnelRealDiaAses.setIdPersonal(this.usuario.getIdPersonal());
			cargaFunnelRealDiaAses.setToken(this.usuario.getToken());
			cargaFunnelRealDiaAses.setFecha(sdf.format(new Date()));
			cargaFunnelRealDiaAses.setFunnelActividades(new ArrayList<FunnelActividades>());
			
			List<FunnelActividades> listFunnel = new ArrayList<FunnelActividades>();
			for (FunnelActividades f: this.listFunnelActividades) {	
				FunnelActividades fun = new FunnelActividades();
				fun.setIdFunnelActividad(f.getIdFunnelActividad());
				fun.setReal(f.getHoy());
				listFunnel.add(fun);
			}
			cargaFunnelRealDiaAses.setFunnelActividades(listFunnel);
			cargaDatosAsService.cargaFunnelRealDiaAses(cargaFunnelRealDiaAses);
			addMessage("Datos almacenados con éxito");
		} catch (Exception e) {
			addMessage("ERROR: " + e.getMessage());
			log.info(e,e);
		}
	}
	
	public void validar() {
		try {
			CargaPlanificaPerAses cargaPlanificaPerAses = new CargaPlanificaPerAses();
			cargaPlanificaPerAses.setIdAsesor(this.usuario.getIdPersonal());
			cargaPlanificaPerAses.setIdEmpresa(this.usuario.getIdEmpresa());
			cargaPlanificaPerAses.setIdPersonal(this.usuario.getIdPersonal());
			cargaPlanificaPerAses.setIdPeriodo(this.periodoVigente.getIdPeriodo());
			cargaPlanificaPerAses.setToken(this.usuario.getToken());
			cargaPlanificaPerAses.setFormaIngreso("1");
			cargaPlanificaPerAses.setIdEstadoPlan("E");
			cargaPlanificaPerAses.setTotalClientes(this.infoPlanificaPerAses.getTotalClientes() != null? this.infoPlanificaPerAses.getTotalClientes().toString():"0");
			cargaPlanificaPerAses.setPrimaEsporadica(this.infoPlanificaPerAses.getPrimaEsporadica() != null? this.infoPlanificaPerAses.getPrimaEsporadica().toString():"0");
			
			List<SubProductos> subProductos = new LinkedList<SubProductos>();
			for (Productos productos: this.infoPlanificaPerAses.getProductos()) {
				for (SubProductos sub: productos.getSubProductos()) {
					subProductos.add(sub);
				}
			}
			cargaPlanificaPerAses.setSubProductos(subProductos);
			cargaDatosAsService.cargaPlanificaPerAses(cargaPlanificaPerAses);
			addMessage("Datos almacenados con éxito");
		} catch (Exception e) {
			addMessage("ERROR: " + e.getMessage());
			log.info(e,e);
		}
	}
	
	public void simular() {
		try {
			CargaPlanificaPerAses cargaPlanificaPerAses = new CargaPlanificaPerAses();
			cargaPlanificaPerAses.setIdAsesor(this.usuario.getIdPersonal());
			cargaPlanificaPerAses.setIdEmpresa(this.usuario.getIdEmpresa());
			cargaPlanificaPerAses.setIdPersonal(this.usuario.getIdPersonal());
			cargaPlanificaPerAses.setIdPeriodo(this.periodoVigente.getIdPeriodo());
			cargaPlanificaPerAses.setToken(this.usuario.getToken());
			cargaPlanificaPerAses.setFormaIngreso("1");
			cargaPlanificaPerAses.setIdEstadoPlan("S");
			cargaPlanificaPerAses.setTotalClientes(this.infoPlanificaPerAses.getTotalClientes() != null? this.infoPlanificaPerAses.getTotalClientes().toString():"0");
			cargaPlanificaPerAses.setPrimaEsporadica(this.infoPlanificaPerAses.getPrimaEsporadica() != null? this.infoPlanificaPerAses.getPrimaEsporadica().toString():"0");
	
			List<SubProductos> subProductos = new LinkedList<SubProductos>();
			for (Productos productos: this.infoPlanificaPerAses.getProductos()) {
				for (SubProductos sub: productos.getSubProductos()) {
					subProductos.add(sub);
				}
			}
			cargaPlanificaPerAses.setSubProductos(subProductos);
			cargaDatosAsService.cargaPlanificaPerAses(cargaPlanificaPerAses);
			addMessage("Datos almacenados con éxito");
		} catch (Exception e) {
			addMessage("ERROR: " + e.getMessage());
			log.info(e,e);
		}
	}

	public void consultaTramo() {
		try {
			this.respuestaGetPerfilAS = perfilService.getPerfilAS(this.usuario);
			this.tramo = this.respuestaGetPerfilAS.getTramoAsesor().getTramo();
			
			if(tramo.equalsIgnoreCase("0")) {
				this.bono = "0";
			}else {	
				for (int i=0;i<=this.respuestaGetPerfilAS.getBonos().size()-1;i++) {
					if(this.respuestaGetPerfilAS.getBonos().get(i).getTramo().equalsIgnoreCase(tramo)) {
						this.bono = this.respuestaGetPerfilAS.getBonos().get(i).getBono();
						this.bono = this.bono.replace(".",",");
					}
				}
			}
		} catch (Exception e) {
			log.info(e,e);
		}
	}

	public String getTramo() {
		return tramo;
	}

	public void setTramo(String tramo) {
		this.tramo = tramo;
	}

	public String getBono() {
		return bono;
	}

	public void setBono(String bono) {
		this.bono = bono;
	}

	public List<FunnelActividades> getListFunnelActividades() {
		return listFunnelActividades;
	}

	public void setListFunnelActividades(List<FunnelActividades> listFunnelActividades) {
		this.listFunnelActividades = listFunnelActividades;
	}

	public PeriodoVigente getPeriodoVigente() {
		return periodoVigente;
	}

	public void setPeriodoVigente(PeriodoVigente periodoVigente) {
		this.periodoVigente = periodoVigente;
	}
	
	public InfoPlanificaPerAses getInfoPlanificaPerAses() {
		return infoPlanificaPerAses;
	}

	public void setInfoPlanificaPerAses(InfoPlanificaPerAses infoPlanificaPerAses) {
		this.infoPlanificaPerAses = infoPlanificaPerAses;
	}

	public InfoProdRealPerAses getInfoProdRealPerAses() {
		return infoProdRealPerAses;
	}

	public void setInfoProdRealPerAses(InfoProdRealPerAses infoProdRealPerAses) {
		this.infoProdRealPerAses = infoProdRealPerAses;
	}
}
