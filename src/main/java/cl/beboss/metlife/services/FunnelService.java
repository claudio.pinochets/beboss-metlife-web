package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.FunnelActXSubPeriodo;
import cl.beboss.metlife.dto.RequerimientoGetFunnel;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class FunnelService implements Serializable {
	private static final Logger log = Logger.getLogger( FunnelService.class.getName() );
	
	public String getFunnel(String idEmpresa, String idPersonal, String token) throws Exception {
    	log.info("getFunnel");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActividadesCorp", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getFunnelGA(String idEmpresa, String idPersonal, String token) throws Exception {
    	log.info("getFunnelGA");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActividadesGA", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getFunnelGU(String idEmpresa, String idPersonal, String token) throws Exception {
    	log.info("getFunnelGU");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActividadesGU", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getFunnelAS(String idEmpresa, String idPersonal, String token) throws Exception {
    	log.info("getFunnelAS");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActividadesAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	@SuppressWarnings("rawtypes")
	public FunnelActXSubPeriodo getFunnelDiario(String idEmpresa, String idPersonal, String idSubPeriodo, String token) throws Exception {
    	log.info("getFunnelDiario");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdSubPeriodo(idSubPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActXSubPeriodoCorp", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<FunnelActXSubPeriodo>() {}.getType();
		FunnelActXSubPeriodo funnelActXSubPeriodo = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return funnelActXSubPeriodo;
    }
	
	@SuppressWarnings("rawtypes")
	public FunnelActXSubPeriodo getFunnelDiarioGU(String idEmpresa, String idPersonal, String idSubPeriodo, String token) throws Exception {
    	log.info("getFunnelDiarioGU");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdSubPeriodo(idSubPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActXSubPeriodoGU", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<FunnelActXSubPeriodo>() {}.getType();
		FunnelActXSubPeriodo funnelActXSubPeriodo = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return funnelActXSubPeriodo;
    }
	
	@SuppressWarnings("rawtypes")
	public FunnelActXSubPeriodo getFunnelDiarioGA(String idEmpresa, String idPersonal, String idSubPeriodo, String token) throws Exception {
    	log.info("getFunnelDiarioGA");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdSubPeriodo(idSubPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActXSubPeriodoGA", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<FunnelActXSubPeriodo>() {}.getType();
		FunnelActXSubPeriodo funnelActXSubPeriodo = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return funnelActXSubPeriodo;
    }
	
	@SuppressWarnings("rawtypes")
	public FunnelActXSubPeriodo getFunnelDiarioAS(String idEmpresa, String idPersonal, String idSubPeriodo, String token) throws Exception {
    	log.info("getFunnelDiarioAS");
    	RequerimientoGetFunnel req = new RequerimientoGetFunnel();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdSubPeriodo(idSubPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/FunnelActXSubPeriodoAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<FunnelActXSubPeriodo>() {}.getType();
		FunnelActXSubPeriodo funnelActXSubPeriodo = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return funnelActXSubPeriodo;
    }
}
