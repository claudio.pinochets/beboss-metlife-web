package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.CargaFunnelRealDiaAses;
import cl.beboss.metlife.dto.CargaPlanificaPerAses;
import cl.beboss.metlife.dto.InfoFunnelRealDiaAses;
import cl.beboss.metlife.dto.InfoPlanificaPerAses;
import cl.beboss.metlife.dto.InfoProdRealPerAses;
import cl.beboss.metlife.dto.PeriodoVigente;
import cl.beboss.metlife.dto.RequerimientoGetInfoFunnelRealDiaAses;
import cl.beboss.metlife.dto.RequerimientoGetInfoPlanificaPerAses;
import cl.beboss.metlife.dto.RequerimientoGetPeriodoVigente;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.dto.SubProductos;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class CargaDatosAsService implements Serializable{
	private static final Logger log = Logger.getLogger( CargaDatosAsService.class.getName() );

	@SuppressWarnings("rawtypes")
	public InfoFunnelRealDiaAses getInfoFunnelRealDiaAses(String idEmpresa, String idPersonal, String idAsesor, String token) throws Exception {
    	log.info("getInfoFunnelRealDiaAses");
    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    	
    	RequerimientoGetInfoFunnelRealDiaAses req = new RequerimientoGetInfoFunnelRealDiaAses();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdAsesor(idAsesor);
    	req.setFecha(sdf.format(new Date()));
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/InfoFunnelRealDiaAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<InfoFunnelRealDiaAses>() {}.getType();
		InfoFunnelRealDiaAses infoFunnelRealDiaAses = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return infoFunnelRealDiaAses;
    }
	
	@SuppressWarnings("rawtypes")
	public InfoPlanificaPerAses getInfoPlanificaPerAses(String idEmpresa, String idPersonal, String idAsesor, String idPeriodo, String token) throws Exception {
    	log.info("getInfoPlanificaPerAses");
    	
    	RequerimientoGetInfoPlanificaPerAses req = new RequerimientoGetInfoPlanificaPerAses();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdAsesor(idAsesor);
    	req.setIdPeriodo(idPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/InfoPlanificaPerAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<InfoPlanificaPerAses>() {}.getType();
		InfoPlanificaPerAses infoPlanificaPerAses = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return infoPlanificaPerAses;
    }
	
	@SuppressWarnings("rawtypes")
	public PeriodoVigente getPeriodoVigente(String idEmpresa, String token) throws Exception {
    	log.info("getPeriodoVigente");
    	
    	RequerimientoGetPeriodoVigente req = new RequerimientoGetPeriodoVigente();
    	req.setIdEmpresa(idEmpresa);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/ConsultarPeriodoVigente", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<PeriodoVigente>() {}.getType();
		PeriodoVigente periodoVigente = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return periodoVigente;
    }	
	
	@SuppressWarnings("rawtypes")
	public Boolean cargaFunnelRealDiaAses(CargaFunnelRealDiaAses cargaFunnelRealDiaAses) throws Exception {
    	log.info("cargaFunnelRealDiaAses");
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/CargaFunnelRealDiaAses", cargaFunnelRealDiaAses);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception(respuesta.getMsgError());
		}
		return null;
    }	
	
	@SuppressWarnings("rawtypes")
	public Boolean cargaPlanificaPerAses(CargaPlanificaPerAses cargaPlanificaPerAses) throws Exception {
    	log.info("cargaFunnelRealDiaAses");
    	
    	if (cargaPlanificaPerAses.getTotalClientes() != null) {
    		cargaPlanificaPerAses.setTotalClientes(cargaPlanificaPerAses.getTotalClientes().replace(",", "."));
		}
    	if (cargaPlanificaPerAses.getPrimaEsporadica() != null) {
    		cargaPlanificaPerAses.setPrimaEsporadica(cargaPlanificaPerAses.getPrimaEsporadica().replace(",", "."));
		}
    	for(SubProductos s:cargaPlanificaPerAses.getSubProductos()) {
    		if (s.getCantidadPolizas() != null) {
    			s.setCantidadPolizas(s.getCantidadPolizas().replace(",", "."));
    		}
    		if (s.getPrimaBasica() != null) {
    			s.setPrimaBasica(s.getPrimaBasica().replace(",", "."));
    		}
    		if (s.getPrimaExceso() != null) {
    			s.setPrimaExceso(s.getPrimaExceso().replace(",", "."));
    		}
    	}
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/CargaPlanificaPerAses", cargaPlanificaPerAses);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception(respuesta.getMsgError());
		}
		return null;
    }	
	
	@SuppressWarnings("rawtypes")
	public InfoProdRealPerAses getInfoProdRealPerAses(String idEmpresa, String idPersonal, String idAsesor, String idPeriodo, String token) throws Exception {
    	log.info("cargaFunnelRealDiaAses");
    	
    	RequerimientoGetInfoPlanificaPerAses req = new RequerimientoGetInfoPlanificaPerAses();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdAsesor(idAsesor);
    	req.setIdPeriodo(idPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/InfoProdRealPerAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		
		Type type = new TypeToken<InfoProdRealPerAses>() {}.getType();
		InfoProdRealPerAses infoProdRealPerAses = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return infoProdRealPerAses;
    }	
}
