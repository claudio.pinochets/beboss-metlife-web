package cl.beboss.metlife.services;

import java.io.Serializable;
import java.util.logging.Logger;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.RequerimientoGetTableroMando;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class MixProdService implements Serializable {
	private static final Logger log = Logger.getLogger( MixProdService.class.getName() );
	
	public String getMixProd(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getMixProd");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/MixCorp", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getMixGA(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getMixGA");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/MixGA", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getMixGU(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getMixGU");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/MixGU", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getMixAS(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getMixAS");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/MixAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
}
