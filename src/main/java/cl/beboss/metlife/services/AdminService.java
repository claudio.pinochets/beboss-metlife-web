package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.RequerimientoAdminGetUsuarios;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.dto.RespuestaAdminGetUsuarios;
import cl.beboss.metlife.dto.Usuario;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class AdminService implements Serializable {
	private static final Logger log = Logger.getLogger( AdminService.class.getName() );
	
	@SuppressWarnings("rawtypes")
	public List<Usuario> getListaUsuarios(UsuarioSession usuario, String busqueda, String cantRegisXPag, String token) throws Exception {
    	log.info("getListaUsuarios");
    	
    	RequerimientoAdminGetUsuarios req = new RequerimientoAdminGetUsuarios();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setBusqueda(busqueda);
    	req.setCantRegisXPag(cantRegisXPag);
    	req.setToken(token);
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/ListaUsuarios", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			return null;
		}
		Type type = new TypeToken<RespuestaAdminGetUsuarios>() {}.getType();
		RespuestaAdminGetUsuarios respuestaAdminGetUsuarios = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaAdminGetUsuarios.getUsuarios();
    }
}
