package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.RequerimientoGetUsuario;
import cl.beboss.metlife.dto.RequerimientoResetClaveUsuario;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.dto.RespuestaResetClaveUsuario;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.utils.MailUtil;
import cl.beboss.metlife.utils.Md5Util;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class ValidaUsuarioService implements Serializable {
	private static final Logger log = Logger.getLogger( ValidaUsuarioService.class.getName() );
	
    @SuppressWarnings("rawtypes")
	public UsuarioSession getUsuario(String idEmpresa, String user, String pass) throws Exception {
    	log.info("getUsuario");
    	RequerimientoGetUsuario req = new RequerimientoGetUsuario();
    	req.setIdEmpresa(idEmpresa);
    	req.setUser(user);
    	req.setPass(Md5Util.MD5(pass));
    			
		String respuestaJson = null;
		UsuarioSession usuario = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/Login", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() == 0) {
			Type type = new TypeToken<UsuarioSession>() {}.getType();
			usuario = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);
		}else if (respuesta.getCodError() != 1) {
			throw new Exception(respuesta.getMsgError());
		}
		
		return usuario;
    }
    
    @SuppressWarnings("rawtypes")
	public UsuarioSession resetClaveUsuario(String idEmpresa, String mail, String token) throws Exception {
    	log.info("getUsuario");
    	RequerimientoResetClaveUsuario req = new RequerimientoResetClaveUsuario();
    	req.setIdEmpresa(idEmpresa);
    	req.setMail(mail);
    	req.setToken(token);
    			
		String respuestaJson = null;
		RespuestaResetClaveUsuario respuestaResetClaveUsuario = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PersonaResetPass", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() == 0) {
			Type type = new TypeToken<RespuestaResetClaveUsuario>() {}.getType();
			respuestaResetClaveUsuario = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);
		}else if (respuesta.getCodError() != 1) {
			throw new Exception();
		}
		
		if (respuestaResetClaveUsuario.getPassNuevo() != null && !respuestaResetClaveUsuario.getPassNuevo().isEmpty()) {
			MailUtil.sendMail(mail, respuestaResetClaveUsuario.getNombres() + " " + respuestaResetClaveUsuario.getApellidos(), respuestaResetClaveUsuario.getPassNuevo());
		}
		
		return null;
    }
}
