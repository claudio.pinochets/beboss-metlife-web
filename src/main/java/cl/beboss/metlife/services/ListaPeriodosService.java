package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.RequerimientoGetListaPeriodos;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.dto.RespuestaGetListaPeriodos;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class ListaPeriodosService implements Serializable{
private static final Logger log = Logger.getLogger( FunnelService.class.getName() );
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetListaPeriodos getListaPeriodos(String idEmpresa, String token) throws Exception {
    	log.info("getListaPeriodos");
    	RequerimientoGetListaPeriodos req = new RequerimientoGetListaPeriodos();
    	req.setIdEmpresa(idEmpresa);
    	req.setToken(token);
    			
    	String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/ListaPeriodosHist", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetListaPeriodos>() {}.getType();
		RespuestaGetListaPeriodos respuestaGetListaPeriodos = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetListaPeriodos;
    }
}
