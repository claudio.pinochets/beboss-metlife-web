package cl.beboss.metlife.services;

import java.io.Serializable;
import java.util.logging.Logger;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.RequerimientoGetTableroMando;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class TableroService implements Serializable {
	private static final Logger log = Logger.getLogger( TableroService.class.getName() );
	
	public String getTableroMandoCorp(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getTableroMando");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/TMCorp", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getTableroMandoGA(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getTableroMando");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/TMGA", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getTableroMandoGU(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getTableroMando");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/TMGU", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getTableroMandoAS(String idEmpresa, String idPersonal, String idPeriodoDesde, String idPeriodoHasta, String token) throws Exception {
    	log.info("getTableroMando");
    	RequerimientoGetTableroMando req = new RequerimientoGetTableroMando();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodoDesde(idPeriodoDesde);
    	req.setIdPeriodoHasta(idPeriodoHasta);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/TMAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
}
