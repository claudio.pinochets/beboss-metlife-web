package cl.beboss.metlife.services;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.logging.Logger;

import com.google.gson.reflect.TypeToken;

import cl.beboss.metlife.constantes.Constantes;
import cl.beboss.metlife.dto.PlanAsesor;
import cl.beboss.metlife.dto.RequerimientoApruebaRechazaPlan;
import cl.beboss.metlife.dto.RequerimientoCambiaEstadoUsuario;
import cl.beboss.metlife.dto.RequerimientoCambioClaveUsuario;
import cl.beboss.metlife.dto.RequerimientoGetAsesoresXGUPerfilCorp;
import cl.beboss.metlife.dto.RequerimientoGetGUXGAPerfilCorp;
import cl.beboss.metlife.dto.RequerimientoGetPerfilCorp;
import cl.beboss.metlife.dto.RequerimientoGetPerfilGA;
import cl.beboss.metlife.dto.RequerimientoGetPerfilGU;
import cl.beboss.metlife.dto.RequerimientoGetPlanificacionAS;
import cl.beboss.metlife.dto.RequerimientoGetSueldoMesActualAses;
import cl.beboss.metlife.dto.Respuesta;
import cl.beboss.metlife.dto.RespuestaGetGUXGAPerfilCorp;
import cl.beboss.metlife.dto.RespuestaGetPerfilAS;
import cl.beboss.metlife.dto.RespuestaGetPerfilCorp;
import cl.beboss.metlife.dto.RespuestaGetPerfilGA;
import cl.beboss.metlife.dto.RespuestaGetPerfilGU;
import cl.beboss.metlife.dto.RespuestaGetPlanificacionAS;
import cl.beboss.metlife.dto.UsuarioSession;
import cl.beboss.metlife.utils.Md5Util;
import cl.beboss.metlife.utils.RestAccessUtil;

@SuppressWarnings("serial")
public class PerfilService implements Serializable {
	private static final Logger log = Logger.getLogger( PerfilService.class.getName() );
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetPerfilCorp getListaPerfilCorp(UsuarioSession usuario, String region) throws Exception {
    	log.info("getListaPerfilCorp");
    	
    	RequerimientoGetPerfilCorp req = new RequerimientoGetPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setRegion(region);
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PerfilCorp", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetPerfilCorp>() {}.getType();
		RespuestaGetPerfilCorp respuestaGetPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaGUXGAPerfilCorp(UsuarioSession usuario, Integer idGA) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetGUXGAPerfilCorp req = new RequerimientoGetGUXGAPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGA(idGA.toString());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerGUXGAPerfilCorp", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaGUXGAPerfilGA(UsuarioSession usuario, Integer idGA) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetGUXGAPerfilCorp req = new RequerimientoGetGUXGAPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGA(idGA.toString());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerGUXGA", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaGUXGAPerfilGU(UsuarioSession usuario, Integer idGA) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetGUXGAPerfilCorp req = new RequerimientoGetGUXGAPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGA(idGA.toString());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerGUXGA", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaAsesoresXGUPerfilCorp(UsuarioSession usuario, Integer idGU) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetAsesoresXGUPerfilCorp req = new RequerimientoGetAsesoresXGUPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGU(idGU.toString());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerAsesoresXGUPerfilCorp", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaAsesoresXGUPerfilGA(UsuarioSession usuario, Integer idGU) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetAsesoresXGUPerfilCorp req = new RequerimientoGetAsesoresXGUPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGU(idGU.toString());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerAsXGUXMeta", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetGUXGAPerfilCorp getListaAsesoresPerfilGU(UsuarioSession usuario) throws Exception {
    	log.info("getListaGUXGAPerfilCorp");
    	
    	RequerimientoGetAsesoresXGUPerfilCorp req = new RequerimientoGetAsesoresXGUPerfilCorp();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdGU(usuario.getIdPersonal());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/VerAsXGUXMeta", req);
		} catch (Exception e) {
			throw new Exception();
		}

		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetGUXGAPerfilCorp>() {}.getType();
		RespuestaGetGUXGAPerfilCorp respuestaGetGUXGAPerfilCorp = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetGUXGAPerfilCorp;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetPerfilGA getPerfilGA(UsuarioSession usuario) throws Exception {
    	log.info("getPerfilGA");
    	
    	RequerimientoGetPerfilGA req = new RequerimientoGetPerfilGA();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PerfilGA", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetPerfilGA>() {}.getType();
		RespuestaGetPerfilGA respuestaGetPerfilGA = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetPerfilGA;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetPerfilGU getPerfilGU(UsuarioSession usuario) throws Exception {
    	log.info("getPerfilGU");
    	
    	RequerimientoGetPerfilGU req = new RequerimientoGetPerfilGU();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PerfilGU", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetPerfilGU>() {}.getType();
		RespuestaGetPerfilGU respuestaGetPerfilGU = RestAccessUtil.gson().fromJson(respuesta.getDataJson(), type);
		return respuestaGetPerfilGU;
    }
	
	@SuppressWarnings("rawtypes")
	public RespuestaGetPerfilAS getPerfilAS(UsuarioSession usuario) throws Exception {
    	log.info("getPerfilAS");
    	
    	RequerimientoGetPerfilGU req = new RequerimientoGetPerfilGU();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(usuario.getIdPersonal());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PerfilAs", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetPerfilAS>() {}.getType();
		RespuestaGetPerfilAS respuestaGetPerfilAS = RestAccessUtil.gson().fromJson(respuesta.getDataJson(), type);
		return respuestaGetPerfilAS;
    }
	
	@SuppressWarnings("rawtypes")
	public boolean cambiaEstadoUsuario(UsuarioSession usuario, String idSuperior, String idPersonal, String estadoOld, String estadoNew) throws Exception {
    	log.info("cambiaEstadoUsuario");
    	
    	RequerimientoCambiaEstadoUsuario req = new RequerimientoCambiaEstadoUsuario();
    	req.setIdEmpresa(usuario.getIdEmpresa());
    	req.setIdPersonal(idPersonal);
    	req.setIdSuperior(idSuperior);
    	req.setEstadoOld(estadoOld);
    	req.setEstadoNew(estadoNew);
    	req.setIdConsultor(usuario.getIdPersonal());
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/ActivaEstadoPersonal", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			return false;
		}
		return true;
    }
	
	@SuppressWarnings("rawtypes")
	public PlanAsesor getPlanificacionAsesor(UsuarioSession usuario, Integer idPlan) throws Exception {
    	log.info("getPlanificacionAsesor");
    	
    	RequerimientoGetPlanificacionAS req = new RequerimientoGetPlanificacionAS();
    	req.setIdPlan(idPlan);
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PlanificacionAsesorXId", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			throw new Exception();
		}
		Type type = new TypeToken<RespuestaGetPlanificacionAS>() {}.getType();
		RespuestaGetPlanificacionAS respuestaGetPlanificacionAS = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);

		return respuestaGetPlanificacionAS.getPlanAsesor();
    }
	
	@SuppressWarnings("rawtypes")
	public boolean apruebaRechazaPlan(UsuarioSession usuario, PlanAsesor planAsesor, String estado) throws Exception {
    	log.info("apruebaPlan");
    	
    	RequerimientoApruebaRechazaPlan req = new RequerimientoApruebaRechazaPlan();
    	req.setIdPersonal(usuario.getIdPersonal());
    	req.setIdPlan(planAsesor.getIdPlan());
    	req.setEstado(estado);
   			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/ApruebaPlanAsesor", req);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() != 0) {
			return false;
		}
		return true;
    }
	
	@SuppressWarnings("rawtypes")
	public UsuarioSession cambioClaveUsuario(String idEmpresa, String idPersonal, String user, String passActual, String passNuevo, String token) throws Exception {
    	log.info("getUsuario");
    	RequerimientoCambioClaveUsuario req = new RequerimientoCambioClaveUsuario();
    	req.setIdEmpresa(idEmpresa);
    	req.setUser(user);
    	req.setPassActual(Md5Util.MD5(passActual));
    	req.setPassNuevo(Md5Util.MD5(passNuevo));
    	req.setIdPersonal(idPersonal);
    	req.setToken(token);
    			
		String respuestaJson = null;
		UsuarioSession usuario = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/PersonaModificarPass", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		Respuesta respuesta = RestAccessUtil.gson().fromJson(respuestaJson, Respuesta.class);
		if (respuesta.getCodError() == 0) {
			Type type = new TypeToken<UsuarioSession>() {}.getType();
			usuario = RestAccessUtil.gson().fromJson(respuesta.getDataJson(),type);
		}else if (respuesta.getCodError() != 1) {
			throw new Exception();
		}
		
		return usuario;
    }
	
	public String getSueldoMesActualAses(String idEmpresa, String idPersonal, String idPeriodo, String token) throws Exception {
    	log.info("getSueldoMesActualAses");
    	RequerimientoGetSueldoMesActualAses req = new RequerimientoGetSueldoMesActualAses();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodo(idPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/SueldoMesActualAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
	
	public String getSueldoArrastreAses(String idEmpresa, String idPersonal, String idPeriodo, String token) throws Exception {
    	log.info("getSueldoArrastreAses");
    	RequerimientoGetSueldoMesActualAses req = new RequerimientoGetSueldoMesActualAses();
    	req.setIdEmpresa(idEmpresa);
    	req.setIdPersonal(idPersonal);
    	req.setIdPeriodo(idPeriodo);
    	req.setToken(token);
    			
		String respuestaJson = null;
		try {
			respuestaJson = RestAccessUtil.post(Constantes.SOA_SERVER + "/beboss/main/SueldoArrastreAses", req);
			log.info("respuestaJson=" + respuestaJson);
		} catch (Exception e) {
			throw new Exception();
		}
		
		return respuestaJson;
    }
}
