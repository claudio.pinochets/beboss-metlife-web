$(function () {
    "use strict";
    $(function () {
        $(".preloader").fadeOut();
    });
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation()
    });
    // ==============================================================
    // This is for the top header part and sidebar part
    // ==============================================================
    var set = function () {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        var topOffset = 55;
        if (width < 5170) {
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
            $(".sidebartoggler i").addClass("ti-menu");
        }
        else {
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
        }
         var height = ((window.innerHeight > 0) ? window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $(".page-wrapper").css("min-height", (height) + "px");
        }
    };
    $(window).ready(set);
    $(window).on("resize", set);
    // ==============================================================
    // Theme options
    // ==============================================================
    $(".sidebartoggler").on('click', function () {
        if ($("body").hasClass("mini-sidebar")) {
            $("body").trigger("resize");
            $("body").removeClass("mini-sidebar");
            $('.navbar-brand span').show();
        }
        else {
            $("body").trigger("resize");
            $("body").addClass("mini-sidebar");
            $('.navbar-brand span').hide();
        }
    });
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").click(function () {
        $("body").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
        $(".nav-toggler i").addClass("ti-close");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
    });
    // ==============================================================
    // Right sidebar options
    // ==============================================================
    $(".right-side-toggle").click(function () {
        $(".right-sidebar").slideDown(50);
        $(".right-sidebar").toggleClass("shw-rside");
    });
    // ==============================================================
    // This is for the floating labels
    // ==============================================================
    $('.floating-labels .form-control').on('focus blur', function (e) {
        $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
    }).trigger('blur');

    // ==============================================================
    //tooltip
    // ==============================================================
    $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    // ==============================================================
    //Popover
    // ==============================================================
    $(function () {
         $('[data-toggle="popover"]').popover()
    }) 

    // ==============================================================
    // Perfact scrollbar 
    // ==============================================================
    $('.scroll-sidebar, .right-side-panel, .message-center, .right-sidebar').perfectScrollbar();
	$('#chat, #msg, #comment, #todo').perfectScrollbar();
    // ==============================================================
    // Resize all elements
    // ==============================================================
    $("body").trigger("resize");
    // ==============================================================
    // To do list
    // ==============================================================
    $(".list-task li label").click(function () {
        $(this).toggleClass("task-done");
    });
    // ==============================================================
    // Collapsable cards
    // ==============================================================
    $('a[data-action="collapse"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="collapse"] i').toggleClass('ti-minus ti-plus');
        $(this).closest('.card').children('.card-body').collapse('toggle');
    });
    // Toggle fullscreen
    $('a[data-action="expand"]').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.card').find('[data-action="expand"] i').toggleClass('mdi-arrow-expand mdi-arrow-compress');
        $(this).closest('.card').toggleClass('card-fullscreen');
    });
    // Close Card
    $('a[data-action="close"]').on('click', function () {
        $(this).closest('.card').removeClass().slideUp('fast');
    });
    // ==============================================================
    // Color variation
    // ==============================================================

    var mySkins = [
        "skin-default",
        "skin-green",
        "skin-red",
        "skin-blue",
        "skin-purple",
        "skin-megna",
        "skin-default-dark",
        "skin-green-dark",
        "skin-red-dark",
        "skin-blue-dark",
        "skin-purple-dark",
        "skin-megna-dark"
    ]
        /**
         * Get a prestored setting
         *
         * @param String name Name of of the setting
         * @returns String The value of the setting | null
         */
    function get(name) {
        if (typeof (Storage) !== 'undefined') {
            return localStorage.getItem(name)
        }
        else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }
    /**
     * Store a new settings in the browser
     *
     * @param String name Name of the setting
     * @param String val Value of the setting
     * @returns void
     */
    function store(name, val) {
        if (typeof (Storage) !== 'undefined') {
            localStorage.setItem(name, val)
        }
        else {
            window.alert('Please use a modern browser to properly view this template!')
        }
    }

    /**
     * Replaces the old skin with the new skin
     * @param String cls the new skin class
     * @returns Boolean false to prevent link's default action
     */
    function changeSkin(cls) {
        $.each(mySkins, function (i) {
            $('body').removeClass(mySkins[i])
        })
        $('body').addClass(cls)
        store('skin', cls)
        return false
    }

    function setup() {
        var tmp = get('skin')
        if (tmp && $.inArray(tmp, mySkins)) changeSkin(tmp)
            // Add the change skin listener
        $('[data-skin]').on('click', function (e) {
            if ($(this).hasClass('knob')) return
            e.preventDefault()
            changeSkin($(this).data('skin'))
        })
    }
    setup()
    $("#themecolors").on("click", "a", function () {
        $("#themecolors li a").removeClass("working"),
        $(this).addClass("working")
    })

    // For Custom File Input
    $('.custom-file-input').on('change',function(){
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
    
});

//============================================================== 
//Bar chart option
//============================================================== 

function barChartAsesor(meta, real, grafico, titulo) {
	var data = {
	  labels: [''],
	  series: [
	    [meta],
	    [real]
	  ]
	};

	var options = {
		showPoint: false,
		showLine: false,
		showArea: true,
		fullWidth: true,
		showLabel: false,
		axisX: {
			showGrid: false,
			showLabel: false,
			offset: 0,
		},
		axisY: {
			showGrid: false,
			showLabel: false,
			offset: 0,
		},
		chartPadding: 0,
		low: 0,
		plugins: [
	      Chartist.plugins.ctBarLabels()
	    ],
	    seriesBarDistance: 30
	};

	var responsiveOptions = [
	  ['screen and (max-width: 640px)', {
	    seriesBarDistance: 5,
	    axisX: {
	      labelInterpolationFnc: function (value) {
	        return value[0];
	      }
	    }
	  }]
	];
	new Chartist.Bar(grafico, data, options, responsiveOptions);
}

function barChartCorporativo(presupuesto, meta, real, grafico, titulo) {
	
	Highcharts.setOptions({
	    lang: {
	      decimalPoint: ',',
	      thousandsSep: '.'
	    }
	});

	var myChart = echarts.init(document.getElementById(grafico));
	
	option = {
	    toolbox: {
		     show : false,
		     feature : {
		         magicType : {show: false, type: ['line', 'bar']},
		         restore : {show: false},
		         saveAsImage : {show: false}
		     }
		 },
		 color: ["#A7A8AA", "#00aca0","#db0a5b"],
		 calculable : true,
		 xAxis : [
		     {
		         type : 'category',
		         data : [titulo],
		         axisTick: {
		   	      	show: false
		         },
	             splitLine:{
	                 show: false
	             },
	             axisLine: { 
	            	 show: false 
	             }
		     }
		 ],
		 yAxis : [
		     {
		    	 show: false,
		         type : 'value',
	             axisTick: {
		   	      	show: false
		         },
	             splitLine:{
	                 show: false
	             },
	             axisLine: { 
	            	 show: false 
	             },
	             axisLabel: {
	                 show: false
	             }
		     }
		 ],
		 series : [
		     {
		         name:'Presupuesto',
		         type:'bar',
		         data:[presupuesto],
		         label: {
		        	 show: false,
		         },
		         markPoint : {
		             data : [
		                //{name : 'The highest year', value : 192.2, xAxis: 7, yAxis: 183, symbolSize:18},
		                // {name : 'Year minimum', value : 2.3, xAxis: 11, yAxis: 3}
		             ]
		         },
		         markLine : {
		             data : [
		                 //{type : 'average', name : 'Presupuesto'}
		             ]
		         }
		     },
		     {
		         name:'Meta',
		         type:'line',
		         data:[meta],
		         markPoint : {
		             data : [
		                 //{name : 'The highest year', value : 182.2, xAxis: 7, yAxis: 183, symbolSize:18},
		                 //{name : 'Year minimum', value : 2.3, xAxis: 11, yAxis: 3}
		             ]
		         },
		         markLine : {
		             data : [
		                 {type : 'average', name : 'Meta'}
		             ]
		         },
		     },
		     {
		         name:'Real',
		         type:'line',
		         data:[real],
		         markPoint : {
		             data : [
		                 //{type : 'max', name: 'Max'},
		                 //{type : 'min', name: 'Min'}
		             ]
		         },
		         markLine : {
		             data : [
		                 {type : 'average', name: 'Real'}
		             ]
		         }
		     }
		 ]
	};
	                 
	
	//use configuration item and data specified to show chart
	myChart.setOption(option, true), $(function() {
	         function resize() {
	             setTimeout(function() {
	                 myChart.resize()
	             }, 20)
	         }
	         $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
	});
}


function barChartDotacion(activos, totales, grafico) {	
	var dif = 120 - (1.2 * ((100 * activos) / totales));
	
	Chart.defaults.global.legend.display = false;
	var ctx = document.getElementById(grafico);
	var myChart = new Chart(ctx, {
	  type: 'bar',
	  data: {
	    datasets: [
	      {
	        data: [activos],
	        backgroundColor: '#db0a5b',
	      },
	      {
	        data: [dif],
	        backgroundColor: '#d9d9d6',
	      },
	    ]
	  },
	  options: {
		tooltips: {
			enabled: false
		},
		legend:{
			show: false
		},
	    scales: {
	      xAxes: [{ 
	    	  		stacked: true,
	    	  		gridLines: {
	                    lineWidth: 0,
	                    drawBorder: true,
                        display: false,
                        drawOnChartArea: false
	                },
	                ticks: {
	                	beginAtZero: true
                  }
	    	  	}
	    	  ],
	      yAxes: [{ 
	    	  		stacked: true, 
	    	  		gridLines: {
	                    lineWidth: 0,
	                    drawBorder: true,
                        display: false,
                        drawOnChartArea: false
	                },
	                display:false,
	                ticks: {
	                	beginAtZero: true
                  }
	      		}
	      	]
	    }
	  }
	});
}


function funnelChart(prospectos_porcentaje, prospectos_totalPlan, prospectos_totalReal,llamadas_porcentaje, llamadas_totalPlan, llamadas_totalReal, citas_porcentaje, citas_totalPlan, citas_totalReal, reunion_r1_porcentaje, reunion_r1_totalPlan, reunion_r1_totalReal, reunion_r2_porcentaje, reunion_r2_totalPlan, reunion_r2_totalReal, clientes_porcentaje, clientes_totalPlan, clientes_totalReal, cierre_porcentaje, cierre_totalPlan, cierre_totalReal) {
	var config = {
        type: 'funnel',
        data: {
            datasets: [{
                data: [prospectos_totalPlan, prospectos_totalPlan, citas_totalPlan, reunion_r1_totalPlan, reunion_r2_totalPlan, clientes_totalPlan, cierre_totalPlan],
                backgroundColor: [
            	   "#FF6384",
                   "#36A2EB",
                   "#FFCE56",
                   "#800000",
                   "#FFFF00",
                   "#808000",
                   "#00FF00"
                ],
                hoverBackgroundColor: [
                	"#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    "#800000",
                    "#FFFF00",
                    "#808000",
                    "#00FF00"
                ]
            }],
            labels: [
                "Prospectos (P) completado: " + prospectos_porcentaje + ',Plan: ' + prospectos_totalPlan + ',Real: ' + prospectos_totalReal,
                "Llamadas (LL) completado: " + llamadas_porcentaje + ',Plan: ' + llamadas_totalPlan + ',Real: ' + llamadas_totalReal,
                "Citas concertadas (C) : completado: " + citas_porcentaje + ',Plan: ' + citas_totalPlan + ',Real: ' + citas_totalReal,
                "Reunión 2 (R1) completado: " + reunion_r1_porcentaje + ',Plan: ' + reunion_r1_totalPlan + ',Real: ' + reunion_r1_totalReal,
                "Reunión 2 (R2) completado: " + reunion_r2_porcentaje + ',Plan: ' + reunion_r2_totalPlan + ',Real: ' + reunion_r2_totalReal,
                "Clientes cotizados (CO) completado: " + clientes_porcentaje + ',Plan: ' + clientes_totalPlan + ',Real: ' + clientes_totalReal,
                "Cierre (E) completado:" + cierre_porcentaje + ',Plan: ' + cierre_totalPlan + ',Real: ' + cierre_totalReal
            ]
        },
        options: {
            responsive: true,
            sort: 'desc',
            
            legend: {
                position: 'left'
            },
            title: {
                display: false,
                text: 'Funnel'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                callbacks: {
                  label: function(tooltipItems, data) {
                    return data.labels[tooltipItems.index] + data.datasets[0].data[tooltipItems.index].toLocaleString();
                  }
                }
              }
        }
    };

    var ctx = document.getElementById("funnelchart").getContext("2d");
    window.myDoughnut = new Chart(ctx, config);
}

function autodisciplina(arrayDias, arrayValores, arrayLabels){
    new Chart(document.getElementById("autodisciplina-asesor"),
    {
        "type":"doughnut",
        "data":{"labels":arrayLabels,
        "datasets":[{
            "label":"My First Dataset",
            "data":arrayValores,
            "backgroundColor":arrayDias}
        ]},
        options: {
            legend: {
               display: false
            },
            tooltips: {
               enabled: true,
               callbacks: {
            	      label: function(tooltipItem, data) {
            	        return arrayLabels[tooltipItem.index];
            	      }
               }
            }
       }
    });
};

function cambiaDecimal(valor){
	 var resp = JSON.stringify(valor).replace(".", ",");
	 if (resp != null && resp != undefined){
		 var cuenta = resp.length-resp.indexOf(",");
		 if(resp.indexOf(",") >= 0 && cuenta <= 2){
			 resp = resp + '0';
		 }
	 	return format(resp);
	 }else{
		return ''; 
	 }
}

function format(numero)
{
	 // Variable que contendra el resultado final
       var resultado = "";

       // Si el numero empieza por el valor "-" (numero negativo)
       if(numero[0]=="-")
       {
           // Cogemos el numero eliminando los posibles puntos que tenga, y sin
           // el signo negativo
           nuevoNumero=numero.replace(/\./g,'').substring(1);
       }else{
           // Cogemos el numero eliminando los posibles puntos que tenga
           nuevoNumero=numero.replace(/\./g,'');
       }

       // Si tiene decimales, se los quitamos al numero
       if(numero.indexOf(",")>=0)
           nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf(","));

       // Ponemos un punto cada 3 caracteres
       for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++)
           resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ".": "") + resultado;

       // Si tiene decimales, se lo añadimos al numero una vez forateado con 
       // los separadores de miles
       if(numero.indexOf(",")>=0)
           resultado+=numero.substring(numero.indexOf(","));

       if(numero[0]=="-")
       {
           // Devolvemos el valor añadiendo al inicio el signo negativo
           return "-"+resultado;
       }else{
           return resultado;
       }
}

